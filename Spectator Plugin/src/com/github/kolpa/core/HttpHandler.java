package com.github.kolpa.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

public class HttpHandler {
	private static Document getPage(String url) {
		try {
			return Jsoup.connect(url).get();
		} catch (IOException e) {
			return null;
		}
	}
	
	public static String[] getLadder(String Region){
		Document page = getPage("http://competitive.leagueoflegends.com/ladders/" + Region.toLowerCase() + "/current/rankedsolo5x5");
		Element table = page.getElementsByTag("tbody").get(0);
		List<Node> nodes = table.childNodes();
		ArrayList<String> tmp = new ArrayList<String>();
		for(Node n : nodes){
			if (n.childNodes().size() != 0){
				tmp.add(n.childNode(3).unwrap().toString());
			}
		}
		return tmp.toArray(new String[0]);
	}
}
