package com.github.kolpa.core;

import javax.swing.JOptionPane;

import net.antropod.plugins.noPermissionException;

public class Main {
	public Main() {
		try {
			System.out.println(HttpHandler.getLadder(net.antropod.plugins.Hooks.getLocation("SpectatorPlugin"))[0]);
		} catch (noPermissionException e) {
			JOptionPane.showMessageDialog(null, "This Plugin needs " + e.getPermission() + " to run properly");
		}
	}
	public static void main(String[] args) {
		JOptionPane.showMessageDialog(null, "This is a League of Legends Launcher Plugin Please don't run it manually");
	}

}