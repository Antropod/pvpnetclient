package com.riotgames.platform.statistics;

import java.util.Date;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class PlayerGameStats extends RiotObject
{
	public PlayerGameStats(TypedObject to)
	{
		super(to);
	}
	
	public FellowPlayerInfo[] fellowPlayers;
	public RawStat[] statistics;
	public Date createDate;
	public String skinName;
	public String gameType;
	public String gameTypeEnum;
	public String gameMode;
	public String subType;
	public String queueType;
	public String difficulty;
	public String difficultyString;
	public boolean ranked;
	public boolean eligibleFirstWinOfDay;
	public boolean leaver;
	public boolean afk;
	public boolean invalid;
	public boolean premadeTeam;
	public int skinIndex;
	public int experienceEarned;
	public int gameMapId;
	public int spell1;
	public int spell2;
	public int teamId;
	public int summonerId;
	public int boostXpEarned;
	public int level;
	public int userId;
	public int userServerPing;
	public int adjustedRating;
	public int premadeSize;
	public int boostIpEarned;
	public int timeInQueue;
	public int ipEarned;
	public int eloChange;
	public int KCoefficient;
	public int teamRating;
	public int predictedWinPct;
	public int rating;
	public double gameId;
	public double id;
}
