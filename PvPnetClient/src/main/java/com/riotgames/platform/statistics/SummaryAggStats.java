package com.riotgames.platform.statistics;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class SummaryAggStats extends RiotObject
{

	public SummaryAggStats(TypedObject to)
	{
		super(to);
	}
	public SummaryAggStat[] stats;
}
