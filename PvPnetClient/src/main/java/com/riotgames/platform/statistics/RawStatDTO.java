package com.riotgames.platform.statistics;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class RawStatDTO extends RiotObject
{
	public String statTypeName;
	public double value;

	public RawStatDTO(TypedObject to)
	{
		super(to);
	}
}