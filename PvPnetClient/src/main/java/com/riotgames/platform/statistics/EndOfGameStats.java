package com.riotgames.platform.statistics;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.team.TeamInfo;

public class EndOfGameStats extends RiotObject
{
	public PlayerParticipantStatsSummary[] teamPlayerParticipantStats;
	public PlayerParticipantStatsSummary[] otherTeamPlayerParticipantStats;
	public TeamInfo myTeamInfo;
	public TeamInfo otherTeamInfo;
	public String gameType;
	public String difficulty;
	public String gameMode;
	public String queueType;
	public String myTeamStatus;
	public String summonerName;
	public String roomName;
	public boolean ranked;
	public boolean leveledUp;
	public boolean imbalancedTeamsNoPoints;
	public boolean invalid;
	public boolean sendStatsToTournamentProvider;
	public int talentPointsGained;
	public int skinIndex;
	public int queueBonusEarned;
	public int experienceEarned;
	public int basePoints;
	public int gameLength;
	public int boostXpEarned;
	public int customMinutesLeftToday;
	public int loyaltyBoostIpEarned;
	public int rpEarned;
	public int completionBonusPoints;
	public int coOpVsAiMsecsUntilReset;
	public int boostIpEarned;
	public int experienceTotal;
	public int timeUntilNextFirstWinBonus;
	public int loyaltyBoostXpEarned;
	public int elo;
	public int ipEarned;
	public int firstWinBonus;
	public int eloChange;
	public int odinBonusIp;
	public int ipTotal;
	public int customMsecsUntilReset;
	public int userId;
	public double gameId;
	public double reportGameId;

	public EndOfGameStats(TypedObject to)
	{
		super(to);
	}
}
