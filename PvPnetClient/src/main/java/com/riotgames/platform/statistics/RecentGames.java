package com.riotgames.platform.statistics;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class RecentGames extends RiotObject
{
	public int userId;
	public PlayerGameStats[] gameStatistics;

	public RecentGames(TypedObject to)
	{
		super(to);
	}
}
