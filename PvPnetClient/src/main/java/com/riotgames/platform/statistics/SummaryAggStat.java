package com.riotgames.platform.statistics;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class SummaryAggStat extends RiotObject
{

	public SummaryAggStat(TypedObject to)
	{
		super(to);
	}
	
	public String 	statType;
	public int		count;
	public int		value;

}
