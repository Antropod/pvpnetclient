package com.riotgames.platform.statistics;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;


public class PlayerStatSummaries extends RiotObject
{

	public PlayerStatSummaries(TypedObject to)
	{
		super(to);
	}
	public PlayerStatSummary[] playerStatSummarySet;
	public double userId;

}
