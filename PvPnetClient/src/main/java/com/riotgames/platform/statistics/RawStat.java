package com.riotgames.platform.statistics;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class RawStat extends RiotObject
{
	public RawStat(TypedObject to)
	{
		super(to);
	}
	
	public String statType;
	public double value;

}
