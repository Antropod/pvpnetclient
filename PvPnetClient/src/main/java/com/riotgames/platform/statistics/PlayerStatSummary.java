package com.riotgames.platform.statistics;

import java.util.Date;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class PlayerStatSummary extends RiotObject
{
	public PlayerStatSummary(TypedObject to)
	{
		super(to);
	}
	public SummaryAggStats	 	aggregatedStats;
	public String 				playerStatSummaryType;
	public String 				playerStatSummaryTypeString;
	public Date 				modifyDate;
	public int 					leaves;
	public int					losses;
	public int					wins;
	public int					rating;
	public int 					maxRating;
	public double				userId;
	
}
