package com.riotgames.platform.statistics;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class FellowPlayerInfo extends RiotObject
{

	public FellowPlayerInfo(TypedObject to)
	{
		super(to);
	}
	
	public int championId;
	public int teamId;
	public int summonerId;

}
