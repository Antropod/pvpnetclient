package com.riotgames.platform.statistics;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class PlayerParticipantStatsSummary extends RiotObject
{
	public RawStatDTO[] statistics;
	public String skinName;
	public String summonerName;
	public boolean leaver;
	public boolean botPlayer;
	public int profileIconId;
	public int elo;
	public int leaves;
	public int teamId;
	public int eloChange;
	public int level;
	public int userId;
	public int spell2Id;
	public int spell1Id;
	public int losses;
	public int wins;
	public double gameId;

	public PlayerParticipantStatsSummary(TypedObject to)
	{
		super(to);
	}
}
