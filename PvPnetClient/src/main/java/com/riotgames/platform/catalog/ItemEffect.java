package com.riotgames.platform.catalog;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class ItemEffect extends RiotObject
{

	public ItemEffect(TypedObject to)
	{
		super(to);
	}
	
	public Effect 	effect;
	public int 		effectId;
	public int		itemEffectId;
	public int 		itemId;
	public String	value;
}
