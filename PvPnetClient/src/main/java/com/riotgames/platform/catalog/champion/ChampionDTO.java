package com.riotgames.platform.catalog.champion;

import net.antropod.client.resources.SQLDatabase;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class ChampionDTO extends RiotObject implements Comparable<ChampionDTO>
{
	public ChampionDTO(TypedObject to)
	{
		super(to);
		// TODO Auto-generated constructor stub
	}
	public ChampionSkinDTO[]	championSkins;		
	public String				description;
	public String				displayName;
	public String				skinName;
	public boolean 				freeToPlayReward;
	public boolean				botEnabled;
	public boolean				ownedByYourTeam;
	public boolean				freeToPlay;
	public boolean				banned;
	public boolean				ownedByEnemyTeam;
	public boolean				owned;
	public boolean				active;
	public int 					championId;
	public int					endDate;
	public int					winCountRemaining;
	@Override
	public int compareTo(ChampionDTO arg0)
	{
		String s1=SQLDatabase.getChampionNameByID(championId);
		String s2=SQLDatabase.getChampionNameByID(arg0.championId);
		return s1.compareToIgnoreCase(s2);
	}
}
