package com.riotgames.platform.catalog.champion;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class ChampionSkinDTO extends RiotObject
{
	public ChampionSkinDTO(TypedObject to)
	{
		super(to);
		// TODO Auto-generated constructor stub
	}
	public boolean	stillObtainable;
	public boolean	lastSelected;
	public boolean	owned;
	public boolean	freeToPlayReward;
	public int		championId;
	public int		skinId;
	public int		skinIndex;
	public int		endDate;
	public int		winCountRemaining;
	public int		purchaseDate;
}
