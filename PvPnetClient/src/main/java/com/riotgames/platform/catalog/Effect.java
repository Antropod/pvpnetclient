package com.riotgames.platform.catalog;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.platform.catalog.runes.RuneType;

public class Effect extends RiotObject
{
	public Effect(TypedObject to)
	{
		super(to);
	}
	
	public RuneType runeType;
	public String	gameCode;
	public String	name;
	public int		effectId;
	//				categoryId
}
