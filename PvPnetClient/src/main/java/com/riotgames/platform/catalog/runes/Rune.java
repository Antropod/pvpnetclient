package com.riotgames.platform.catalog.runes;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.platform.catalog.ItemEffect;

public class Rune extends RiotObject
{
	public Rune(TypedObject to)
	{
		super(to);
	}
	public RuneType 	runeType;
	public ItemEffect[] itemEffects;
	public String 		imagePath;
	public String 		toolTip;
	public String		baseType;
	public String		name;
	public String		description;
	public int			tier;
	public int			itemId;
	public int 			duration;
	public int			gameCode;
	//					uses;
	
	
}
