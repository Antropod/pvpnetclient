package com.riotgames.platform.catalog.runes;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class RuneType extends RiotObject
{
	public RuneType(TypedObject to)
	{
		super(to);
	}
	
	public String 	name;
	public int		runeTypeId;

}
