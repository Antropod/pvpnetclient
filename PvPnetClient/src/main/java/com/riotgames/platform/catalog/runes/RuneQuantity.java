package com.riotgames.platform.catalog.runes;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class RuneQuantity extends RiotObject
{
	public int runeId;
	public int quantity;

	public RuneQuantity(TypedObject to)
	{
		super(to);
	}
}
