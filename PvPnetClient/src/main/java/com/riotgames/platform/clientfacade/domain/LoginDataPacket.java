package com.riotgames.platform.clientfacade.domain;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.kudos.dto.PendingKudosDTO;
import com.riotgames.platform.broadcast.BroadcastNotification;
import com.riotgames.platform.game.GameTypeConfigDTO;
import com.riotgames.platform.statistics.PlayerStatSummaries;
import com.riotgames.platform.summoner.AllSummonerData;
import com.riotgames.platform.summoner.SummonerCatalog;
import com.riotgames.platform.systemstate.ClientSystemStatesNotification;

public class LoginDataPacket extends RiotObject
{
	public LoginDataPacket(TypedObject to)
	{
		super(to);
	}
	
	public PlayerStatSummaries 		playerStatSummaries;
	public SummonerCatalog 			summonerCatalog;
	public AllSummonerData 			allSummonerData;
	public PendingKudosDTO 			pendingKudosDTO;
	public GameTypeConfigDTO[] 		gameTypeConfigs;
	public BroadcastNotification	broadcastNotification;
	public ClientSystemStatesNotification clientSystemStates;
	//								reconnectInfo;
	public String[]					languages;
	public String					platformId;
	public String					competitiveRegion;
	public boolean					minor;
	public boolean					inGhostGame;
	public boolean					bingePreventionSystemEnabledForClient;
	public boolean					matchmakingEnabled;
	public boolean					minutesUntilShutdownEnabled;
	public boolean					bingeIsPlayerInBingePreventionWindow;
	public boolean					minorShutdownEnforced;
	public int						minutesUntilShutdown;
	public int						maxPracticeGameSize;
	public int						ipBalance;
	public int						rpBalance;
	public int						customMinutesLeftToday;
	public int						coOpVsAiMinutesLeftToday;
	public int						leaverPenaltyLevel;
	public int 						pendingBadges;
	public int						minutesUntilMidnight;
	public int						timeUntilFirstWinOfDay;
	public int						coOpVsAiMsecsUntilReset;
	public int						bingeMinutesRemaining;
	public int						leaverBusterPenaltyTime;
	public int						customMsecsUntilReset;
	
}
