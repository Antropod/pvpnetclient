package com.riotgames.platform.systemstate;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class ClientSystemStatesNotification extends RiotObject
{

	public ClientSystemStatesNotification(TypedObject to)
	{
		super(to);
	}
	//public GameMapEnabledDTO[]	gameMapEnabledDTOList;
	//public QueueThrottleDTO		queueThrottleDTO;
	public String[]				observableGameModes;
	public String				replayServiceAddress;
	public boolean				runeUniquePerSpellBook;
	public boolean				modularGameModeEnabled;
	public boolean				socialIntegrationEnabled;
	public boolean				teamServiceEnabled;
	public boolean				storeCustomerEnabled;
	public boolean				tribunalEnabled;
	public boolean				practiceGameEnabled;
	public boolean				advancedTutorialEnabled;
	public boolean				observerModeEnabled;
	public boolean				archievedStatsEnabled;
	public boolean				displayPromoGamesPlayedEnabled;
	public boolean				masteryPageOnServer;
	public boolean				tournamentSendStatsEnabled;
	public boolean				kudosEnabled;
	public boolean				buddyNotesEnabled;
	public boolean				localeSpecificChatRoomsEnabled;
	public Integer[]			inactiveOdinSpellIdList;
	public Integer[]			inactiveTutorialSpellIdList;
	public Integer[]			inactiveSpellIdList;
	public Integer[]			inactiveClassicSpellIdList;
	public Integer[]			inactiveChampionIdList;
	public Integer[]			unobtainableChampionSkinIDList;
	//public Integer[]			obtainableChampionSkinIDList;
	public Integer[]			practiceGameTypeConfigIdList;
	public Integer[]			enabledQueueIdsList;
	public Integer[]			freeToPlayChampionIdList;
	public int					minNumPlayersForPracticeGame;
	public int					spectatorSlotLimit;
	public int					clientHeartBeatRateSeconds;
	public int					maxMasteryPagesOnServer;
	public double 				riotDataServiceDataSendProbability;
	
	
}
