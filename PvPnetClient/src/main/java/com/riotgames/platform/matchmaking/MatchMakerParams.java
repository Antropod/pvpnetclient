package com.riotgames.platform.matchmaking;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class MatchMakerParams extends RiotObject
{

	public MatchMakerParams()
	{
	}

	public MatchMakerParams(TypedObject to)
	{
		super(to);
	}

	
	public String botDifficulty;
	public String lastMaestroMessage;
	public Integer[] queueIds;
	public double teamId;
	public double invitationId;
	//public ? languages;
	public Integer[] team;
	
}
