package com.riotgames.platform.matchmaking;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class QueueInfo extends RiotObject
{
	public QueueInfo(TypedObject to)
	{
		super(to);
	}
	
	public int waitTime;
	public int queueId;
	public int queueLength;

}
