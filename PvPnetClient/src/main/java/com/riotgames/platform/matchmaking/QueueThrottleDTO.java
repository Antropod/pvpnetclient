package com.riotgames.platform.matchmaking;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class QueueThrottleDTO extends RiotObject
{

	public QueueThrottleDTO(TypedObject to)
	{
		super(to);
	}

	public String mode;
	public boolean generic;
	public int level;
	public int queueId;
}
