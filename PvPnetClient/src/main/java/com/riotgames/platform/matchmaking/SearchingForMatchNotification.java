package com.riotgames.platform.matchmaking;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class SearchingForMatchNotification extends RiotObject
{

	public SearchingForMatchNotification(TypedObject to)
	{
		super(to);
	}
	
	//public ? playerJoinFailures;//not sure which type this object is
	//public ? ghostGameSummoners;//same here
	public QueueInfo[] joinedQueues;

}
