package com.riotgames.platform.matchmaking;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class MatchingThrottleConfig extends RiotObject
{

	public MatchingThrottleConfig(TypedObject to)
	{
		super(to);
	}
	
	//public ?[] matchingThrottleProperties;
	public String cacheName;
	public double limit;

}
