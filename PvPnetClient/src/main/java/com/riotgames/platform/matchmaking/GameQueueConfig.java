package com.riotgames.platform.matchmaking;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class GameQueueConfig extends RiotObject
{
	public GameQueueConfig(TypedObject to)
	{
		super(to);
	}

	public MatchingThrottleConfig matchingThrottleConfig;
	public String queueState;
	public String queueStateString;
	public String type;
	public String cacheName;
	public String queueBonusKey;
	public String pointsConfigKey;
	public String gameMode;
	public String typeString;
	public String mapSelectionAlgorithm;
	public boolean thresholdEnabled;
	public boolean ranked;
	public boolean teamOnly;
	public boolean disallowFreeChampions;
	public Integer[] supportedMapIds;
	public int blockedMinutesThreshold;
	public int minimumParticipantListSize;
	public int maxLevel;
	public int minLevel;
	public int gameTypeConfigId;
	public int id;
	public int minimumQueueDodgeDelayTime;
	public int numPlayersPerTeam;
	public int maximumParticipantListSize;
	public double thresholdSize;

}
