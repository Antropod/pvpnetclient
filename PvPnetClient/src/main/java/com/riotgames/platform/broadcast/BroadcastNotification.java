package com.riotgames.platform.broadcast;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class BroadcastNotification extends RiotObject
{
	public BroadcastNotification(TypedObject to)
	{
		super(to);
		// TODO Auto-generated constructor stub
	}
	public String content;
	public String severity;
	public String messageKey;
	public int id;
}
