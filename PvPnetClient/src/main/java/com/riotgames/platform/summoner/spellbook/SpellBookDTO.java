package com.riotgames.platform.summoner.spellbook;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class SpellBookDTO extends RiotObject
{
	public SpellBookDTO(TypedObject to)
	{
		super(to);
	}
	public String dateString;
	public double summonerId;
	public SpellBookPageDTO[] bookPages;
}
