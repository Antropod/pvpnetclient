package com.riotgames.platform.summoner;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class TalentRow extends RiotObject
{

	public TalentRow(TypedObject to)
	{
		super(to);
	}
	public Talent[] talents;
	public int 		index;
	public int		tltGroupId;
	public int		tltRowId;
	public int		pointsToActivate;
}
