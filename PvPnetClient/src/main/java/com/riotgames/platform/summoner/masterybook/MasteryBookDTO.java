package com.riotgames.platform.summoner.masterybook;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class MasteryBookDTO extends RiotObject
{
	public MasteryBookDTO(TypedObject to)
	{
		super(to);
	}
	public MasteryBookPageDTO[] bookPages;
	public String 				dateString;
	public double				summonerId;
}
