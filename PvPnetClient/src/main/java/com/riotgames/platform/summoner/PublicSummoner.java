package com.riotgames.platform.summoner;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import java.util.Date;

public class PublicSummoner extends RiotObject
{
	public String internalName;
	public int acctId;
	public String name;
	public int profileIconId;
	public Date revisionDate;
	public int revisionId;
	public int summonerLevel;
	public int summonerId;

	public PublicSummoner(TypedObject to)
	{
		super(to);
	}
}
