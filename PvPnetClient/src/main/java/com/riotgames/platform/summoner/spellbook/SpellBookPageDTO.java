package com.riotgames.platform.summoner.spellbook;

import java.util.Date;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class SpellBookPageDTO extends RiotObject implements Comparable<SpellBookPageDTO>
{

	public SpellBookPageDTO(TypedObject to)
	{
		super(to);
	}
	public SlotEntry[] 	slotEntries;
	public String 		name;
	public Date			createDate;
	public boolean 		current;
	public double 		pageId;
	public double 		summonerId;
	
	public String toString()
	{
		return name;
	}
	
	@Override
	public int compareTo(SpellBookPageDTO m)
	{
		if(pageId>m.pageId)
			return 1;
		else if(pageId<m.pageId)
			return -1;
		else
			return 0;
	}
	
}
