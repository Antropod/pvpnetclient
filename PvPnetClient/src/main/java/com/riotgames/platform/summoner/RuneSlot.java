package com.riotgames.platform.summoner;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.platform.catalog.runes.RuneType;

public class RuneSlot extends RiotObject
{
	public RuneSlot(TypedObject to)
	{
		super(to);
	}
	public RuneType runeType;
	public int 		id;
	public int		minLevel;

}
