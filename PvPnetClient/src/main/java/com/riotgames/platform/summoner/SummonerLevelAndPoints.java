package com.riotgames.platform.summoner;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class SummonerLevelAndPoints extends RiotObject
{
	public SummonerLevelAndPoints(TypedObject to)
	{
		super(to);
	}
	
	public int 		infPoints;
	public int 		expPoints;
	public int 		summonerLevel;
	public double	summonerId;
}
