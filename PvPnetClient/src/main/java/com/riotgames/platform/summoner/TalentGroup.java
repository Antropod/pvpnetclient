package com.riotgames.platform.summoner;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class TalentGroup extends RiotObject
{

	public TalentGroup(TypedObject to)
	{
		super(to);
	}
	public TalentRow[] 	talentRows;
	public String 		name;
	public int			tltGroupId;
	public int			index;
	
}
