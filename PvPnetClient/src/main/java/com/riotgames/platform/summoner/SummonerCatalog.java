package com.riotgames.platform.summoner;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class SummonerCatalog extends RiotObject
{

	public SummonerCatalog(TypedObject to)
	{
		super(to);
	}
	
	public TalentGroup[] 	talentTree;
	public RuneSlot[]		spellBookConfig;
}
