package com.riotgames.platform.summoner;

import java.util.Date;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class SummonerTalentsAndPoints extends RiotObject
{
	public SummonerTalentsAndPoints(TypedObject to)
	{
		super(to);
	}
	
	public Date modifyDate;
	public Date createDate;
	public int talentPoints;
	public double summonerId;

}
