package com.riotgames.platform.summoner;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class SummonerLevel extends RiotObject
{
	public SummonerLevel(TypedObject to)
	{
		super(to);
	}
	
	public int expTierMod;
	public int grantRp;
	public int expForLoss;
	public int summonerTier;
	public int infTierMod;
	public int expToNextLevel;
	public int expForWin;
	public int summonerLevel;

}
