package com.riotgames.platform.summoner.masterybook;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.platform.summoner.Talent;

public class TalentEntry extends RiotObject
{
	public TalentEntry(TypedObject to)
	{
		super(to);
	}
	public Talent 	talent;
	public int		rank;
	public int		talentId;
	public double 	summonerId;
}
