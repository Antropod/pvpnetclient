package com.riotgames.platform.summoner;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.platform.summoner.masterybook.MasteryBookDTO;
import com.riotgames.platform.summoner.spellbook.SpellBookDTO;

public class AllSummonerData extends RiotObject
{
	public AllSummonerData(TypedObject to)
	{
		super(to);
	}
	public SpellBookDTO spellBook;
	//public SummonerDefaultSpells summonerDefaultSpells; //Not necessary/too complicated...
	public SummonerTalentsAndPoints summonerTalentsAndPoints;
	public Summoner summoner;
	public MasteryBookDTO masteryBook;
	public SummonerLevelAndPoints summonerLevelAndPoints;
	public SummonerLevel summonerLevel;
	
}
