package com.riotgames.platform.summoner.spellbook;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.platform.catalog.runes.Rune;
import com.riotgames.platform.summoner.RuneSlot;

public class SlotEntry extends RiotObject
{

	public SlotEntry(TypedObject to)
	{
		super(to);
	}
	
	public RuneSlot runeSlot;
	public Rune rune;
	public int runeId;
	public int runeSlotId;
}
