package com.riotgames.platform.summoner;

import java.util.Date;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class Summoner extends RiotObject
{
	public Summoner(TypedObject to)
	{
		super(to);
	}
	public String 	seasonTwoTier;
	public String 	seasonOneTier;
	public String 	internalName;
	public Date 	lastGameDate;
	public Date		revisionDate;
	public String 	name;
	public boolean	helpFlag;
	public boolean	displayEloQuestionaire;
	public boolean 	advancedTutorialFlag;
	public boolean	nameChangeFlag;
	public boolean	tutorialFlag;
	public int		profileIconId;
	public int		revisionid;
	public double	acctId;
	public double	sumId;
}
