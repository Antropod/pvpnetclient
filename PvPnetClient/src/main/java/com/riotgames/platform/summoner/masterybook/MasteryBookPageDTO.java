package com.riotgames.platform.summoner.masterybook;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class MasteryBookPageDTO extends RiotObject implements Comparable<MasteryBookPageDTO>
{
	public MasteryBookPageDTO(TypedObject to)
	{
		super(to);
	}
	public TalentEntry[] 	talentEntries;
	public String 			name;
	public boolean 			current;
	public int 				pageId;
	public double 			summonerId;
	
	public String toString()
	{
		return name;
	}

	@Override
	public int compareTo(MasteryBookPageDTO m)
	{
		if(pageId>m.pageId)
			return 1;
		else if(pageId<m.pageId)
			return -1;
		else
			return 0;
	}
	
}
