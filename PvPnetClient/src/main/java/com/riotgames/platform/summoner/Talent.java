package com.riotgames.platform.summoner;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class Talent extends RiotObject
{

	public Talent(TypedObject to)
	{
		super(to);
	}
	public String 	name;
	public String 	level1Desc;
	public String 	level2Desc;
	public String 	level3Desc;
	public String 	level4Desc;
	public String 	level5Desc;
	public Integer	pereqTalentGameCode;
	public int		index;
	public int		minLevel;
	public int		maxRank;
	public int		tltId;
	public int		talentGroupId;
	public int		gameCode;
	public int		minTier;
	public int		talentRowId;
}
