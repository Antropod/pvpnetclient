package com.riotgames.platform.inventory.loot;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class RuneCombiner extends RiotObject
{
	public int id;
	public LootTable lootTable;
	public String name;
	public int inputTier;
	public int inputCount;

	public RuneCombiner(TypedObject to)
	{
		super(to);
	}
}