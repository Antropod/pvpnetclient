package com.riotgames.platform.inventory.loot;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class LootTable extends RiotObject
{
	public int id;
	public int minNumberOfItemsReturned;
	public LootTableItem[] lootTableItems;
	public int type;

	public LootTable(TypedObject to)
	{
		super(to);
	}
}