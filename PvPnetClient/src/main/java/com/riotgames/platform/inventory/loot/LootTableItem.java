package com.riotgames.platform.inventory.loot;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class LootTableItem extends RiotObject
{
	public int id;
	public int refId;
	public int type;
	public int likelihood;

	public LootTableItem(TypedObject to)
	{
		super(to);
	}
}