package com.riotgames.platform.game.practice;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.platform.game.PlayerParticipant;

public class PracticeGameSearchResult extends RiotObject
{
	//public Object	glmHost;
	//public Object	glmGameId;
	//public int 	glmPort;
	//public int	glmSecurePort;
	
	public PracticeGameSearchResult(TypedObject to)
	{
		super(to);
		// TODO Auto-generated constructor stub
	}
	public PlayerParticipant	owner;
	public String 				gameModeString;
	public String				gameMode;
	public String				allowSpectators;
	public String				name;
	public boolean				privateGame;
	public int					spectatorCount;
	public int					gameMapId;
	public int					maxNumPlayers;
	public int					team1Count;
	public int					team2Count;
	public double				id;
	
}
