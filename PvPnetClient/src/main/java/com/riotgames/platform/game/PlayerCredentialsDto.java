/**
 * 
 */
package com.riotgames.platform.game;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

/**
 * @author Antropod
 *
 */
public class PlayerCredentialsDto extends RiotObject
{
	public PlayerCredentialsDto(TypedObject to)
	{
		super(to);
		// TODO Auto-generated constructor stub
	}
	
	public String encryptionKey;
	public String observerEncryptionKey;
	public String serverIp;
	public String observerServerIp;
	public String handshakeToken;
	public String summonerName;
	public boolean observer;
	public double gameId;
	public int summonerId;
	public int serverPort;
	public int observerServerPort;
	public int championId;
	
}
