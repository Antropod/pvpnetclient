package com.riotgames.platform.game;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.platform.catalog.champion.ChampionDTO;

public class BotParticipant extends KnownParticipant
{
	public BotParticipant(TypedObject to)
	{
		super(to);
		// TODO Auto-generated constructor stub
	}
	public ChampionDTO champion;
	//public String summonerName;
	//public String summonerInternalName;
	public String botSkillLevelName;
	public boolean isGameOwner;
	public boolean isMe;
	//public int lastSelectedSkinIndex;
	public String teamId;
	public int botSkillLevel;
	//public int badges;
	//public int pickMode;
	//public int pickTurn;
	public int team;
}
