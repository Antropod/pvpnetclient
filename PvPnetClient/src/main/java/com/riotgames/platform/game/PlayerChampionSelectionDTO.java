package com.riotgames.platform.game;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class PlayerChampionSelectionDTO extends RiotObject{
	
	public PlayerChampionSelectionDTO(TypedObject to)
	{
		super(to);
		// TODO Auto-generated constructor stub
	}
	public String summonerInternalName;
	public int spell2Id;
	public int selectedSkinIndex;
	public int championId;
	public int spell1Id;
	
	

}
