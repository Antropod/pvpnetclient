package com.riotgames.platform.game;


import net.antropod.client.resources.LanguageDB;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class GameTypeConfigDTO extends RiotObject
{
	public GameTypeConfigDTO(TypedObject to)
	{
		super(to);
		// TODO Auto-generated constructor stub
	}
	public String 	name;
	public String 	pickMode;
	public boolean	allowTrades;
	public boolean	exclusivePick;
	public int		id;
	public int		mainPickTimerDuration;
	public int		maxAllowablebans;
	public int		banTimerDuration;
	public int		postPickTimerDuration;
	
	public String toString()
	{
		return LanguageDB.getLocalizedString("PICK_MODE"+id);
	}
}
