package com.riotgames.platform.game;

import net.antropod.client.resources.SQLDatabase;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class ChampionBanInfoDTO extends RiotObject implements Comparable<ChampionBanInfoDTO>
{
	public ChampionBanInfoDTO(TypedObject to)
	{
		super(to);
		// TODO Auto-generated constructor stub
	}
	
	public boolean owned;
	public boolean enemyOwned;
	public int championId;


	public int compareTo(ChampionBanInfoDTO arg0)
	{
		String s1=SQLDatabase.getChampionNameByID(championId);
		String s2=SQLDatabase.getChampionNameByID(arg0.championId);
		return s1.compareToIgnoreCase(s2);
	}
}
