package com.riotgames.platform.game.map;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class GameMapEnabledDTO extends RiotObject
{
	public GameMapEnabledDTO(TypedObject to)
	{
		super(to);
	}
	
	public int minPlayers;
	public int gameMapId;

}
