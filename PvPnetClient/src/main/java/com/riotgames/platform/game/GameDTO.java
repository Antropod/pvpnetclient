package com.riotgames.platform.game;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class GameDTO extends RiotObject
{
	public static final String STATE_TEAM_SELECT=			"TEAM_SELECT";
	public static final String STATE_PRE_CHAMP_SELECT=		"PRE_CHAMP_SELECT";
	public static final String STATE_CHAMP_SELECT=			"CHAMP_SELECT";
	public static final String STATE_POST_CHAMP_SELECT=		"POST_CHAMP_SELECT";
	public static final String STATE_JOINING_CHAMP_SELECT=	"JOINING_CHAMP_SELECT";
	public static final String STATE_TERMINATED= 			"TERMINATED";

	public static final String TYPE_CUSTOM_GAME=	"PRACTICE_GAME";//Custom game was called practice game previously
	public static final String TYPE_PRACTICE_GAME=	"PRACTICE_GAME";
	public static final String TYPE_COOP_GAME=		"COOP_VS_AI_GAME";
	public static final String TYPE_NORMAL_GAME=	"NORMAL_GAME";
	public static final String TYPE_RANKED_GAME=	"RANKED_GAME";
	
	public GameDTO(TypedObject to)
	{
		super(to);
	}
	public GameObserver[]				observers;
	public PlayerParticipant			ownerSummary;
	public Participant[]				teamOne;
	public Participant[]				teamTwo;
	public BannedChampion[]				bannedChampions;
	public PlayerChampionSelectionDTO[]	playerChampionSelections;
	public String 						spectatorsAllowed;
	public String 						gameType;
	public String 						roomName;
	public String 						name;
	public String						terminatedCondition;
	public String 						queueTypeName;
	public String						roomPassword;
	public String						gameMode;
	public String 						gameStateString;
	public String 						gameState;
	public String						statusOfParticipants;
	public boolean						passwordSet;
	//public int[]						banOrder;
	public int							gameTypeConfigId;
	public int		 					spectatorDelay;
	public int							optimisticLock;
	public int							maxNumPlayers;
	public int							queuePosition;
	public int 							expiryTime;
	public int 							mapId;
	public int							pickTurn;
	public double						id;
	
}
