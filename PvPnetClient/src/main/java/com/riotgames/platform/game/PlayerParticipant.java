package com.riotgames.platform.game;

import com.gvaneyck.rtmp.TypedObject;


public class PlayerParticipant extends  KnownParticipant
{
	public PlayerParticipant(TypedObject to)
	{
		super(to);
		// TODO Auto-generated constructor stub
	}
	//public RiotObject dateOfBirth;
	
	public String	botDifficulty;
	//public String	summonerName;
	//public String	summonerInternalName;
	public String	originalPlatformId;
	public boolean	teamOwner;
	public boolean	clientInSynch;
	public int		index;
	public int		queueRating;
	public int		accountId;
	public int		originalAccountNumber;
	//public int	lastSelectedSkinIndex;
	public int		profileIconId;
	public int		summonerId;
	//public int	badges;
	//public int	pickTurn;
	//public int	pickMode;
	public int		teamParticipantId;
	public int		timeAddedToQueue;
	
}
