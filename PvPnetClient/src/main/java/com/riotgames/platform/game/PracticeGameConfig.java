package com.riotgames.platform.game;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.platform.game.map.GameMap;

public class PracticeGameConfig extends RiotObject
{
	public PracticeGameConfig()
	{
		super();
	}
	public PracticeGameConfig(TypedObject to)
	{
		super(to);
		// TODO Auto-generated constructor stub
	}
	public String gamePassword;
	public String gameName;
	public String gameMode;
	public GameMap gameMap;
	public int maxNumPlayers;
	public int gameTypeConfig;
	public String allowSpectators;
	
}
