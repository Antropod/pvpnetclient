package com.riotgames.platform.game.map;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class GameMap extends RiotObject
{
	public GameMap(TypedObject to)
	{
		super(to);
		// TODO Auto-generated constructor stub
	}
	public GameMap()
	{
		super();
	}
	public String description;
	public String displayName;
	public String name;
	public int mapId;
	public int totalPlayers;
	public int minCustomPlayers;
	
	@Override
	public String toString()
	{
		return displayName;
	}
}
