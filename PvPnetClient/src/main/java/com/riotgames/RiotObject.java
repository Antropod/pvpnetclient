package com.riotgames;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import com.gvaneyck.rtmp.TypedObject;


public class RiotObject
{
	protected RiotObject()
	{
		super();
	}

	public RiotObject(TypedObject to)
	{
		if (to!=null&&to.type!=null&&!this.getClass().getName().equals(to.type))
			throw new IllegalArgumentException(this.getClass().getName() + " != " + to.type);
		else if(to!= null)
		{
			Field[] fields = this.getClass().getFields();
			for (Field f : fields)
			{
				Class<?> type = f.getType();
				if (to.get(f.getName()) != null)
				{

					//System.out.println(f.getType().getSimpleName() + " " + f.getName() + " should be: " + to.get(f.getName()).getClass().getSimpleName());
				}
				else
				{
					//System.out.println(f.getType().getSimpleName() + " " + f.getName() + " is null");
				}
				try
				{
					if (type.isArray())
					{
						// System.out.println("its an array   "+
						// type.getSimpleName()+" ;;; "+type.getName());
						Object[] arr = to.getArray(f.getName());
						String classname = type.getComponentType().getName();
						if (classname.contains("com.riotgames"))
						{
							Object[] convarr = (Object[]) Array.newInstance(type.getComponentType(), arr.length);
							for (int i = 0; i < arr.length; i++)
							{
								Constructor<?> con = Class.forName(((TypedObject) arr[i]).type).getConstructor(TypedObject.class);
								convarr[i] = con.newInstance((TypedObject) arr[i]);

							}
							f.set(this, convarr);
						} else
						// if(type.isPrimitive()||type.isInstance(new String()))
						{
							Object[] objarr = null;
							objarr = (Object[]) Array.newInstance(type.getComponentType(), arr.length);
							
							for (int i = 0; i < arr.length; i++)
							{
								
								objarr[i] = arr[i];

							}
							f.set(this, objarr);

						}

					} else if (type.getName().contains("com.riotgames") && to.getTO(f.getName()) != null)
					{
						// System.out.println(type.getName());
						Constructor<?> con = Class.forName(to.getTO(f.getName()).type).getConstructor(TypedObject.class);
						Object o = con.newInstance(to.getTO(f.getName()));
						f.set(this, o);
					} else
					// if(type.isPrimitive()||type.isInstance(new String()))
					{

						String s = type.getName();
						if (to.get(f.getName()) == null)
							continue;
						switch (s)
						{
							case "boolean":
								f.setBoolean(this, to.getBoolean(f.getName()));
								break;
							case "double":
								f.setDouble(this, to.getDouble(f.getName()));
								break;
							case "int":
								f.setInt(this, to.getInt(f.getName()));
								break;
							default:
								f.set(this, to.get(f.getName()));
								break;
						}
					}

				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	public TypedObject toTypedObject()
	{
		TypedObject to = new TypedObject();
		try
		{
			to.type = getClass().getName();
			System.out.println(getClass().getName());
			Field[] fields = getClass().getFields();

			for (Field f : fields)
			{
				Object o = null;
				if (f.get(this) != null)
				{
					if (f.getType().isArray())
					{
						Class<?> c = f.getType().getComponentType();
						if (c.getName().startsWith("com.riotgames"))
						{
							RiotObject[] ro = (RiotObject[]) f.get(this);
							TypedObject[] convto = new TypedObject[ro.length];
							for (int i = 0; i < ro.length; i++)
							{
								convto[i] = ro[i].toTypedObject();
							}
							o = convto;
						} else
						{
							o = f.get(this);
						}
					} else
					{
						Class<?> c = f.getType();
						if (c.getName().startsWith("com.riotgames"))
						{
							RiotObject ro = (RiotObject) f.get(this);
							o = ro.toTypedObject();
						} else
						{
							o = f.get(this);
						}
					}
				}
				to.put(f.getName(), o);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return to;
	}
	/*
	 * public String toString() { try { StringBuilder sb = new
	 * StringBuilder(1000); Field[] fields = getClass().getFields();
	 * sb.append(getClass().getName()).append(": { "); for (Field f : fields) {
	 * 
	 * sb.append(f.getName()).append("=").append(f.get(this)).append("; "); }
	 * sb.append("}"); return sb.toString(); } catch (IllegalArgumentException |
	 * IllegalAccessException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); return "NULL"; } }
	 */
}
