package com.riotgames.team;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class TeamId extends RiotObject
{
	public String fullId;

	public TeamId(TypedObject to)
	{
		super(to);
	}
}
