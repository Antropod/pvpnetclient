package com.riotgames.team.dto;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import java.util.Date;

public class TeamMemberInfoDTO extends RiotObject
{
	public String playerName;
	public String status;
	public Date joinDate;
	public Date inviteDate;
	public int playerId;

	public TeamMemberInfoDTO(TypedObject to)
	{
		super(to);
	}
}
