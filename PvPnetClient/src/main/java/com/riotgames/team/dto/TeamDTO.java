package com.riotgames.team.dto;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.team.TeamId;
import com.riotgames.team.stats.MatchHistorySummary;
import com.riotgames.team.stats.TeamStatSummary;
import java.util.Date;

public class TeamDTO extends RiotObject
{
	public TeamStatSummary teamStatSummary;
	public RosterDTO roster;
	public TeamId teamId;
	public MatchHistorySummary[] matchHistory;
	public String status;
	public String tag;
	public String name;
	public Date lastGameDate;
	public Date modifyDate;
	public Date lastJoinDate;
	public Date secondLastJoinDate;
	public Date thirdLastJoinDate;
	public Date createDate;
	public double secondsUntilEligibleForDeletion;

	public TeamDTO(TypedObject to)
	{
		super(to);
	}
}
