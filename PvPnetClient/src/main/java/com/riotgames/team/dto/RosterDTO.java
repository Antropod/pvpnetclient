package com.riotgames.team.dto;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class RosterDTO extends RiotObject
{
	public int ownerId;
	public TeamMemberInfoDTO[] memberList;

	public RosterDTO(TypedObject to)
	{
		super(to);
	}
}
