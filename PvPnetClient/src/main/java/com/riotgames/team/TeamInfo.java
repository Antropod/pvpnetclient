package com.riotgames.team;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class TeamInfo extends RiotObject
{
	public TeamId teamId;
	public String memberStatusString;
	public String memberStatus;
	public String name;
	public String tag;
	public double secondsUntilEligibleForDeletion;

	public TeamInfo(TypedObject to)
	{
		super(to);
	}
}
