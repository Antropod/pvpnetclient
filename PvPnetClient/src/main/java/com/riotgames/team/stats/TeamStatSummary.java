package com.riotgames.team.stats;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.team.TeamId;

public class TeamStatSummary extends RiotObject
{
	public TeamStatDetail[] teamStatDetails;
	public TeamId teamId;
	public String teamIdString;

	public TeamStatSummary(TypedObject to)
	{
		super(to);
	}
}
