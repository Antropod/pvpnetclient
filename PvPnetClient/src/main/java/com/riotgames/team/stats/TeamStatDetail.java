package com.riotgames.team.stats;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.team.TeamId;

public class TeamStatDetail extends RiotObject
{
	public TeamId teamId;
	public String teamIdString;
	public String teamStatTypeString;
	public String teamStatType;
	public int maxRating;
	public int seedRating;
	public int losses;
	public int rating;
	public int averageGamesPlayed;
	public int wins;

	public TeamStatDetail(TypedObject to)
	{
		super(to);
	}
}
