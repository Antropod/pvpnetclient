package com.riotgames.team.stats;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import java.util.Date;

public class MatchHistorySummary extends RiotObject
{
	public String gamemode;
	public String opposingTeamName;
	public boolean invalid;
	public boolean win;
	public int mapId;
	public int assists;
	public int deaths;
	public double gameId;
	public int kills;
	public Date date;
	public int opposingTeamKills;

	public MatchHistorySummary(TypedObject to)
	{
		super(to);
	}
}
