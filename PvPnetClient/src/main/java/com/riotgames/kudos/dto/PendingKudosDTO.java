package com.riotgames.kudos.dto;


import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class PendingKudosDTO extends RiotObject
{
	public PendingKudosDTO(TypedObject to)
	{
		super(to);
	}
	public Integer[] pendingCounts;

}
