package com.riotgames.messaging;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class StoreAccountBalanceNotification extends RiotObject
{
	public int rp;
	public int ip;

	public StoreAccountBalanceNotification(TypedObject to)
	{
		super(to);
	}
}