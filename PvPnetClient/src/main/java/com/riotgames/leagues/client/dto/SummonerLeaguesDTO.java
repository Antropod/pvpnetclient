package com.riotgames.leagues.client.dto;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;
import com.riotgames.leagues.pojo.LeagueListDTO;

public class SummonerLeaguesDTO extends RiotObject
{

	public SummonerLeaguesDTO(TypedObject to)
	{
		super(to);
	}
	
	public LeagueListDTO[] summonerLeagues;

}
