package com.riotgames.leagues.pojo;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class LeagueListDTO extends RiotObject
{
	public LeagueListDTO(TypedObject to)
	{
		super(to);
	}
	public LeagueItemDTO entries;
	public String queue;
	public String name;
	public String tier;
	public String requestorsRank;
	public String requestorsName;

}
