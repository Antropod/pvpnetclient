package com.riotgames.leagues.pojo;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class LeagueItemDTO extends RiotObject
{

	public LeagueItemDTO(TypedObject to)
	{
		super(to);
		// TODO Auto-generated constructor stub
	}
	MiniSeriesDTO miniSeries;
	public int previousDayLeaguePosition;
	public int playerOrTeamId;
	public double lastPlayed;
	public boolean hotStreak;
	public boolean freshBlood;
	public String tier;
	public int leaguePoints;
	public boolean inactive;
	public String rank;
	public boolean veteran;
	public String queueType;
	public int losses;
	public String playerOrTeamName;
	public int wins;
	
	
}
