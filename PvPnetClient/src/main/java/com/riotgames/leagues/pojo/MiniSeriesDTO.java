package com.riotgames.leagues.pojo;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.RiotObject;

public class MiniSeriesDTO extends RiotObject
{
	public MiniSeriesDTO(TypedObject to)
	{
		super(to);
	}

	public int target;
	public int losses;
	public double timeLeftToPlayMillis;
	public int wins;
}
