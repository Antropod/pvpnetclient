package com.github.kolpa.res;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import javax.imageio.ImageIO;

import com.flagstone.transform.Movie;
import com.flagstone.transform.MovieTag;
import com.flagstone.transform.image.DefineImage2;
import com.flagstone.transform.image.DefineJPEGImage2;
import com.flagstone.transform.util.image.BufferedImageEncoder;

import net.antropod.client.Globals;

public class Extractor {
	public Extractor(){
		File resPath = new File(Globals.lolAirDir.toString() + "\\assets\\imagePacks");
		for (File file : resPath.listFiles()){
		//File file = new File(resPath + "\\ImagePack_spells.swf");
			System.out.println("getting file : " + file.toString());
			ArrayList<BufferedImage> images = getJpeg(file);
			for (BufferedImage png : getPng(file))
				images.add(png);
			for (int i = 0; i < images.size(); i++) {
				BufferedImage img = images.get(i);
				try {
					ImageIO.write(img, "png", new File(file.toString() + "_" + i + ".png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private static ArrayList<BufferedImage> getJpeg(File res) {
		ArrayList<BufferedImage> tmpimg = new ArrayList<BufferedImage>();
		Movie m = new Movie();
		try {
			m.decodeFromFile(res);
			for (MovieTag re : m.getObjects()) {
				if (re instanceof DefineJPEGImage2) {
					DefineJPEGImage2 img = (DefineJPEGImage2) re;
					BufferedImage tmp = ImageIO.read(new ByteArrayInputStream(img.getImage()));
					tmpimg.add(tmp);
				}
			}
			return tmpimg;
		} catch (DataFormatException | IOException e) {
			e.printStackTrace();
		}
		return tmpimg;
		
	}
	private static ArrayList<BufferedImage> getPng(File res) {
		BufferedImageEncoder enc = new BufferedImageEncoder();
		ArrayList<BufferedImage> tmpimg = new ArrayList<BufferedImage>();
		Movie m = new Movie();
		try {
			m.decodeFromFile(res);
			for (MovieTag re : m.getObjects()) {
				if (re instanceof DefineImage2) {
					DefineImage2 img = (DefineImage2) re;
					enc.setImage(img);
					BufferedImage tmp = enc.getBufferedImage();
					tmpimg.add(tmp);
				}
			}
			return tmpimg;
		} catch (DataFormatException | IOException e) {
			e.printStackTrace();
		}
		return tmpimg;
		
	}
	


}
