package net.antropod.client.serverdata;

import java.util.ArrayList;

import net.antropod.client.resources.GameMaps;

import com.riotgames.platform.game.map.GameMap;
import com.riotgames.platform.matchmaking.GameQueueConfig;

/**
 * 
 * @author Antropod
 * 
 */
public class MMQProvider
{
	private GameQueueConfig[] GQCs;

	public enum QueueType
	{
		RANKED_GAME, RANKED_TEAM_GAME, NORMAL_GAME, COOP_GAME
	}

	public MMQProvider(GameQueueConfig[] gqc)
	{
		GQCs = gqc;
	}

	public Integer[] getPickTypeIdsForMapAndType(GameMap m, QueueType gt)
	{
		ArrayList<Integer> a = new ArrayList<>();
		if (gt == QueueType.RANKED_GAME)
		{
			for (GameQueueConfig g : GQCs)
			{
				if (g.ranked && (!g.type.contains("TEAM")||g.type.contains("SOLO")))
					for (int i : g.supportedMapIds)
					{
						if (i == m.mapId)
						{
							a.add(g.gameTypeConfigId);
						}
					}
			}
		}
		else if (gt == QueueType.RANKED_TEAM_GAME)
		{
			for (GameQueueConfig g : GQCs)
			{
				if (g.ranked && (g.type.contains("TEAM")&&!g.type.contains("SOLO")))
					for (int i : g.supportedMapIds)
					{
						if (i == m.mapId)
						{
							a.add(g.gameTypeConfigId);
						}
					}
			}
		}
		else if (gt == QueueType.NORMAL_GAME)
		{
			for (GameQueueConfig g : GQCs)
			{
				if (!g.ranked && (g.type.contains("UNRANKED")||g.type.contains("NORMAL")))
					for (int i : g.supportedMapIds)
					{
						if (i == m.mapId)
						{
							a.add(g.gameTypeConfigId);
						}
					}
			}
		}
		else if (gt == QueueType.COOP_GAME)
		{
			for (GameQueueConfig g : GQCs)
			{
				if (!g.ranked && (g.type.contains("BOT")||g.type.contains("COOP")))
					for (int i : g.supportedMapIds)
					{
						if (i == m.mapId)
						{
							a.add(g.gameTypeConfigId);
						}
					}
			}
		}
		return a.toArray(new Integer[0]);
	}
	
	public QueueType[] getQueueTypesForMap(GameMap m)
	{
		if(m.mapId==GameMaps.SUMMONERS_RIFT)
		{
			return new QueueType[]{QueueType.RANKED_GAME,QueueType.RANKED_TEAM_GAME,QueueType.NORMAL_GAME,QueueType.COOP_GAME};
		}
		else if(m.mapId==GameMaps.THE_TWISTED_TREELINE)
		{
			return new QueueType[]{QueueType.RANKED_TEAM_GAME,QueueType.NORMAL_GAME,QueueType.COOP_GAME};
		}
		else if(m.mapId==GameMaps.THE_CRYSTAL_SCAR)
		{
			return new QueueType[]{QueueType.NORMAL_GAME,QueueType.COOP_GAME};
		}
		return new QueueType[]{};
	}
	
	public GameQueueConfig getQueue(GameMap m, QueueType gt, int pickType)
	{
		if (gt == QueueType.RANKED_GAME)
		{
			for (GameQueueConfig g : GQCs)
			{
				if (g.gameTypeConfigId==pickType&&g.ranked && (!g.type.contains("TEAM")||g.type.contains("SOLO")))
					for (int i : g.supportedMapIds)
					{
						if (i == m.mapId)
						{
							return g;
						}
					}
			}
		}
		else if (gt == QueueType.RANKED_TEAM_GAME)
		{
			for (GameQueueConfig g : GQCs)
			{
				if (g.gameTypeConfigId==pickType&&g.ranked && (g.type.contains("TEAM")&&!g.type.contains("SOLO")))
					for (int i : g.supportedMapIds)
					{
						if (i == m.mapId)
						{
							return g;
						}
					}
			}
		}
		else if (gt == QueueType.NORMAL_GAME)
		{
			for (GameQueueConfig g : GQCs)
			{
				if (g.gameTypeConfigId==pickType&&!g.ranked && (g.type.contains("UNRANKED")||g.type.contains("NORMAL")))
					for (int i : g.supportedMapIds)
					{
						if (i == m.mapId)
						{
							return g;
						}
					}
			}
		}
		else if (gt == QueueType.COOP_GAME)
		{
			for (GameQueueConfig g : GQCs)
			{
				if (g.gameTypeConfigId==pickType&&!g.ranked && (g.type.contains("BOT")||g.type.contains("COOP")))
					for (int i : g.supportedMapIds)
					{
						if (i == m.mapId)
						{
							return g;
						}
					}
			}
		}
		return null;
	}

}
