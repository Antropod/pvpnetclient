package net.antropod.client.serverdata;

import com.riotgames.platform.game.GameTypeConfigDTO;

/**
 * 
 * @author Antropod
 *
 */
public class GTCProvider
{
	
	private GameTypeConfigDTO[] GTCs;
	public GTCProvider(GameTypeConfigDTO[] gtc)
	{
		GTCs=gtc;
	}
	
	public GameTypeConfigDTO getConfigById(int i)
	{
		for(GameTypeConfigDTO g:GTCs)
			if(g.id==i)
				return g;
		return null;
	}
	
	/**
	 * Searches for the pickMode and returns it's ban timer duration.
	 * 
	 * @param pickmodeId
	 *            The ID of the pick mode
	 * @return The duration of the pick timer.
	 */
	public int getPreChampSelectionTime(int pickModeId)
	{
		int j = 0;
		for (int i = 0; i < GTCs.length; i++)
		{
			if (GTCs[i].id == pickModeId)
			{
				j = GTCs[i].banTimerDuration;
				break;
			}
		}
		return j - 3;
	}

	/**
	 * Searches for the pickMode and returns it's pick timer duration.
	 * 
	 * @param pickmodeId
	 *            The ID of the pick mode
	 * @return The duration of the pick timer.
	 */
	public int getChampSelectionTime(int pickModeId)
	{
		int j = 0;
		for (int i = 0; i < GTCs.length; i++)
		{
			if (GTCs[i].id == pickModeId)
			{
				j = GTCs[i].mainPickTimerDuration;
				break;
			}
		}
		return j - 3;
	}

	/**
	 * Searches for the pickMode and returns it's post pick timer duration.
	 * 
	 * @param pickModeId
	 *            The ID of the pick mode
	 * @return The duration of the post pick timer.
	 */
	public int getPostChampSelectionTime(int pickModeId)
	{
		int j = 0;
		for (int i = 0; i < GTCs.length; i++)
		{
			if (GTCs[i].id == pickModeId)
			{
				j = GTCs[i].postPickTimerDuration;
				break;
			}
		}
		return j - 3;
	}
	
}
