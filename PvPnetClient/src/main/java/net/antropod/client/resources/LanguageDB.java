package net.antropod.client.resources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import net.antropod.util.Language;
/**
 * This class provides access to the language database
 * @author Antropod
 *
 */
public class LanguageDB
{
	/** The "Connection" to the database */
	private static Connection conn;
	/** */
	private static Statement stat;
	/** The currently selected language*/
	private static Language lang;

	static
	{
		try
		{
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:Languages.sqlite");
			stat = conn.createStatement();
			lang=Language.getLanguageByLocale("en_US");
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * Changes the language
	 * @param l The new language
	 */
	public static void setLanguage(Language l)
	{
		lang=l;
	}
	/**
	 * Searches for the given key in the language database
	 * @param key
	 * @return
	 */
	public static String getLocalizedString(String key)
	{
		key=key.toUpperCase();
		String s="**Missing String**"+"("+key+")";
		try {

			 
		    ResultSet rs = stat.executeQuery("SELECT Value FROM "+lang.getLocale()+" WHERE Key=\""+key+"\";");
		    while (rs.next())
		    {
		      s= rs.getString("Value");
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
}
