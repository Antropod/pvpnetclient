package net.antropod.client.resources;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import net.antropod.client.Globals;

public class ImageAssets 
{
	//public static String path="C:/Program Files (x86)/LoL/League of Legends/";
	public static BufferedImage IP_ICON,RP_ICON;
	public static BufferedImage PRIVATE_GAME_ICON;
	public static BufferedImage LOGIN_BG,STANDARD_BG,CHAMP_SELECT_BG;
	public static BufferedImage LOL_LOGO, LOL_NAME_LOGO;
	
	public static BufferedImage MAP1,MAP7,MAP8,MAP10;
	public static BufferedImage MINIMAP1,MINIMAP7,MINIMAP8,MINIMAP10;
	
	public static ImageIcon PVPNET_ONLINE,PVPNET_AWAY,PVPNET_BUSY;

	private static HashMap<Integer, BufferedImage> summonerIconCache;
	private static HashMap<Integer, BufferedImage> championIconCache;
	
	static
	{
		try {
			IP_ICON=ImageIO.read(new File("img/ip_icon.png"));
			RP_ICON=ImageIO.read(new File("img/rp_icon.png"));
			PRIVATE_GAME_ICON=ImageIO.read(new File("img/lock_icon.png"));
			LOGIN_BG=ImageIO.read(new File(Globals.lolAirDir,"assets/images/champions/Thresh_Splash_0.jpg"));
			STANDARD_BG=ImageIO.read(new File("img/bg/std_bg.jpg"));
			CHAMP_SELECT_BG=ImageIO.read(new File("img/bg/cs_bg.jpg"));
			LOL_LOGO=ImageIO.read(new File("img/tray_32.png"));
            LOL_NAME_LOGO=ImageIO.read(new File("img/lol_name_logo.png"));
			PVPNET_ONLINE=new ImageIcon(ImageIO.read(new File("img/pvpnet/chat_online.png")));
			PVPNET_AWAY=new ImageIcon(ImageIO.read(new File("img/pvpnet/chat_away.png")));
			PVPNET_BUSY=new ImageIcon(ImageIO.read(new File("img/pvpnet/chat_busy.png")));
			MAP1=ImageIO.read(new File("img/maps/1.png"));
			MAP7=ImageIO.read(new File("img/maps/7.png"));
			MAP8=ImageIO.read(new File("img/maps/8.png"));
			MAP10=ImageIO.read(new File("img/maps/10.png"));
			MINIMAP1=ImageIO.read(new File("img/maps/mm1.png"));
			MINIMAP7=ImageIO.read(new File("img/maps/mm7.png"));
			MINIMAP8=ImageIO.read(new File("img/maps/mm8.png"));
			MINIMAP10=ImageIO.read(new File("img/maps/mm10.png"));

			summonerIconCache=new HashMap<>();
			championIconCache=new HashMap<>();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static BufferedImage getSummonerIcon(int i)
	{
		BufferedImage bimg= summonerIconCache.get(i);
		if(bimg!=null)
			return bimg;
		
		try {
			bimg=ImageIO.read(new File("img/summoner_icons/"+i+".jpg"));
			summonerIconCache.put(i, bimg);
			return bimg;
		} catch (IOException e) {
			if(i!=0)
			return getSummonerIcon(0);
			else return null;
		}
	}

	
	public static BufferedImage getChampionIcon(int id)
	{
		if(id<1)return null;
		BufferedImage bimg=championIconCache.get(id);
		if(bimg!=null)
			return bimg;
		try {
			bimg=ImageIO.read(new File(Globals.lolAirDir,"assets/images/champions/"+SQLDatabase.getChampionIconPathById(id)));
			championIconCache.put(id, bimg);
			return bimg;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Image getChampionIcon(int id, int size)
	{
		return getChampionIcon(id).getScaledInstance(size, size, Image.SCALE_SMOOTH);
	}
	
	public static Image getSummonerSpellIcon(int id, int size)
	{
		try {
			if(id<0)return null;
			return ImageIO.read(new File("img/summoner_spells/"+id+".png")).getScaledInstance(size, size, Image.SCALE_SMOOTH);
		} catch (IOException e) {
			e.printStackTrace();
		};
		return null;
	}
	
	public static BufferedImage getSummonerSpellIcon(int id)
	{
		try {
			if(id<0)return null;
			return ImageIO.read(new File("img/summoner_spells/"+id+".png"));
		} catch (IOException e) {
			e.printStackTrace();
		};
		return null;
	}
}
