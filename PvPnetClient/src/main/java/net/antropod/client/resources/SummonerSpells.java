package net.antropod.client.resources;

public enum SummonerSpells
{
	//OBSERVER(0,99,"", false),
	CLEANSE(1,2),
	CLAIRVOYANCE(2,10,"CLASSIC,ODIN"),
	EXHAUST(3,8),
	FLASH(4,12),
	//FORTIFY(5,5,"",false),
	GHOST(6,1),
	HEAL(7,1),
	//RALLY(9,8,"",false),
	REVIVE(10,1,"CLASSIC,ODIN"),
	SMITE(11,1,"CLASSIC,ODIN"),
	TELEPORT(12,2,"CLASSIC"),
	CLARITY(13,6),
	IGNITE(14,8),
	//SURGE(16,1,"",false),
	GARRISON(17,1,"ODIN"),
	//PROMOTE(20,8,"",false),
	BARRIER(21,6);
	
	
	private int id;
	private boolean active;
	private int requiredLevel;
	private String availableModes;

	SummonerSpells(int i, int level)
	{
		this(i, level,"CLASSIC,ARAM,ODIN",true);
	}
	
	SummonerSpells(int i, int level,String modes)
	{
		this(i, level, modes,true);
	}
	
	SummonerSpells(int i, int level,String modes, boolean active)
	{
		id=i;
		this.active=active;
		availableModes=modes;
		requiredLevel=level;
	}
	
	public int getID()
	{
		return id;
	}
	
	public boolean isActive()
	{
		return active;
	}
	
	public int getRequiredLevel()
	{
		return requiredLevel;
	}
	
	public String getAvailableModes()
	{
		return availableModes;
	}
	
}
