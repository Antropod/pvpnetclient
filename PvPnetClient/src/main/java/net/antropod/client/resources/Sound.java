package net.antropod.client.resources;

import java.io.File;
import java.net.URL;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.FactoryRegistry;
import javazoom.jl.player.advanced.AdvancedPlayer;
import javazoom.jl.player.advanced.PlaybackEvent;
import javazoom.jl.player.advanced.PlaybackListener;
import net.antropod.client.Globals;

public class Sound extends PlaybackListener
{
	public static Sound
	// Music
			LOGIN_LOOP = new Sound(Globals.getAirContentLocation() + "sounds/ambient/LoginScreenLoop.mp3", 0),
			LOGIN_INTRO = new Sound(Globals.getAirContentLocation() + "sounds/ambient/LoginScreenIntro.mp3", 0),
			CHAMP_SLCT_DRAFT = new Sound("sound/ambient/ChmpSlct_DraftMode.mp3", 0),
			CHAMP_SLCT_BLIND = new Sound("sound/ambient/ChmpSlct_BlindPick.mp3", 0),

			// Sounds
			CHAMP_SLCT_YOUR_TURN = new Sound(Globals.getAirContentLocation() + "sounds/champSelect/yourturn.mp3", 1), CHAMP_SLCT_PHASE_CHANGED = new Sound(Globals.getAirContentLocation()
					+ "sounds/champSelect/phasechangedrums2.mp3", 1),
			CHAMP_SLCT_BAN = new Sound(Globals.getAirContentLocation() + "sounds/newSounds/air_button_press_12.mp3", 1),
			CHAMP_SLCT_LOCK_IN = new Sound(Globals.getAirContentLocation() + "sounds/champSelect/lockinchampion.mp3", 1), CHAMP_SLCT_CD10SEC = new Sound(Globals.getAirContentLocation()
					+ "sounds/champSelect/countdown10seconds.mp3", 1),
			CHAMP_SLCT_EXIT_CHAMP_SELECT = new Sound(Globals.getAirContentLocation() + "sounds/champSelect/exitchampionselect.mp3", 1),
			MMQ_MATCH_FOUND = new Sound(Globals.getAirContentLocation() + "sounds/matchmakingqueued.mp3", 1);

	private static Sound[] channels = new Sound[4];

	private String fileName;
	private int channel;
	private AdvancedPlayer player;
	public boolean stopped;

	private Sound(String file, int channel)
	{
		fileName = file;
		this.channel = channel;
		stopped = false;

	}

	public static Sound getChampionSelectionQuote(int i)
	{

		return (new Sound(Globals.getAirContentLocation() + "sounds/en_US/champions/" + SQLDatabase.getChampionNameByID(i) + ".mp3", 2));
	}

	public boolean isStopped()
	{
		return stopped;
	}

	public void play()
	{
		try
		{

			String urlAsString = "file:///" + (new File(this.fileName).getCanonicalPath());

			this.player = new AdvancedPlayer(new URL(urlAsString).openStream(), FactoryRegistry.systemRegistry().createAudioDevice());
			player.setPlayBackListener(this);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		if (channel == 0 && !Globals.properties.isMusicEnabled())
			return;
		if (channels[channel] != null && !channels[channel].isStopped())
			channels[channel].stop();
		new Thread()
		{
			public void run()
			{
				try
				{
					player.play();
				} catch (JavaLayerException e)
				{
					e.printStackTrace();
				}
			}
		}.start();
		channels[channel] = this;

	}

	public void stop()
	{
		player.stop();
	}

	public static void stopMusic()
	{
		stopChannel(0);
	}

	public void playbackStarted(PlaybackEvent playbackEvent)
	{
		stopped = false;
	}

	public void playbackFinished(PlaybackEvent playbackEvent)
	{
		stopped = true;
	}

	public static void stopChannel(int channel)
	{
		if (channels[channel] != null)
			channels[channel].stop();
		channels[channel] = null;
	}
}