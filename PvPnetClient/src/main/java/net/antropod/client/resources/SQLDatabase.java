package net.antropod.client.resources;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

import net.antropod.client.Globals;

public class SQLDatabase 
{
	private static Connection conn;
	private static Statement stat;
	static{
		try {
		    Class.forName("org.sqlite.JDBC");
		    conn = DriverManager.getConnection("jdbc:sqlite:"+Globals.lolAirDir.getAbsolutePath()+"/assets/data/gameStats/gameStats_en_US.sqlite");
		    stat = conn.createStatement();
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized static String getChampionNameByID(int i)
	{
		String s="Missingno.";
		try {

			 
		    ResultSet rs = stat.executeQuery("select name from champions where id="+i+";");
		    while (rs.next())
		    {
		      return rs.getString("name");
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}

	
	public synchronized static int getChampionIDByName(String s)
	{
		try {

			 
		    ResultSet rs = stat.executeQuery("select id from champions where name=\""+s+"\"");
		    while (rs.next())
		    {
		      return rs.getInt("id");
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public synchronized static String getChampionIconPathById(int i)
	{
		String s="Missingno.";
		try {

			 
		    ResultSet rs = stat.executeQuery("select iconPath from champions where id="+i+";");
		    while (rs.next())
		    {
		      return rs.getString("iconPath");
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
}
