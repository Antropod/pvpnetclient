package net.antropod.client.resources;

import java.util.ArrayList;

import com.riotgames.platform.game.map.GameMap;

public class GameMaps
{
	private static GameMap[] maps;

	public static final int SUMMONERS_RIFT=1;
	public static final int THE_PROVING_GROUNDS=7;
	public static final int THE_CRYSTAL_SCAR=8;
	public static final int THE_TWISTED_TREELINE=10;
	
	private GameMaps()
	{

	}

	public static void load()
	{
		maps = new GameMap[11];
		maps[1] = createMap(1, "SummonersRift", "Summoner\'s Rift", "Desc SR", 10);
		maps[2] = createMap(2, "SummonersRiftWinter", "Summoner\'s Rift (Autumn)", "Desc SR2", 10);
		maps[4] = createMap(4, "TwistedTreeline", "The Twisted Treeline", "Desc TT", 6);
		maps[7] = createMap(7, "ProvingGroundsARAM", "The Proving Grounds", "Desc PG", 10);
		maps[8] = createMap(8, "CrystalScar", "The Crystal Scar", "Desc CS", 10);
		maps[10] = createMap(10, "TwistedTreeline", "The Twisted Treeline!", "Desc TT2", 6);
	}

	public static GameMap getMap(int id)
	{
		return maps[id];
	}

	private static GameMap createMap(int id, String name, String dispName, String desc, int maxPlayers)
	{
		GameMap map = new GameMap();
		map.mapId = id;
		map.name = name;
		map.displayName = dispName;
		map.description = desc;
		map.totalPlayers = maxPlayers;
        map.minCustomPlayers = 1;
		return map;
	}
}
