package net.antropod.client.chat.components;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import net.antropod.client.chat.IChatListElement;
import net.antropod.client.components.JClientWindow;
import net.antropod.client.components.JIcon;
import net.antropod.client.resources.ImageAssets;

public class ChatListRenderer extends JPanel implements ListCellRenderer<IChatListElement> 
{
	private JIcon pIcon;
	private JLabel summonerName;
	
	public ChatListRenderer()
	{
		super(null);
		pIcon = new JIcon();
		pIcon.setSize(64, 64);
		pIcon.setLocation(3, 3);
		summonerName=new JLabel("**placeholder");
		summonerName.setBounds(90,10,200,20);
		summonerName.setFont(summonerName.getFont().deriveFont(Font.BOLD,15f));
		setSize(getWidth(), 70);
		setBorder(BorderFactory.createBevelBorder(1));
		add(pIcon);
		add(summonerName);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends IChatListElement> arg0, IChatListElement e, int arg2, boolean arg3, boolean arg4)
	{
		Object[] o=e.getAdditionalInformation();
		pIcon.setIcon(ImageAssets.getSummonerIcon((Integer)o[0]));
		summonerName.setText((String)o[1]);
		setPreferredSize(new Dimension(JClientWindow.FRAME_WIDTH/4-35, e.isVisible()?getHeight():0));
		return this;
	}

	

}
