package net.antropod.client.chat;

import java.awt.Component;

import javax.swing.JPanel;

public interface IChatListElement
{
	public boolean isVisible();
	public String getDisplayName();
	public Object[] getAdditionalInformation();
}
