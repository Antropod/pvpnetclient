package net.antropod.client.chat;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class GameInvite
{
	public int inviteId;
	public String userName;
	public String gameType;
	public int mapId;
	public int queueId;
	public String gameMode;
	public String gameDifficulty="";
	public GameInvite(String s)
	{
		try
		{
			DocumentBuilder db=DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc=db.parse(new InputSource(new StringReader(s)));

			Element body=(Element)doc.getFirstChild();
			
			inviteId=Integer.parseInt(body.getElementsByTagName("inviteId").item(0).getTextContent());
			userName=body.getElementsByTagName("userName").item(0).getTextContent();
			gameType=body.getElementsByTagName("gameType").item(0).getTextContent();
			mapId=Integer.parseInt(body.getElementsByTagName("mapId").item(0).getTextContent());
			queueId=Integer.parseInt(body.getElementsByTagName("queueId").item(0).getTextContent());
			gameMode=body.getElementsByTagName("gameMode").item(0).getTextContent();
			gameDifficulty=body.getElementsByTagName("gameDifficulty").item(0).getTextContent();
			
		} catch (ParserConfigurationException | SAXException | IOException e)
		{
			e.printStackTrace();
		}
	}
	public GameInvite()
	{
		
	}
	public Message toXMPPMessage()
	{
		Message m =new Message();
		m.setSubject("GAME_INVITE");
		m.setBody(toString());
		m.setType(Type.normal);
		return m;
	}
	
	public String toString()
	{
		return "<body>" +
				"<inviteId>"+inviteId+"</inviteId>" +
				"<userName>"+userName+"</userName>" +
				"<gameType>"+gameType+"</gameType>" +
				"<mapId>"+mapId+"</mapId>" +
				"<queueId>"+queueId+"</queueId>" +
				"<gameMode>"+gameMode+"</gameMode>" +
				"<gameDifficulty>"+gameDifficulty+"</gameDifficulty>" +
				"</body>";
	}
}
