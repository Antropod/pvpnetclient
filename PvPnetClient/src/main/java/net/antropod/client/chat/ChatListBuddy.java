package net.antropod.client.chat;

import java.awt.Component;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import net.antropod.client.Globals;
import net.antropod.client.components.JIcon;
import net.antropod.client.resources.ImageAssets;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.packet.Presence;

public class ChatListBuddy implements IChatListElement
{

	private boolean visible;
	private RosterEntry entry;
	private Presence presence;

	public ChatListBuddy(RosterEntry r, Presence p)
	{
		visible = true;
		entry = r;
		presence = p;

	}

	public void setVisible(boolean b)
	{
		visible = b;
	}

	public boolean isVisible()
	{
		if(presence.getMode()!=Presence.Mode.chat&&presence.getMode()!=Presence.Mode.available&&presence.getMode()!=Presence.Mode.away&&presence.getMode()!=Presence.Mode.dnd)
			return false;
		return visible;
	}
	
	public String getUser()
	{
		return entry.getUser();
	}

	@Override
	public String getDisplayName()
	{
		return entry.getName();
	}

	@Override
	public Object[] getAdditionalInformation()
	{
		Object[] o= new Object[2];
		o[0]=getSummonerIcon();
		o[1]=entry.getName();
		return o;
	}

	public int getSummonerIcon()
	{
		if(presence.getStatus()==null||presence.getStatus().equals(""))return 0;
		try
		{
			Pattern p = Pattern.compile("<profileIcon>([0-9]*)</profileIcon>", Pattern.DOTALL);

			Matcher matcher = p.matcher(presence.getStatus());
			matcher.find();
			return Integer.parseInt(matcher.group(1));
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return 0;
		}
	}
	
	public void setPresence(Presence p)
	{
		presence=p;
	}

}
