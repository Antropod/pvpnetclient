package net.antropod.client.chat;

import java.util.HashMap;

import javax.swing.JOptionPane;

import net.antropod.client.Globals;
import net.antropod.client.chat.components.JChat;
import net.antropod.util.Delegate;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.muc.MultiUserChat;
import com.gvaneyck.rtmp.DummySSLSocketFactory;

public class PvpNetConnection
{
	public static final String[] CHAT_SERVER = { "chat.na1.lol.riotgames.com", "chat.eu.lol.riotgames.com", "chat.eun1.lol.riotgames.com" };
	private static final String JABBER_DOMAIN="pvp.net";
	private Connection connection;
	private Presence presence;
	
	// status related vars
	private int profileIcon, level, wins;
	private String statusMsg, gameStatus;
	
	private HashMap<Chat, JChat> chatComponents;

	public PvpNetConnection(String user, String pass, String region, int profileIcon, int level, String statusMsg, int wins) throws XMPPException
	{
		this.profileIcon = profileIcon;
		this.level = level;
		this.wins = wins;
		this.statusMsg = statusMsg;
		this.gameStatus = "outOfGame";
		chatComponents=new HashMap<>();
		ConnectionConfiguration cc = new ConnectionConfiguration(getChatServerForRegion(region), 5223, "pvp.net");
		cc.setSecurityMode(ConnectionConfiguration.SecurityMode.enabled);
		cc.setSocketFactory(new DummySSLSocketFactory());

		connection = new XMPPConnection(cc);
		connection.connect();

		connection.login(user, "AIR_" + pass);
		presence = new Presence(Presence.Type.available);
		presence.setStatus("<body><profileIcon>" + profileIcon + "</profileIcon><statusMsg>" + statusMsg + "</statusMsg><level>" + level + "</level><wins>" + wins
				+ "</wins><tier>PLATINUM</tier><skinname>Lux</skinname><gameStatus>" + gameStatus + "</gameStatus></body>");
		presence.setPriority(24);
		presence.setMode(Presence.Mode.chat);
		connection.sendPacket(presence);
		connection.addPacketListener(new PacketListener()
		{

			@Override
			public void processPacket(Packet packet)
			{
				processMatchmakingInvite(new GameInvite(((Message)packet).getBody()));
			}
			
		}, new PacketFilter(){

			@Override
			public boolean accept(Packet packet)
			{
					return (packet instanceof Message)&&(((Message)packet).getSubject().equalsIgnoreCase("GAME_INVITE"));
			}
			
		});
		
	}

	public void updateStatus()
	{
		presence.setStatus("<body><profileIcon>" + profileIcon + "</profileIcon><statusMsg>" + statusMsg + "</statusMsg><level>" + level + "</level><wins>" + wins
				+ "</wins><tier>PLATINUM</tier><skinname>Lux</skinname><gameStatus>" + gameStatus + "</gameStatus></body>");
		connection.sendPacket(presence);
	}

	public void setProfileIcon(int id)
	{

		profileIcon = id;
	}

	public void addPacketHandler(PacketListener pl)
	{
		connection.addPacketListener(pl, new PacketFilter()
		{

			@Override
			public boolean accept(Packet packet)
			{
				return true;
			}

		});
	}

	public Roster getRoster()
	{
		return connection.getRoster();
	}
	
	public RosterEntry getEntryByUser(String s)
	{
		return connection.getRoster().getEntry(s.replace("/xiff", ""));
		
	}
	
	public void sendMessageTo(String user, Message m)
	{
		try
		{
			getOrCreatePrivateChat(user).sendMessage(m);
		} catch (XMPPException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Chat getOrCreatePrivateChat(String user)
	{
		user=user.replace("/xiff","");
		if(connection.getChatManager().getThreadChat(user.replace("/xiff",""))==null)
		{
			JChat c=new JChat();
			Chat chat=connection.getChatManager().createChat(user, user, c);
			chatComponents.put(chat, c);
			return chat;
		}
		return connection.getChatManager().getThreadChat(user);
	}
	/**
	 * Join a chat room
	 * @param crt The ChatRoomType
	 * @param roomName The room's name
	 * @param password The room's password
	 * @return The chat room
	 */
	public void joinMultiUserChat(final String roomName,final String password, final Delegate callback)
	{
		//This will be  executed in a seperate Thread, as the channel may take a few seconds to be created
		new Thread(){
			public void run()
			{
				System.out.println("Joining '"+roomName+"."+JABBER_DOMAIN+"' with the password '"+password+"'");
				MultiUserChat muc=new MultiUserChat(connection,roomName+"."+JABBER_DOMAIN);
				try
				{
					muc.join(Globals.loginPacket.allSummonerData.summoner.name, password);
				} catch (XMPPException e)
				{
					e.printStackTrace();
				}
				callback.invoke(muc);
			}
		}.start();
	}
	
	private static String getChatServerForRegion(String s)
	{
		switch (s)
		{
			case "NA":
				return CHAT_SERVER[0];
			case "EUW":
				return CHAT_SERVER[1];
			case "EUNE":
				return CHAT_SERVER[2];
		}
		return null;
	}

	public void processMatchmakingInvite(GameInvite g)
	{
		if(JOptionPane.showConfirmDialog(Globals.mainFrame, g.toString(),"Game Invite",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION)
		Globals.client.acceptInviteForMatchmakingGame(g.inviteId);
	}
}
