package net.antropod.client.chat.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import net.antropod.client.Globals;
import net.antropod.client.chat.ChatListBuddy;
import net.antropod.client.chat.GameInvite;
import net.antropod.client.chat.IChatListElement;

public class JChatList extends JList<ChatListBuddy> implements MouseListener, ActionListener
{
	private DefaultListModel<ChatListBuddy> listModel;
	private JPopupMenu popupMenu;
	
	private JMenuItem inviteToGameItem;
	
	public JChatList()
	{
		inviteToGameItem=new JMenuItem("Invite to game");
		inviteToGameItem.addActionListener(this);
		
		listModel=new DefaultListModel<>();
		this.setModel(listModel);
		setCellRenderer(new ChatListRenderer());
		addMouseListener(this);
		popupMenu=new JPopupMenu();
		popupMenu.add(inviteToGameItem);
		
		popupMenu.setLightWeightPopupEnabled(false);
		
	}
	
	public void addChatListElement(ChatListBuddy e)
	{
		listModel.addElement(e);
	}
	
	public void mousePressed(MouseEvent e)  {check(e);}
	public void mouseReleased(MouseEvent e) {check(e);}

	public void check(MouseEvent e) {
	    if (e.getButton()==MouseEvent.BUTTON3) { //if the event shows the menu
	        setSelectedIndex(locationToIndex(e.getPoint())); //select the item
	        popupMenu.show(this, e.getX(), e.getY()); //and show the menu
	        if(popupMenu.getX()+popupMenu.getWidth()<=Globals.mainFrame.getWidth())
	        {
	        	popupMenu.setLocation(Globals.mainFrame.getX()+Globals.mainFrame.getWidth()-popupMenu.getWidth()+1, getLocationOnScreen().y+e.getY());
	        }
	    }
	}

	@Override
	public void mouseClicked(MouseEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
	}
}
