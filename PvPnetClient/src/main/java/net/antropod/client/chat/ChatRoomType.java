package net.antropod.client.chat;
/**
 * This class provides the prefixes of the chat rooms. The name of the chat rooms have the following format: "ChatRoomType + PREFIX_DELIMETER + SHA1(*Chat Rooms Actual Name*)"
 * @author Antropod
 *
 */
public class ChatRoomType
{
    public static final String CHAMPION_SELECT2="c2";

    public static final String PRIVATE="pr";

    public static final String ARRANGING_GAME="ag";

    public static final String PUBLIC="pu";

    public static final String GLOBAL="gl";

    public static final String RANKED_TEAM="tm";

    public static final String QUEUED="aq";

    public static final String POST_GAME="pg";

    public static final String ARRANGING_PRACTICE="ap";

    public static final String PREFIX_DELIMITER="~";

    public static final String CHAMPION_SELECT1="c1";
}
