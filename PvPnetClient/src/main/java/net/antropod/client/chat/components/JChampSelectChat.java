package net.antropod.client.chat.components;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EtchedBorder;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.muc.MultiUserChat;

import net.antropod.client.Globals;
import net.antropod.client.components.ClientLayout;
import net.antropod.client.components.JColorTextPane;
import net.antropod.client.components.JPanel;

public class JChampSelectChat extends JPanel implements ActionListener, PacketListener
{
	public static final int STD_WIDTH = 672, STD_HEIGHT = 260;
	private static final Pattern chatPattern = Pattern.compile("\\<\\!\\[CDATA\\[(.*)\\]\\]\\>");//Messages are sent in this format:  <![CDATA[*Actual Message*]]>
	private JColorTextPane area;
	private JTextField field;
	private JScrollPane js;
	private MultiUserChat chat;

	public JChampSelectChat()
	{
		setLayout(new ClientLayout(STD_WIDTH, STD_HEIGHT));
		setSize(STD_WIDTH, STD_HEIGHT);

		area = new JColorTextPane();
		area.setEditable(false);
		area.setOpaque(false);
		area.setBackground(new Color(0, 0, 0, 0));// Workaround for a bug in the
													// Nimbus Look And Feel
		js = new JScrollPane(area);
		js.setAutoscrolls(true);
		js.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		js.getViewport().setBackground(new Color(0x343469));// Dark Blue
		add(js, 0, 0, STD_WIDTH, STD_HEIGHT - 40);
		field = new JTextField();
		field.addActionListener(this);
		field.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		field.setBackground(new Color(0x343469));// Dark Blue
		field.setForeground(new Color(0xFFFFFF));
		add(field, 0, STD_HEIGHT - 30, STD_WIDTH, 30);
	}

	public void setChat(MultiUserChat chat)
	{
		this.chat = chat;
		chat.addMessageListener(this);

	}

	public void leaveChatRoom()
	{
		if (chat != null)
		{
			chat.leave();
			chat = null;
			area.setText("");
			field.setText("");
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		if (chat != null)
		{
			try
			{
				chat.sendMessage(" <![CDATA[" + field.getText() + "]]>");
			} catch (XMPPException e)
			{
				e.printStackTrace();
			}
		}
		//area.append(Color.MAGENTA, Globals.loginPacket.allSummonerData.summoner.name + ": ");
		//area.append(Color.WHITE, field.getText() + "\n");
		field.setText("");
	}

	@Override
	public void processPacket(Packet p)
	{
		if (p instanceof Message)
		{
			try
			{
				Message m = (Message) p;
				area.append(Color.CYAN, chat.getOccupant(m.getFrom()).getNick() + ": ");
				System.out.println("Received Message in champion selection: " + m.toString());
				Matcher ma=chatPattern.matcher(m.getBody());
				ma.find();
				area.append(Color.WHITE, ma.group(1) + "\n");
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		} else
		{
			System.out.println("What the hell is this Message??");
		}
	}
}
