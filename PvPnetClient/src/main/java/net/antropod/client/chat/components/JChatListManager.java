package net.antropod.client.chat.components;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import net.antropod.client.Globals;
import net.antropod.client.chat.ChatListBuddy;
import net.antropod.client.chat.PvpNetConnection;
import net.antropod.client.components.ClientLayout;
import net.antropod.client.components.JClientWindow;
import net.antropod.client.components.JPanel;
import net.antropod.client.resources.ImageAssets;
import static net.antropod.client.Globals.pvpnet;

public class JChatListManager extends JPanel implements Runnable, PacketListener, RosterListener
{
	private PvpNetConnection pvpnet;
	
	private JChatList chatList;
	private HashMap<String, ChatListBuddy> buddyList;
	
	public JChatListManager(PvpNetConnection net)
	{
		super();
		pvpnet=net;
		new Thread(this).start();

	}

	public void run()
	{
		buddyList=new HashMap<>();
		chatList=new JChatList();
		pvpnet.addPacketHandler(this);
		
		Roster r= pvpnet.getRoster();
		ArrayList<RosterEntry> e=new ArrayList<>(r.getEntries());
		for(int i=0;i<e.size();i++)
		{
			ChatListBuddy clb=new ChatListBuddy(e.get(i),r.getPresence(e.get(i).getUser().replace("/xiff","")));
			buddyList.put(e.get(i).getUser().replace("/xiff",""), clb);
			chatList.addChatListElement(clb);
		}
		r.addRosterListener(this);
		add(chatList);
	}

	@Override
	public void processPacket(Packet arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void entriesAdded(Collection<String> arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void entriesDeleted(Collection<String> arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void entriesUpdated(Collection<String> arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void presenceChanged(Presence p)
	{
		ChatListBuddy c=buddyList.get(p.getFrom().replace("/xiff",""));
		c.setPresence(p);
		chatList.repaint();
	}

}
