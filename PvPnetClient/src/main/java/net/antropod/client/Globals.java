package net.antropod.client;

import java.awt.KeyboardFocusManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import javax.swing.JOptionPane;

import org.jivesoftware.smack.XMPPException;

import net.antropod.client.chat.PvpNetConnection;
import net.antropod.client.components.JClientWindow;
import net.antropod.client.components.MinimalLoginScreen;
import net.antropod.client.managers.WindowManager;
import net.antropod.client.resources.LanguageDB;
import net.antropod.client.resources.Sound;
import net.antropod.client.serverdata.GTCProvider;
import net.antropod.client.serverdata.MMQProvider;
import net.antropod.plugins.PluginLoader;
import net.antropod.util.GlobalKeyDispatcher;

import com.github.kolpa.res.Extractor;
import com.riotgames.platform.catalog.champion.ChampionDTO;
import com.riotgames.platform.clientfacade.domain.LoginDataPacket;
import com.riotgames.platform.statistics.PlayerStatSummary;
/**
 * Contains all static and globally needed information about the client.
 * 
 * @author Antropod
 * 
 */
public class Globals
{
	// DEBUGGING STUFF
	public static String REGION = "EUW";
	public static String CLIENT_VERSION = "3.02.qwertyuiop";
	public final static int build = 41;
	/**
	 * Let's the client know, if it should print sent and received RTMP packets
	 * to the console
	 */
	public static boolean netDebug = true;
	/** Decides, if debugging information should be printed to the console */
	public static boolean debug = true;
	/*
	 * Constants
	 */

	/** The port on which a maestro-like service will be hosted */
	public static final int MAESTRO_PORT = 8100;

	/*
	 * Errors
	 */
	/** ID of an unknown error */
	public static final int ERROR_UNKNOWN = 0;

	/*
	 * Window Stuff
	 */
	/** The login window */
	public static MinimalLoginScreen loginScreen;
	/** The window that displays almost everything in this application */
	public static JClientWindow mainFrame;
	/** The window's mein manager */
	public static WindowManager winManager;
	/*
	 * Data Providers and controllers
	 */
	/** Match Making Queue Provider */
	public static MMQProvider mmqProvider;
	/** Pick Type Configurations Provider */
	public static GTCProvider gtcProvider;
	/*
	 * Background Stuff
	 */
	/** SWF extractor */
	public static Extractor extractor;
	/** RTMP client */
	public static LoLClient client;
	/** Handles packets for the RTMP Client */
	public static PacketHandler pp;
	/** The XMPP Connection to the PvP.net */
	public static PvpNetConnection pvpnet;
	/** The game client interface */
	public static GameClient gameClient;
	/*
	 * Que Stuff
	 */
	/** in Queue Boolean **/
	public static boolean IN_QUEUE = false;
	/** Queue place int **/
	public static int QUEUE_PLACE = 0;
	// public static boolean IN_CHAMP_SELECT = false;
	public static boolean GAME_START_REQUESTED = false;
	/**
	 * A random number generator, which can be used for trivial and unnecessary
	 * tasks
	 */
	public static final Random RNG = new Random();

	/**
	 * The LoginDataPacket, which will be acquired shortly after the login. It
	 * contains useful information about the summoner and the server
	 * configuration.
	 */
	public static LoginDataPacket loginPacket;
	/** Stores all champions, the summoner can pick during champion selection */
	public static ChampionDTO[] availableChampions;
	/** Stores the data of all champions */
	public static ChampionDTO[] allChampions;
	/** Contains the client's local configuration */
	public static ClientProperties properties;
	/** The directory where "lol.launcher.exe" resides */
	public static File lolDir;
	/** The directory where "LolClient.exe" resides */
	public static File lolAirDir;
	/** The directory where "League of Legends.exe" resides */
	public static File lolGameClientDir;
	private static boolean init=false;
	protected static void init()
	{
		if(init)
			return;
		// Create Paths
		loadProperties();
		lolDir=new File(properties.getLoLLocation());
		String lolAir = "RADS/projects/lol_air_client/releases/";

		File versions = new File(lolDir,lolAir);
		int maxGame = 0;
		for (File f : versions.listFiles())
		{
			String[] parts = f.getName().split("\\.");
			if (parts.length != 4)
				continue;

			int ver = Integer.parseInt(parts[3]);
			if (ver > maxGame)
				maxGame = ver;
		}
		lolAirDir = new File(versions,"0.0.0." + maxGame + "/deploy/");

		String lolGamePath = "RADS/solutions/lol_game_client_sln/releases/";

		versions = new File(lolDir,lolGamePath);
		maxGame = 0;
		for (File f : versions.listFiles())
		{
			String[] parts = f.getName().split("\\.");
			if (parts.length != 4)
				continue;

			int ver = Integer.parseInt(parts[3]);
			if (ver > maxGame)
				maxGame = ver;
		}
		lolGameClientDir = new File(versions,"0.0.0." + maxGame + "/deploy/");
		try
		{
			gameClient = new GameClient(new File(lolGameClientDir, "League of Legends.exe"));
		} catch (IllegalArgumentException | FileNotFoundException e)
		{
			e.printStackTrace();
		}
		mainFrame=new JClientWindow();
		winManager=new WindowManager(mainFrame);
		loginScreen=new MinimalLoginScreen(CLIENT_VERSION,REGION,properties.getSummonerName());
		
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(new GlobalKeyDispatcher());
		
		init=true;
	}
	public static void startExtractor() {
		extractor = new Extractor();
	}
	/**
	 * Loads the client's local configuration.
	 */
	public static void loadProperties()
	{
		try
		{
			if(properties==null)
			{
				properties = new ClientProperties();
			}

		} catch (Exception e)
		{

		}
	}
	
	/**
	 * 
	 * @return The absolute path to the directory, where images, sounds and
	 *         other stuff resides for the Air Client.
	 */
	public static String getAirContentLocation()
	{
		return lolAirDir.getAbsolutePath()+ "/assets/";
	}
	/**
	 * Sets the RTMP connection and attaches a PacketHandler to it.
	 * @param rtmp The RTMP connection to riots server.
	 */
	public static void setRTMP(LoLClient rtmp)
	{
		if (client == null)
		{
			client = rtmp;
			client.debug = netDebug;
			pp = new PacketHandler(client);
			pp.start();
			client.setReceiveHandler(pp);
		}
	}
	/**
	 * Sets the pvpnet connection.
	 * @param pvp the PvPNetConnection to the chat server
	 */
	public static void setXMPP(PvpNetConnection pvp)
	{
		if (pvpnet == null)
			pvpnet = pvp;
	}
	
	public static void login(String username, String password)
	{

		// showLoadingDiag();

		// Connect
		System.out.println("Connecting...");
		System.out.println();

		LoLClient c = new LoLClient(REGION, CLIENT_VERSION, username, password);

		setRTMP(c);

		try
		{
			client.connectAndLogin();

		} catch (IOException e)
		{
			System.out.println("Failed to connect");
			if (e.getMessage() != null && e.getMessage().contains("403"))
			{
				System.out.println("Incorrect user or pass");
				JOptionPane.showMessageDialog(mainFrame, LanguageDB.getLocalizedString("WRONG_USER_OR_PASS"), LanguageDB.getLocalizedString("ERROR"), JOptionPane.ERROR_MESSAGE);
				// hideLoadingDiag();
				return;
			} else
			{
				e.printStackTrace();
				// showCriticalError(ERROR_UNKNOWN);
			}
		}

		// Get Summoner Info
		try
		{

			loginPacket = client.getLoginDataPacketForUser();

			Arrays.sort(loginPacket.allSummonerData.masteryBook.bookPages);
			allChampions = client.getAvailableChampions();

			ArrayList<ChampionDTO> alchamp = new ArrayList<>();

			for (ChampionDTO champ : allChampions)
			{
				if (champ.owned || champ.freeToPlay)
				{
					alchamp.add(champ);
				}
			}

			availableChampions = alchamp.toArray(new ChampionDTO[0]);
			winManager.getHomeScreenManager().setSummonerIcon(loginPacket.allSummonerData.summoner.profileIconId);
			winManager.getHomeScreenManager().setIPBalance(loginPacket.ipBalance);
			winManager.getHomeScreenManager().setRPBalance(loginPacket.rpBalance);
			for (int i = 0; i < loginPacket.gameTypeConfigs.length; i++)
			{
				winManager.getCustomGameCreationManager().addPickMode(loginPacket.gameTypeConfigs[i]);
			}
			winManager.getCustomGameCreationManager().setCustomGameName(loginPacket.allSummonerData.summoner.name+"'s game");
			winManager.getChampionSelectionManager().updateMasteriesAndRunePages();
			winManager.getChampionSelectionManager().setChampionsForPick(availableChampions);
			gtcProvider= new GTCProvider(loginPacket.gameTypeConfigs);
			mmqProvider=new MMQProvider(client.getAvailableQueues());
			
		} catch (Exception e)
		{
			e.printStackTrace();
			// showCriticalError(ERROR_UNKNOWN);
		}
		
		PvpNetConnection p;
		try
		{
			p = new PvpNetConnection(username, password, REGION, loginPacket.allSummonerData.summoner.profileIconId, loginPacket.allSummonerData.summonerLevel.summonerLevel,
					"NGLC_BUILD" + build, getStatsForQueue("Unranked").wins);
			setXMPP(p);
		} catch (XMPPException e)
		{
			e.printStackTrace();
		}
		Sound.stopMusic();
		loginScreen.setVisible(false);
		mainFrame.setCurrentView(WindowManager.HOME_VIEW);
		mainFrame.setVisible();
		winManager.initChatComponent();
	}

	/**
	 * Searches for the PlayerStatSummary for the specified queue.
	 * 
	 * @param queue
	 *            The queue's name.
	 * @return A PlayerStatSummary object, which contains all the stats for a
	 *         queue or null if no stats exist for the given queue.
	 */
	public static PlayerStatSummary getStatsForQueue(String queue)
	{
		for (int i = 0; i < loginPacket.playerStatSummaries.playerStatSummarySet.length; i++)
		{
			if (loginPacket.playerStatSummaries.playerStatSummarySet[i].playerStatSummaryType.equals(queue))
			{
				return loginPacket.playerStatSummaries.playerStatSummarySet[i];
			}
		}
		return null;
	}
	public static File getPath() {
		try {
			String f = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI()).toString();
			if (f.endsWith(".jar"))
				return new File(f.substring(0, f.lastIndexOf("\\")));
			else
				return new File(f);
		} catch (URISyntaxException e) {
			return null;
		}
	}
	
	public static void loadPlugins() 
	{
		new PluginLoader();
	}
	/**
	 * Sets fields like region, version, etc.
	 * @param args {REGION, debug, netDebug, VERSION}
	 */
	public static void args(String[] args)
	{
		for(int i=0;i<args.length;i++)
		{
			switch(i)
			{
				case 1:
					REGION=args[i];
					break;
				case 2:
					debug=args[i].equalsIgnoreCase("true")||args[i].equals("1");
					break;
				case 3:
					netDebug=args[i].equalsIgnoreCase("true")||args[i].equals("1");
					break;
				case 4:
					CLIENT_VERSION=args[i];
					break;
			}
		}
	}
}
