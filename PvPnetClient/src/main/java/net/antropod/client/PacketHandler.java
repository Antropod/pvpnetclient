package net.antropod.client;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import com.gvaneyck.rtmp.Callback;
import com.gvaneyck.rtmp.LoLRTMPSClient;
import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.platform.game.ChampionBanInfoDTO;
import com.riotgames.platform.game.GameDTO;
import com.riotgames.platform.game.PlayerCredentialsDto;

import net.antropod.client.components.JChampionPanel;
import net.antropod.client.managers.WindowManager;
import net.antropod.client.resources.Sound;

/**
 * Handles incoming packets from the RTMP server
 * @author Antropod
 *
 */
public class PacketHandler extends Thread implements Callback
{
	/** Stores the packets, that have not been processed yet. */
	private Stack<TypedObject> packets = new Stack<>();

	private boolean running=false;
	
	public PacketHandler(LoLRTMPSClient client)
	{
		if (client == null)
			throw new NullPointerException("The client cannot be null!!");
	}
	/**
	 * Do not call this Method manually, call {@link #start()} instead!
	 */
	public void run()
	{
		if(running)
			return;
		running=true;
		try
		{
			while (true)
			{
				if (packets.size() == 0)
				{
					sleep(100);
					continue;
				}
				TypedObject to = packets.pop();
				if (to.getTO("data").getTO("body").type.contains("GameDTO"))
				{
					Globals.winManager.setCurrentGame(new GameDTO(to.getTO("data").getTO("body")));
				} else if (to.getTO("data").getTO("body").type.contains("PlayerCredentialsDto"))
				{
					startGameService(to);
				}
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * Initializes the GameClient and informs the server, that the PvP.net client is in sync with it.
	 * @param to The players credentials.
	 * @throws IOException
	 */
	private void startGameService(TypedObject to) throws IOException
	{
		PlayerCredentialsDto pcd=to.getTO("data").getTO("body").toJavaObject(PlayerCredentialsDto.class);
		Globals.client.invoke("gameService", "setClientReceivedGameMessage", new Object[] { pcd.gameId, "GAME_START_CLIENT" });
		Globals.client.invoke("gameService", "setClientReceivedMaestroMessage", new Object[] { pcd.gameId, "GameClientConnectedToServer" });
		Process game;
		try
		{
			game = Globals.gameClient.startGame(pcd);
			Sound.stopMusic();
			Globals.mainFrame.setCurrentView(WindowManager.HOME_VIEW);// Will be replaced with the post game screen soon
			Globals.winManager.getHomeScreenManager().getPlayButton().setEnabled(false);
			//System.gc();
			game.waitFor();
			Globals.winManager.getHomeScreenManager().getPlayButton().setEnabled(true);
			Globals.mainFrame.setCurrentView(WindowManager.HOME_VIEW);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	@Override
	public void callback(TypedObject result)
	{
		if (result.getString("result").equals("receive"))
			packets.push(result);

	}
}
