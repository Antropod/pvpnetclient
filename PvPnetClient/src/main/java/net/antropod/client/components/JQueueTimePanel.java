package net.antropod.client.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.Timer;

import net.antropod.client.resources.LanguageDB;

public class JQueueTimePanel extends JPanel implements ActionListener
{
	public static final int WIDTH=200,HEIGHT=100;
	private final String approx, actual;
	private JLabel approxLabel,actualLabel;
	private JLabel descLabel;
	
	private Timer timer;
	private long passedTime;
	
	public JQueueTimePanel()
	{
		super(new ClientLayout(200, 100));
		setSize(WIDTH,HEIGHT);
		approx=LanguageDB.getLocalizedString("APPROX");
		actual=LanguageDB.getLocalizedString("ACTUAL");
		
		descLabel=new JLabel(LanguageDB.getLocalizedString("SEARCHING_FOR_MATCH"));
		descLabel.setFont(descLabel.getFont().deriveFont(Font.BOLD, 14f));
		descLabel.setHorizontalAlignment(JLabel.CENTER);
		descLabel.setForeground(Color.white);
		add(descLabel,0,0,200,20);
		
		approxLabel=new JLabel(approx+": 00:00");
		approxLabel.setForeground(Color.white);
		add(approxLabel,0,30,200,20);
		
		actualLabel=new JLabel(actual+": 00:00");
		actualLabel.setForeground(Color.white);
		add(actualLabel,0,60,200,20);
		
		
		timer=new Timer(1000,this);
		timer.setRepeats(true);
		
		passedTime=0;
		
	}
	
	public void setApprox(long msecs)
	{
		long secs=msecs/1000;
		long mins=secs/60;
		String strSecs="00"+secs;
		String strMins="00"+mins;
		
		approxLabel.setText(approx+": "+strMins.substring(strMins.length()-2)+":"+strSecs.substring(strSecs.length()-2));
	}
	
	public void setActual(long msecs)
	{
		long secs=msecs/1000;
		long mins=secs/60;
		String strSecs="0"+secs;
		String strMins="0"+mins;
		
		actualLabel.setText(actual+": "+strMins.substring(strMins.length()-2)+":"+strSecs.substring(strSecs.length()-2));
	}
	
	@Override
	public void setVisible(boolean b)
	{

	}
	public void setVisible2(boolean b)
	{
		super.setVisible(b);
		if(b)
		{
			timer.start();
		}
		else
		{
			timer.stop();
			passedTime=0;
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		passedTime+=1000;
		setActual(passedTime);
	}
}
