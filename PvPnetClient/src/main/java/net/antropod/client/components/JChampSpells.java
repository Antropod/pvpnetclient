package net.antropod.client.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.border.TitledBorder;

import org.pushingpixels.trident.Timeline;
import org.pushingpixels.trident.Timeline.RepeatBehavior;

import net.antropod.client.resources.ImageAssets;

public class JChampSpells extends JPanel 
{
	public final static int STD_WIDTH=200,STD_HEIGHT=100;

	private JIcon champ;
	private JIcon spell1;
	private JIcon spell2;
	private JLabel phase;
	private final Timeline animator;
	
	public JChampSpells()
	{
		super(new ClientLayout(STD_WIDTH, STD_HEIGHT, false));
		animator = new Timeline(this);
		
		TitledBorder b=BorderFactory.createTitledBorder("Summoner");
		b.setTitleColor(Color.white);
		setBorder(b);
		setBackground(Color.black);
		setForeground(Color.white);
		setSize(STD_WIDTH,STD_HEIGHT);
		
		phase=new JLabel("none");
		phase.setFont(new Font("Arial",0,12));
		phase.setForeground(Color.white);
		add(phase,10, STD_HEIGHT/2-10, STD_WIDTH/3, 20);
		
		champ=new JIcon();
		champ.setBorder(BorderFactory.createLoweredSoftBevelBorder());
		add(champ,STD_WIDTH/2-32, STD_HEIGHT/2-32,64, 64);

		spell1=new JIcon();
		spell1.setBorder(BorderFactory.createLoweredSoftBevelBorder());
		add(spell1,STD_WIDTH/2-32+64+10, STD_HEIGHT/2-32,32,32);

		spell2=new JIcon();
		spell2.setBorder(BorderFactory.createLoweredSoftBevelBorder());
		add(spell2,STD_WIDTH/2-32+64+10, STD_HEIGHT/2,32,32);
	
	}
	
	private boolean isSpecial(String s)
	{
		return false;//No one is special :P
	}
	private boolean isRioter(String s)
	{
		return s.contains("Riot")||s.equals("Morello")||s.equals("Phreak");
	}
	public void setSummonerName(String s)
	{
		TitledBorder b=BorderFactory.createTitledBorder(s);
		if(isSpecial(s))
			b.setTitleColor(Color.CYAN);
		else if(isRioter(s))
			b.setTitleColor(Color.RED);
		else
			b.setTitleColor(Color.WHITE);
		
		setBorder(b);
	}
	
	public void setChamp(int i)
	{
		BufferedImage ii=ImageAssets.getChampionIcon(i);
		if(ii!=null)
			champ.setIcon(ii);
		

	}
	
	public void setSpells(int id1,int id2)
	{
		BufferedImage s1=ImageAssets.getSummonerSpellIcon(id1);
		BufferedImage s2=ImageAssets.getSummonerSpellIcon(id2);
		if(s1!=null&&s2!=null)
		{
			spell1.setIcon(s1);
			spell2.setIcon(s2);
		}
		
	}
	
	public void playAnim(boolean isPlayer)
	{
		if(animator.getDuration()!=2000)
		{
			animator.addPropertyToInterpolate("background", isPlayer?Color.ORANGE:Color.white,Color.black);
			animator.setDuration(2000);
			animator.playLoop(RepeatBehavior.REVERSE);
		}
		else
		{
			animator.playLoop(RepeatBehavior.REVERSE);
		}
	}
	public void stopAnim()
	{
		
		animator.end();
		try
		{
			Thread.sleep(10);
		} catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setBackground(Color.BLACK);
	}
	public void setPhase(String s)
	{
		phase.setText(s);
	}
}
