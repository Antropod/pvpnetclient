package net.antropod.client.components;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class TeamTableRenderer extends DefaultTableCellRenderer{

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) 
	{
		if(value instanceof JSummoner)
		{
			return (Component) value;
		}
		else
			return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
	}

}