package net.antropod.client.components;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import net.antropod.client.Globals;
import net.antropod.client.resources.SummonerSpells;
import net.antropod.client.resources.ImageAssets;
import net.antropod.client.resources.SQLDatabase;

public class JSpellPopup extends JPopupMenu implements ActionListener
{

	private boolean secondSpell;
	private String currentGameMode;

	public JSpellPopup(boolean issecondspell)
	{
		super();
		secondSpell = issecondspell;
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
//		JMenuItem item;
//		for (ESummonerSpells ess : ESummonerSpells.values())
//		{
//			if (!ess.isActive())
//				continue;
//			add(item = new JMenuItem(ess.name(), new ImageIcon(ImageAssets.getSummonerSpellIcon(ess.getID(), 16))));
//			item.setHorizontalTextPosition(JMenuItem.RIGHT);
//			item.setActionCommand(ess.name());
//			item.addActionListener(this);
//		}
	}
	
	public void setGameMode(String mode)
	{
		if(mode==currentGameMode)
			return;
		removeAll();
		JMenuItem item;
		for (SummonerSpells ess : SummonerSpells.values())
		{
			if (!ess.isActive())
				continue;
			if(!ess.getAvailableModes().contains(mode))
				continue;
			if(Globals.loginPacket.allSummonerData.summonerLevel.summonerLevel<ess.getRequiredLevel())
				continue;
			add(item = new JMenuItem(ess.name(), new ImageIcon(ImageAssets.getSummonerSpellIcon(ess.getID(), 32))));
			item.setHorizontalTextPosition(JMenuItem.RIGHT);
			item.setActionCommand(ess.name());
			item.addActionListener(this);
		}
		currentGameMode=mode;
	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		System.out.println(arg0.getActionCommand());
		try
		{
			if (secondSpell)
			{
				
				Globals.client.invoke("gameService", "selectSpells", new Object[] {Globals.winManager.getChampionSelectionManager().getSpell1ID(), SummonerSpells.valueOf(arg0.getActionCommand()).getID() });
				
			}
			else
			{
				Globals.client.invoke("gameService", "selectSpells", new Object[] { SummonerSpells.valueOf(arg0.getActionCommand()).getID(), Globals.winManager.getChampionSelectionManager().getSpell2ID()});
			}
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
