package net.antropod.client.components;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

import net.antropod.client.Globals;
import net.antropod.client.resources.LanguageDB;
import net.antropod.client.resources.Sound;

public class JGamePoppedDialog extends JDialog implements ActionListener
{
	private static final int WIDTH = 400, HEIGHT = 200;
	private JPanel contentPane;
	private JButton acceptButton, declineButton;
	private JLabel participantsLabel;

	public JGamePoppedDialog(JClientWindow c)
	{
		super(c);
		setResizable(false);
		contentPane = new JPanel(new ClientLayout(WIDTH, HEIGHT));
		setContentPane(contentPane);
		setSize(WIDTH, HEIGHT);
		if (c != null)
			setLocation(c.getX() + (c.getWidth() - WIDTH) / 2, c.getY() + (c.getHeight() - HEIGHT) / 2);
		setTitle("A match has been found!");

		participantsLabel = new JLabel("0000000000");
		participantsLabel.setFont(participantsLabel.getFont().deriveFont(Font.BOLD, 25f));
		participantsLabel.setHorizontalAlignment(JLabel.CENTER);

		acceptButton = new JButton(LanguageDB.getLocalizedString("ACCEPT"));
		acceptButton.addActionListener(this);
		declineButton = new JButton(LanguageDB.getLocalizedString("DECLINE"));
		declineButton.addActionListener(this);

		contentPane.add(participantsLabel, 0, 50, WIDTH, 30);
		contentPane.add(acceptButton, WIDTH / 2 - 100 - 20, HEIGHT - 70, 100, 50);
		contentPane.add(declineButton, WIDTH / 2 + 20, HEIGHT - 70, 100, 50);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

	}

	public void setStatusOfParticipants(String s)
	{
		participantsLabel.setText("<html><center>" + s + "</center></html>");
	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		Globals.client.acceptPoppedGame(arg0.getSource() == acceptButton);
		if (arg0.getSource() == declineButton)
			Globals.winManager.getHomeScreenManager().getQueuePanel().setVisible2(false);
		acceptButton.setEnabled(false);
		declineButton.setEnabled(false);
	}

	public void showDialog()
	{
		((JClientWindow) getParent()).toFront();
		setLocation(getParent().getX() + (getParent().getWidth() - WIDTH) / 2, getParent().getY() + (getParent().getHeight() - HEIGHT) / 2);
		Sound.MMQ_MATCH_FOUND.play();
		acceptButton.setEnabled(true);
		declineButton.setEnabled(true);
		setVisible(true);
	}

	public void hideDialog()
	{
		setVisible(false);
		if (acceptButton.isEnabled())
		{
			Globals.winManager.getHomeScreenManager().getPlayButton().setEnabled(true);
			Globals.winManager.getHomeScreenManager().getQueuePanel().setVisible2(false);
		}
	}
}
