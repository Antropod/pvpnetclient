package net.antropod.client.components;

import com.riotgames.platform.game.map.GameMap;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.ItemSelectable;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import net.antropod.client.resources.ImageAssets;

public class JMapSelectionPanel extends Canvas implements MouseListener, ItemSelectable
{
	private int mapsPerRow = 2;
	private final int TITLE_HEIGHT = 20;
	private ArrayList<GameMap> maps;
	private int selectedIndex = 0;
	private ArrayList<ItemListener> itemListeners;
	private ArrayList<Integer> disabledMapIds;

	public JMapSelectionPanel()
	{
		this.maps = new ArrayList<>();
		this.disabledMapIds = new ArrayList<>();
		setBackground(Color.black);
		setCursor(Cursor.getPredefinedCursor(12));
		addMouseListener(this);
		this.itemListeners = new ArrayList<>();
	}

	public void setVisible(boolean flag)
	{
		super.setVisible(flag);
	}

	public void addMap(GameMap map)
	{
		addMap(map, true);
	}

	public void addMap(GameMap map, boolean enabled)
	{
		this.maps.add(map);
		if (!enabled)
		{
			this.disabledMapIds.add(Integer.valueOf(map.mapId));
		}
	}

	public void paint(Graphics g1)
	{
		BufferedImage buffer = new BufferedImage(getWidth(), getHeight(), 2);
		Graphics2D g = buffer.createGraphics();
		g.setFont(getFont().deriveFont(1, 16.0F));
		g.setColor(Color.white);
		g.drawString("Maps:", TITLE_HEIGHT, 17);

		for (int i = 0; i < this.maps.size(); i++)
		{
			BufferedImage img;
			switch (((GameMap) this.maps.get(i)).mapId)
			{
				case 7:
					img = ImageAssets.MAP7;
					break;
				case 8:
					img = ImageAssets.MAP8;
					break;
				case 10:
					img = ImageAssets.MAP10;
					break;
				case 9:
				default:
					img = ImageAssets.MAP1;
			}

			g.drawImage(this.disabledMapIds.contains(Integer.valueOf(((GameMap) this.maps.get(i)).mapId)) ? convertToGrayScale(img) : img, i % this.mapsPerRow * getWidth() / this.mapsPerRow, i
					/ this.mapsPerRow * ((getHeight() - TITLE_HEIGHT) / (this.maps.size() / this.mapsPerRow)) + TITLE_HEIGHT, i % this.mapsPerRow * getWidth() / this.mapsPerRow + getWidth() / this.mapsPerRow, i
					/ this.mapsPerRow * ((getHeight() - TITLE_HEIGHT) / (this.maps.size() / this.mapsPerRow)) + TITLE_HEIGHT + (getHeight() - TITLE_HEIGHT) / (this.maps.size() / this.mapsPerRow), 0, 0, img.getWidth(),
					img.getHeight(), null);
		}
		g.setColor(Color.CYAN);

		g.drawRect(this.selectedIndex % this.mapsPerRow * getWidth() / this.mapsPerRow, this.selectedIndex / this.mapsPerRow * ((getHeight() - TITLE_HEIGHT) / (this.maps.size() / this.mapsPerRow)) + TITLE_HEIGHT,
				getWidth() / this.mapsPerRow - 2, (getHeight() - TITLE_HEIGHT) / (this.maps.size() / this.mapsPerRow));
		g1.drawImage(buffer, 0, 0, getWidth(), getHeight(), 0, 0, buffer.getWidth(), buffer.getHeight(), null);
		g.dispose();
	}

	public GameMap getSelectedMap()
	{
		return this.selectedIndex == -1 ? null : (GameMap) this.maps.get(this.selectedIndex);
	}

	public void mouseClicked(MouseEvent m)
	{
	}

	public void mouseEntered(MouseEvent arg0)
	{
	}

	public void mouseExited(MouseEvent arg0)
	{
	}

	public void mousePressed(MouseEvent m)
	{
		int selx = m.getX() / (getWidth() / this.mapsPerRow);
		int sely = (m.getY() - TITLE_HEIGHT) / ((getHeight() - TITLE_HEIGHT) / (this.maps.size() / this.mapsPerRow));
		int sel = sely * this.mapsPerRow + selx;
		if ((sel < 0) || (sel >= this.maps.size()))
		{
			sel = 0;
		}
		if (sel != this.selectedIndex)
			repaint();
		if (!this.disabledMapIds.contains(Integer.valueOf(((GameMap) this.maps.get(sel)).mapId)))
			this.selectedIndex = sel;
		ItemEvent i = new ItemEvent(this, this.selectedIndex, this.maps.get(this.selectedIndex), 1);
		for (ItemListener il : this.itemListeners)
			il.itemStateChanged(i);
	}

	public void mouseReleased(MouseEvent arg0)
	{
	}

	public void addItemListener(ItemListener arg0)
	{
		this.itemListeners.add(arg0);
	}

	public Object[] getSelectedObjects()
	{
		if (this.selectedIndex < 0)
			return new Object[0];
		return new Object[] { this.maps.get(this.selectedIndex) };
	}

	public void removeItemListener(ItemListener arg0)
	{
		this.itemListeners.remove(arg0);
	}

	private BufferedImage convertToGrayScale(BufferedImage img)
	{
		BufferedImage gray = new BufferedImage(img.getWidth(), img.getHeight(), 10);
		Graphics2D g = gray.createGraphics();
		g.drawImage(img, 0, 0, gray.getWidth(), gray.getHeight(), 0, 0, img.getWidth(), img.getHeight(), null);
		g.dispose();
		return gray;
	}
}