package net.antropod.client.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.jivesoftware.smack.XMPPException;

import com.riotgames.platform.catalog.champion.ChampionDTO;

import net.antropod.client.Globals;
import net.antropod.client.LoLClient;
import net.antropod.client.chat.PvpNetConnection;
import net.antropod.client.managers.WindowManager;
import net.antropod.client.resources.ImageAssets;
import net.antropod.client.resources.LanguageDB;
import net.antropod.client.resources.Sound;
import net.antropod.logging.SimpleLogger;

/**
 * Minimal login screen without background image/animation/music
 * 
 * @author ArcadeStorm
 */
public class MinimalLoginScreen extends javax.swing.JFrame
{

	private String region;

	/**
	 * Creates new form MinimalLoginScreen
	 * 
	 * @param clientVersion
	 *            version string that is displayed in the frame title
	 * @param region
	 *            riot server region to log onto
	 * @param username
	 *            the username to fill in if remember username was previously
	 *            checked, or null if previously unchecked
	 */
	public MinimalLoginScreen(String clientVersion, String region, String username)
	{
		this.region = region;
		// Calls auto-generated content
		initComponents();
		// Set form components to correct status
		if (username != null&&!username.equals(""))
		{
			jTextField1.setText(username);
			jCheckBox1.setSelected(true);
			jPasswordField1.requestFocus();
		}
		// disable log in button because password field is empty
		jButton1.setEnabled(false);
		setIconImage(ImageAssets.LOL_LOGO);

		setResizable(false);
		setTitle("League of Legends " + clientVersion + " (" + region + ")");
		getContentPane().setBackground(new Color(11, 13, 26));

		// center the login screen
		Dimension screenResolution = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(screenResolution.width / 2 - getWidth() / 2, screenResolution.height / 2 - getHeight() / 2);
		// try {
		// UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		// } catch (ClassNotFoundException | InstantiationException |
		// IllegalAccessException | UnsupportedLookAndFeelException ex) {
		// SimpleLogger.log(ex);
		// }
		setVisible(true);
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents()
	{

		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jTextField1 = new javax.swing.JTextField();
		jLabel3 = new javax.swing.JLabel();
		jPasswordField1 = new javax.swing.JPasswordField();
		jButton1 = new javax.swing.JButton();
		jCheckBox1 = new javax.swing.JCheckBox();
		jSeparator1 = new javax.swing.JSeparator();
		jSeparator2 = new javax.swing.JSeparator();
		jLabel4 = new LinkLabel();
		try
		{
			jLabel4.setURL(new URL("https://euw.leagueoflegends.com/account/recovery/username"));
		} catch (MalformedURLException ex)
		{
			SimpleLogger.getInstance().log(ex);
		}
		jLabel5 = new LinkLabel();
		try
		{
			jLabel5.setURL(new URL("https://euw.leagueoflegends.com/account/recovery/password"));
		} catch (MalformedURLException ex)
		{
			SimpleLogger.getInstance().log(ex);
		}
		jLabel6 = new LinkLabel();
		try
		{
			jLabel6.setURL(new URL("https://signup.leagueoflegends.com/en/signup/index"));
		} catch (MalformedURLException ex)
		{
			SimpleLogger.getInstance().log(ex);
		}

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
		jLabel1.setIcon(new ImageIcon(ImageAssets.LOL_NAME_LOGO));

		jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
		jLabel2.setForeground(new java.awt.Color(255, 255, 255));
		jLabel2.setText("Username");

		jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
		jLabel3.setForeground(new java.awt.Color(255, 255, 255));
		jLabel3.setText("Password");

		jPasswordField1.setEchoChar('*'); //linux does not approve the dot i cant even put it in the comment
		jPasswordField1.addKeyListener(new KeyListener()
		{

			@Override
			public void keyTyped(KeyEvent e)
			{
				if (e.getKeyChar() == KeyEvent.VK_ENTER && jButton1.isEnabled())
				{
					jButton1.doClick();
				}
			}

			@Override
			public void keyPressed(KeyEvent e)
			{
			}

			@Override
			public void keyReleased(KeyEvent e)
			{
				jButton1.setEnabled(jPasswordField1.getPassword().length != 0);
			}
		});

		jButton1.setBackground(new java.awt.Color(200, 100, 20));
		jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
		jButton1.setForeground(new java.awt.Color(255, 255, 255));
		jButton1.setText("Log In");
		jButton1.addActionListener(new java.awt.event.ActionListener()
		{
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				jButton1ActionPerformed(evt);
			}
		});

		jCheckBox1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
		jCheckBox1.setForeground(new java.awt.Color(255, 255, 255));
		jCheckBox1.setText("Remember Username");

		jLabel4.setText("Forgot your username?");

		jLabel5.setText("Forgot your password?");

		jLabel6.setText("Sign up for an account!");

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
										.addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jTextField1, javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jPasswordField1, javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGroup(
												javax.swing.GroupLayout.Alignment.LEADING,
												layout.createSequentialGroup()
														.addGroup(
																layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
																		.addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)).addGap(0, 0, Short.MAX_VALUE))
										.addGroup(
												javax.swing.GroupLayout.Alignment.LEADING,
												layout.createSequentialGroup().addComponent(jCheckBox1).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
														.addComponent(jButton1))).addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jLabel2).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jLabel3).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jCheckBox1).addComponent(jButton1))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		pack();
	}// </editor-fold>//GEN-END:initComponents

	/**
	 * This method is called when the log in button is clicked or when enter is
	 * pressed while the password field has focus.
	 * 
	 * @param evt
	 */
	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)
	{// GEN-FIRST:event_jButton1ActionPerformed
		// ((WinActions) (Globals.al)).login(jTextField1.getText(), new
		// String(jPasswordField1.getPassword()));
		String username = jTextField1.getText();
		String password = new String(jPasswordField1.getPassword());
		
		Globals.login(username, password);

		if(jCheckBox1.isSelected())
			Globals.properties.setSummonerName(username);

	}// GEN-LAST:event_jButton1ActionPerformed

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton jButton1;
	private javax.swing.JCheckBox jCheckBox1;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private net.antropod.client.components.LinkLabel jLabel4;
	private net.antropod.client.components.LinkLabel jLabel5;
	private net.antropod.client.components.LinkLabel jLabel6;
	private javax.swing.JPasswordField jPasswordField1;
	private javax.swing.JSeparator jSeparator1;
	private javax.swing.JSeparator jSeparator2;
	private javax.swing.JTextField jTextField1;
	// End of variables declaration//GEN-END:variables
}
