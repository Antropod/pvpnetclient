package net.antropod.client.components;

import java.awt.LayoutManager;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.PanelUI;

public class JPanel extends JComponent implements Accessible
{
	private static final String uiClassID = "PanelUI";

	private BufferedImage bgImage;

	public JPanel(LayoutManager paramLayoutManager, boolean paramBoolean)
	{
		setLayout(paramLayoutManager);
		setDoubleBuffered(paramBoolean);
		updateUI();
	}

	public JPanel(LayoutManager paramLayoutManager)
	{
		this(paramLayoutManager, true);
	}

	public JPanel(boolean paramBoolean)
	{
		this(new FlowLayout(), paramBoolean);
	}

	public JPanel()
	{
		this(true);
	}
	
	public void setBackgroundImage(BufferedImage bgimg)
	{
		bgImage=bgimg;
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		if(bgImage!=null)
		g.drawImage(bgImage, 0, 0, getWidth(), getHeight(), 0, 0, bgImage.getWidth(), bgImage.getHeight(), null);
	}

	protected String paramString()
	{
		return super.paramString();
	}

	public AccessibleContext getAccessibleContext()
	{
		if (this.accessibleContext == null)
			this.accessibleContext = new AccessibleJPanel();
		return this.accessibleContext;
	}

	protected class AccessibleJPanel extends JComponent.AccessibleJComponent
	{

		protected AccessibleJPanel()
		{
			super();
		}

		public AccessibleRole getAccessibleRole()
		{
			return AccessibleRole.PANEL;
		}
	}
	
	public void add(Component j, int initX, int initY, int initW, int initH)
	{
		add(j, sizeToStr(initX, initY, initW, initH));
	}
	
	public void add(JComponent c, int[] size)
	{
		
		add(c, sizeToStr(size));
	}
	
	private static String sizeToStr(int i, int j, int k, int l)
	{
		StringBuilder sb=new StringBuilder();
		sb.append(i).append(",").append(j).append(",").append(k).append(",").append(l);
		return sb.toString();
	}
	
	private static String sizeToStr(int[] i)
	{
		return sizeToStr(i[0],i[1],i[2],i[3]);
	}
}