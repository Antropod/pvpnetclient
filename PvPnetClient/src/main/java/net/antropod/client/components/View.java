package net.antropod.client.components;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JComponent;
/**
 * A view, which can be added to a JClientWindow.
 * @author Antropod
 *
 */
public class View
{
	/**
	 * Components of this view.
	 */
	private ArrayList<Component> components;
	/**
	 * This view's unique ID.
	 */
	private int id;
	/**
	 * The background image of this view.
	 */
	private BufferedImage bg;
	
	/**
	 * 
	 * @param id This view's unique ID.
	 */
	public View(int id)
	{
		super();
		components=new ArrayList<>();
		this.id=id;
		bg=null;
	}
	
	/**
	 * Adds a component to this view.
	 * @param j The Component that will be added.
	 */
	public void addComponent(Component j)
	{
		if(!components.contains(j))
		{
			components.add(j);
		}
	}
	
	/**
	 * 
	 * @return the unique ID of this view
	 */
	public int getID()
	{
		return id;
	}
	
	/**
	 * 
	 * @return All components of this view.
	 */
	public ArrayList<Component> getComponents()
	{
		return components;
	}
	/**
	 * Sets the background image for this view.
	 * @param img The background image.
	 */
	public void setBackgroundImage(BufferedImage img)
	{
		bg=img;
	}
	/**
	 * 
	 * @return The background image for this view or null if this view has no background image.
	 */
	public BufferedImage getBackgroungImage()
	{
		return bg;
	}
}
