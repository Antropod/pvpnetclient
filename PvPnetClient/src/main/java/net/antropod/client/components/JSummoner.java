package net.antropod.client.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import net.antropod.client.resources.ImageAssets;

public class JSummoner extends JPanel
{
	public final static int STD_WIDTH=300,STD_HEIGHT=100;
	
	private int level;
	private String name;
	
	public JSummoner(BufferedImage icon, String summoner, boolean isOwner)
	{
		super(new ClientLayout(STD_WIDTH, STD_HEIGHT));
		setBackground(isOwner?new Color(0xDBA709):Color.black);
		setForeground(Color.white);
		setSize(STD_WIDTH,STD_HEIGHT);
		name=summoner;
		
		JLabel n=new JLabel(summoner,JLabel.CENTER);
		//n.setBounds(100,5,200,30);
		n.setFont(new Font("Arial",Font.BOLD,17));
		n.setBackground(Color.black);
		n.setForeground(Color.white);
		add(n,"100,5,200,30");
		
		JIcon s=new JIcon(icon);
		//s.setBounds(18,18,64,64);
		add(s,"18,18,64,64");

		JSeparator js=new JSeparator(JSeparator.VERTICAL);
		//js.setBounds(100, 0, 2, STD_HEIGHT);
		add(js,"100, 0, 2,"+ STD_HEIGHT);
		
		JSeparator js2=new JSeparator(JSeparator.HORIZONTAL);
		//js2.setBounds(100, n.getY()+n.getHeight()+5, STD_WIDTH-js.getX(), 2);
		add(js2, "100, "+(n.getY()+n.getHeight()+5)+","+ (STD_WIDTH-js.getX())+", 2");
		
	}
	
	public JSummoner(int iconid, String summoner, boolean isOwner)
	{
		this(ImageAssets.getSummonerIcon(iconid),summoner,isOwner);
	}

}
