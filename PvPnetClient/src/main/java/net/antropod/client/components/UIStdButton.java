package net.antropod.client.components;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicButtonUI;

public class UIStdButton extends BasicButtonUI
{
	public static BufferedImage idle,hover,click;
	public BufferedImage clidle,clhover,clclick;
	
	public UIStdButton(Color c)
	{
		super();
		try {
			idle=ImageIO.read(new File("img/ui/StdButton.png"));
			hover=ImageIO.read(new File("img/ui/StdButton_Hover.png"));
			click=ImageIO.read(new File("img/ui/StdButton_Click.png"));
			setColor(c);
			
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
	protected void setColor(Color c)
	{
		if(c!=null)
		{
			clidle=colorImage(idle,c);
			clhover=colorImage(hover,c);
			clclick=colorImage(click,c);
		}
		else
		{
			clidle=idle;
			clhover=hover;
			clclick=click;
		}
	}
	
	@Override
	public void paint(Graphics g, JComponent j) 
	{
		AbstractButton ab=(AbstractButton)j;
		if((ab.isEnabled())&&(ab.getModel().isPressed()))
		{
			g.drawImage(clclick, 0, 0, j.getWidth(), j.getHeight(), 0, 0, clclick.getWidth(), clclick.getHeight(), null);
		}
		else if((ab.isRolloverEnabled())&&(ab.getModel().isRollover()))
		{
			g.drawImage(clhover, 0, 0, j.getWidth(), j.getHeight(), 0, 0, clhover.getWidth(), clhover.getHeight(), null);
		}
		else
		{
		g.drawImage(clidle, 0, 0, j.getWidth(), j.getHeight(), 0, 0, clidle.getWidth(), clidle.getHeight(), null);
		}
		super.paint(g, j);
	}
	protected void installDefaults(AbstractButton ab)
	{
		
	}
	
	public static BufferedImage colorImage(BufferedImage loadImg, int red, int green, int blue) {
	    BufferedImage img = new BufferedImage(loadImg.getWidth(), loadImg.getHeight(),
	        BufferedImage.TYPE_INT_RGB);
	    Graphics2D graphics = img.createGraphics(); 
	    Color newColor = new Color(0xFF-red,0xFF-green,0xFF-blue);
	    graphics.setXORMode(newColor);
	    graphics.drawImage(loadImg, null, 0, 0);
	    graphics.dispose();
	    return img;
	}
	public static BufferedImage colorImage(BufferedImage loadImg,Color c)
	{
		return colorImage(loadImg, c.getRed(), c.getGreen(), c.getBlue());
	}
	
}