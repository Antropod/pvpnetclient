package net.antropod.client.components;

import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

public class JColorTextPane extends JTextPane
{

	public void append(Color c, String s)
	{
		StyledDocument doc = getStyledDocument();
		Style style = addStyle("" + System.currentTimeMillis(), null);
		StyleConstants.setForeground(style, c);
		try
		{
			doc.insertString(doc.getLength(), s, style);
		} 
		catch (BadLocationException e)
		{
			e.printStackTrace();
		}
		removeStyle(style.getName());
	}

	public void append(String s)
	{
		append(Color.black, s);
	}

}