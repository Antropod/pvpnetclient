package net.antropod.client.components;

import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import javax.swing.JLabel;
import net.antropod.logging.SimpleLogger;

/**
 * JLabel that attempts to open a given URL in the default browser when clicked. 
 * 
 * @author ArcadeStorm
 */
public class LinkLabel extends JLabel implements MouseListener {

    private URL url;
    // Original text is kept in a seperate field for the mouseEntered and
    // mouseExited methods, since getText() also returns the surrounding HTML. 
    private String originalText;

    public LinkLabel() {
        super();
        url = null;
        originalText = null;
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        addMouseListener(this);
    }

    
    public URL getURL() {
        return url;
    }

    public void setURL(URL url) {
        this.url = url;
    }

    @Override
    public void setText(String text) {
        originalText = text;
        // Add styling (white, bold font)
        super.setText("<html><font color=\"#FFFFFF\"><b>" + text + "</b></font></html>");
    }
    
    
    /**
     * Opens the given URL in the host system's default browser. If this action
     * isn't supported or the url isn't set, nothing is done. 
     * 
     * @param e mouse event to process
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (Desktop.isDesktopSupported() && url != null) {
            if (Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                try {
                    Desktop.getDesktop().browse(url.toURI());
                } catch (URISyntaxException | IOException ex) {
                    SimpleLogger.log(ex);
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // Add underline on hover
        super.setText("<html><font color=\"#FFFFFF\"><b><u>" + originalText + "</u></b></font></html>");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // Remove underline after hover
        super.setText("<html><font color=\"#FFFFFF\"><b>" + originalText + "</b></font></html>");
    }
}
