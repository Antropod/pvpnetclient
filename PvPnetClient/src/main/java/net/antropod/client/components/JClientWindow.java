package net.antropod.client.components;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;

import net.antropod.client.Globals;
import net.antropod.client.resources.ImageAssets;
import net.antropod.client.resources.LanguageDB;

public class JClientWindow extends JFrame
{
	// Constants-------------------------------------
	/** The window's initial width */
	public static final int FRAME_WIDTH = 1280;
	/** The window's initial height */
	public static final int FRAME_HEIGHT = 800;

	// ----------------------------------------------

	private JPanel contentPane;
	private View[] views;
	private int currentView = -1;
	private JDialog iconFlashHelper;

	public JClientWindow()
	{
		iconFlashHelper=new JDialog(this);
		iconFlashHelper.setUndecorated(true);
		iconFlashHelper.setSize(0, 0);
		iconFlashHelper.setModal(false);
		iconFlashHelper.addWindowFocusListener(new WindowAdapter() {
 
            @Override
            public void windowGainedFocus(WindowEvent e) {
                requestFocus();
                iconFlashHelper.setVisible(false);
                super.windowGainedFocus(e);
            }
 
        });
        addWindowFocusListener(new WindowAdapter() {
 
            @Override
            public void windowGainedFocus(WindowEvent e) {
            	iconFlashHelper.setVisible(false);
                super.windowGainedFocus(e);
            }
        });
		
		// super();
		views = new View[50];
		contentPane = new JPanel(new ClientLayout(FRAME_WIDTH, FRAME_HEIGHT));
		setTitle(LanguageDB.getLocalizedString("FRAME_TITLE") + " [BUILD " + Globals.build + "]");
		setIconImage(ImageAssets.LOL_LOGO);
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setMinimumSize(new Dimension(960, 600));
		setContentPane(contentPane);
		setLocation((Toolkit.getDefaultToolkit().getScreenSize().width - FRAME_WIDTH) / 2, (Toolkit.getDefaultToolkit().getScreenSize().height - FRAME_HEIGHT) / 2);
		// super.setVisible(true);

		addWindowListener(new WindowListener()
		{
			public void windowOpened(WindowEvent arg0)
			{
			}

			public void windowIconified(WindowEvent arg0)
			{
			}

			public void windowDeiconified(WindowEvent arg0)
			{
			}

			public void windowDeactivated(WindowEvent arg0)
			{
			}

			public void windowActivated(WindowEvent arg0)
			{
			}

			public void windowClosing(WindowEvent arg0)
			{
				if (Globals.client != null)
					Globals.client.close();
				Globals.properties.save();
				System.exit(0);
			}

			public void windowClosed(WindowEvent arg0)
			{
				if (Globals.client != null)
					Globals.client.close();
				Globals.properties.save();
				System.exit(0);
			}
		});
			
		
	}

	/**
	 * Adds a new view, with the given ID, to the window. If the id has already
	 * been taken, no action will be taken.
	 * 
	 * @param id
	 * @param arr
	 */
	public void addView(View view)
	{
		if (views[view.getID()] == null)
		{
			views[view.getID()] = view;
			for (Component j : view.getComponents())
			{
				contentPane.add(j, j.getX(), j.getY(), j.getWidth(), j.getHeight());
				if (view.getID() != currentView)
					j.setVisible(false);
			}
		}
	}

	public void setCurrentView(int i)
	{
		if (currentView == i)
			return;
		if (views[i] == null)
		{
			throw new IllegalArgumentException("The view '" + i + "' doesn't exist!");
		}
		if (currentView > -1)
			for (Component j : views[currentView].getComponents())
				j.setVisible(false);

		for (Component j : views[i].getComponents())
			j.setVisible(true);

		if (views[i].getBackgroungImage() != null)
		{
			contentPane.setBackgroundImage(views[i].getBackgroungImage());
		}
		currentView = i;
	}

	@Override
	/**
	 * The window's visibility can't be changed.
	 * This is to prevent plugins from hiding the window.
	 * @param b
	 */
	public void setVisible(boolean b)
	{
		if (Globals.debug)
		{
			System.err.println("You can't change the window's visibility!");
		}
	}

	public void setVisible()
	{
		super.setVisible(true);
	}

	@Override
	/**
	 * Brings the window to the front.
	 * 
	 */
	public void toFront()//Implements a workaround, if the user has disabled the "force window focus" option.
	{
		int state = super.getExtendedState() & ~JFrame.ICONIFIED;
		super.setExtendedState(state);
		super.setAlwaysOnTop(true);
		super.toFront();
		super.requestFocus();
		super.setAlwaysOnTop(false);
	}
	/** 
	 * Flashes the the icon in the task bar if the window doesn't has the focus.
	 */
	public void flashIcon()
	{
		if (!isFocused()) {
            if (iconFlashHelper.isVisible()) {
            	iconFlashHelper.setVisible(false);
            }
            iconFlashHelper.setLocation(0, 0);
            iconFlashHelper.setLocationRelativeTo(this);
            iconFlashHelper.setVisible(true);
        }
	}
}
