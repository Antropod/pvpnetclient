package net.antropod.client.components;

import javax.swing.table.DefaultTableModel;

public class TeamTableModel extends DefaultTableModel
{
	
	public TeamTableModel(Object[] o, int i)
	{
		super(o,i);
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
	
}
