package net.antropod.client.components;

import java.util.HashMap;
import java.util.Map;

import javax.swing.table.DefaultTableModel;

public class GameTableModel extends DefaultTableModel 
{

    private Map<String, Double> gameIds = new HashMap<>();

    public GameTableModel(Object[] o, int i) {
        super(o, i);
    }

    public void addRow(Object[] o, Double gameID, int i) {
        super.addRow(o);
        gameIds.put(o[1].toString(), gameID);
    }

    public Double getGameId(String name) {
        return gameIds.get(name);
    }

    public void clear() {
        super.setRowCount(0);
        gameIds.clear();
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
