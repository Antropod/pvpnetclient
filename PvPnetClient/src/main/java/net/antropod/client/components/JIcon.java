package net.antropod.client.components;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class JIcon extends JPanel
{
	private BufferedImage icon;

	public JIcon()
	{
		this(null);
	}

	public JIcon(BufferedImage icn)
	{
		if (icn != null)
			setIcon(icn);
		setOpaque(false);
	}

	public void setIcon(BufferedImage icn)
	{

		icon = icn;
		repaint();
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		if (icon != null)
			g.drawImage(icon, 0, 0, getWidth(), getHeight(), 0, 0, icon.getWidth(), icon.getHeight(), null);
	}
}
