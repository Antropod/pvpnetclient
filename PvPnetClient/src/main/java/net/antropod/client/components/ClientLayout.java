package net.antropod.client.components;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.util.HashMap;

import javax.swing.ImageIcon;

public class ClientLayout implements LayoutManager
{
	int initialHeight;
	int initialWidth;
	public boolean hasInsets;
	double scaleX = 1f;
	double scaleY = 1f;

	HashMap<Component, Rectangle> initSizes = new HashMap<>();

	public ClientLayout(int initWidth, int initHeight, boolean hasInsets)
	{
		initialHeight = initHeight;
		initialWidth = initWidth;
		this.hasInsets=hasInsets;
	}
	public ClientLayout(int initWidth, int initHeight)
	{
		this(initWidth,initHeight,true);
	}

	@Override
	public void addLayoutComponent(String s, Component c)
	{
		try
		{
			if (s != null)
			{
				String[] bounds = s.split(",");
				if (bounds.length > 3)
				{
					int i, j, k, l;
					i = Integer.parseInt(bounds[0].trim());
					j = Integer.parseInt(bounds[1].trim());
					k = Integer.parseInt(bounds[2].trim());
					l = Integer.parseInt(bounds[3].trim());
					initSizes.put(c, new Rectangle(i, j, k, l));
					c.setBounds(i, j, k, l);
				}
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void layoutContainer(Container c)
	{
		int count = c.getComponentCount();
		int width = c.getWidth() - (hasInsets?(c.getInsets().left + c.getInsets().right):0);
		int height = c.getHeight() - (hasInsets?(c.getInsets().top + c.getInsets().bottom):0);
		scaleX = (double) width / (double) initialWidth;
		scaleY = (double) height / (double) initialHeight;
		for (int i = 0; i < count; i++)
		{
			Component comp = c.getComponent(i);
			Rectangle rect = initSizes.get(comp);
			if (rect != null)
				comp.setBounds((int) (rect.x * scaleX), (int) (rect.y * scaleY), (int) (rect.width * scaleX), (int) (rect.height * scaleY));
		}
	}

	@Override
	public Dimension minimumLayoutSize(Container arg0)
	{
		// TODO Auto-generated method stub
		return new Dimension(800, 600);
	}

	@Override
	public Dimension preferredLayoutSize(Container arg0)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeLayoutComponent(Component arg0)
	{
		// TODO Auto-generated method stub

	}

}
