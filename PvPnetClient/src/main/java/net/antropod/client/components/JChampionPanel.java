package net.antropod.client.components;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;

import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.platform.catalog.champion.ChampionDTO;
import com.riotgames.platform.game.ChampionBanInfoDTO;

import net.antropod.client.Globals;
import net.antropod.client.resources.ImageAssets;
import net.antropod.client.resources.SQLDatabase;
import net.antropod.client.resources.Sound;

public class JChampionPanel extends JScrollPane implements ActionListener
{

	public HashMap<JButton, ChampionDTO> champions;
	public HashMap<JButton, ChampionBanInfoDTO> bannableChamps;
	public ArrayList<Integer> bannedChamps;
	private ArrayList<Integer> pickedChamps;
	private JPanel pane;
	private int phase;
	public static final int PICK = 0;
	public static final int BAN = 1;

	public JChampionPanel()
	{
		super();
		bannedChamps=new ArrayList<>();
		pickedChamps=new ArrayList<>();
	}

	@Override
	public void setSize(int width, int height)
	{
		super.setSize(width, height);
	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		try
		{
			if ((Globals.winManager.getChampionSelectionManager().getPlayerTeam() == 1 ? Globals.winManager.getCurrentGame().teamOne : Globals.winManager.getCurrentGame().teamTwo)[Globals.winManager.getChampionSelectionManager().getPlayerPosition()].pickMode == 1)
			{
				if (phase == PICK)
				{

					int id = Globals.client.invoke("gameService", "selectChampion", new Object[] { champions.get(arg0.getSource()).championId });
					Globals.client.join(id);
					Sound.getChampionSelectionQuote(champions.get(arg0.getSource()).championId).play();
				} else if (phase == BAN)
				{
					int id = Globals.client.invoke("gameService", "banChampion", new Object[] { bannableChamps.get(arg0.getSource()).championId });
					Globals.client.join(id);
					Sound.CHAMP_SLCT_BAN.play();
				}
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void setEnabled(boolean f)
	{
		if (champions == null)
			return;
		else
		{
			JButton[] buttons = champions.keySet().toArray(new JButton[0]);
			for (int i = 0; i < buttons.length; i++)
			{
				if(!bannedChamps.contains(champions.get(buttons[i]).championId))
				buttons[i].setEnabled(f);
			}
		}
	}

	public void setChampsForPick(ChampionDTO[] champs)
	{
		Arrays.sort(champs);
		champions = new HashMap<>();
		for (int i = 0; i < champs.length; i++)
		{
			JButton jb = new JButton();
			jb.setBounds(i % 10 * 64, i / 10 * 64, 64, 64);
			jb.setToolTipText(SQLDatabase.getChampionNameByID(champs[i].championId));
			jb.setIcon(new ImageIcon(ImageAssets.getChampionIcon(champs[i].championId, 60)));
			jb.addActionListener(this);
			jb.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			champions.put(jb, champs[i]);
		}
		setPhase(PICK);
	}

	public void setChampsForBan(ChampionBanInfoDTO[] cb)
	{
		Arrays.sort(cb);
		bannableChamps = new HashMap<>();
		for (int i = 0, j = 0; i < cb.length; i++)
		{
			if (cb[i].enemyOwned)
			{
				JButton jb = new JButton();
				jb.setBounds(j % 10 * 64, j / 10 * 64, 64, 64);
				jb.setToolTipText(SQLDatabase.getChampionNameByID(cb[i].championId));
				jb.setIcon(new ImageIcon(ImageAssets.getChampionIcon(cb[i].championId, 60)));
				jb.addActionListener(this);
				jb.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				bannableChamps.put(jb, cb[i]);
				j++;
			}
		}
		setPhase(BAN);
	}

	public void setPhase(int phase)
	{
		this.phase = phase;

		getViewport().removeAll();

		pane = new JPanel(null);
		setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pane.setPreferredSize(new Dimension(64 * 10, 64 * ((phase == PICK ? champions.size() : bannableChamps.size()) / 10 + 1)));
		switch (phase)
		{
			case PICK:
				Set<JButton> s = champions.keySet();
				for (JButton j : s)
				{

					pane.add(j);
				}
				break;
			case BAN:
				Set<JButton> s2 = bannableChamps.keySet();
				for (JButton j : s2)
				{

					pane.add(j);
				}
				break;
		}
		getViewport().add(pane);
		revalidate();

	}
	public void bannedChamps(int... champIds)
	{
		if(bannableChamps!=null)
		for (Entry<JButton,ChampionBanInfoDTO> ent:bannableChamps.entrySet())
		{
			for (int i = 0; i < champIds.length; i++)
			{
				if(champIds[i]==ent.getValue().championId)
				{
					ent.getKey().setEnabled(false);
				}
			}
		}
		for (Entry<JButton,ChampionDTO> ent:champions.entrySet())
		{
			for (int i = 0; i < champIds.length; i++)
			{
				if(champIds[i]==ent.getValue().championId)
				{
					ent.getKey().setEnabled(false);
				}
			}
		}
		bannedChamps.clear();
		for(int i:champIds)
		{
			bannedChamps.add(i);
		}
	}
	
	public void clearPickedChamps()
	{
		
		for (Entry<JButton,ChampionDTO> ent:champions.entrySet())
		{
			for (int i = 0; i < pickedChamps.size(); i++)
			{
				if(pickedChamps.get(i)==ent.getValue().championId)
				{
					ent.getKey().setEnabled(true);
				}
			}
		}
		pickedChamps.clear();
	}
	
	public void addPickedChamp(int id)
	{
		for (Entry<JButton,ChampionDTO> ent:champions.entrySet())
		{
			if(id==ent.getValue().championId)
			{
				ent.getKey().setEnabled(false);
			}
		}
		pickedChamps.add(id);
	}
}
