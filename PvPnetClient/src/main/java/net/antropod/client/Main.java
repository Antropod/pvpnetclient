package net.antropod.client;

import javax.swing.UnsupportedLookAndFeelException;

import org.jivesoftware.smack.SmackConfiguration;

import net.antropod.client.resources.GameMaps;

//import chrriis.dj.nativeswing.NativeSwing;
//import chrriis.dj.nativeswing.swtimpl.NativeInterface;
public class Main {

    public static void main(String[] args) {
        try {
            //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
            SmackConfiguration.setPacketReplyTimeout(10000);
    		GameMaps.load();
    		Globals.args(args);
            Globals.init();
            //Globals.startExtractor(); //Has thrown too much errors, reading my own errors is hard enough^^
            Globals.loadPlugins();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }
}
