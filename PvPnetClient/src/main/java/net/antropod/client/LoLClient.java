package net.antropod.client;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import net.antropod.util.Delegate;

import com.gvaneyck.rtmp.LoLRTMPSClient;
import com.gvaneyck.rtmp.RTMPSClient;
import com.gvaneyck.rtmp.TypedObject;
import com.riotgames.platform.catalog.champion.ChampionDTO;
import com.riotgames.platform.clientfacade.domain.LoginDataPacket;
import com.riotgames.platform.game.ChampionBanInfoDTO;
import com.riotgames.platform.game.GameDTO;
import com.riotgames.platform.game.PracticeGameConfig;
import com.riotgames.platform.game.practice.PracticeGameSearchResult;
import com.riotgames.platform.matchmaking.GameQueueConfig;
import com.riotgames.platform.matchmaking.MatchMakerParams;
import com.riotgames.platform.matchmaking.SearchingForMatchNotification;
import com.riotgames.platform.summoner.masterybook.MasteryBookDTO;
import com.riotgames.platform.summoner.masterybook.MasteryBookPageDTO;
import com.riotgames.platform.summoner.spellbook.SpellBookPageDTO;

/**
 * A class which extends an RTMP client, to provide some convenient methods for
 * the League of Legends servers.
 * 
 * @author Antropod
 * 
 */
public class LoLClient extends LoLRTMPSClient implements Runnable
{
	private ArrayList<RemoteCall> asyncCalls = new ArrayList<>();

	/**
	 * Initializes the RTMP client.
	 * 
	 * @param region
	 *            The region to connect to (EUW, EUNE, NA).
	 * @param clientVersion
	 *            The clients version, which can be found in the top left corner
	 *            of the official client. Version has to match the servers
	 *            version.
	 * @param user
	 *            The username which is used to login.
	 * @param pass
	 *            The account's password.
	 */
	public LoLClient(String region, String clientVersion, String user, String pass)
	{
		super(region, clientVersion, user, pass);
		(new Thread(this)).start();
	}

	/**
	 * Requests all champions from the server.
	 * 
	 * @return An array containing all champions.
	 */
	public ChampionDTO[] getAvailableChampions()
	{
		try
		{
			int id = invoke("inventoryService", "getAvailableChampions", new Object[] {});
			TypedObject result = getResult(id);
			Object[] o = result.getTO("data").getArray("body");
			TypedObject[] tos = Arrays.copyOf(o, o.length, TypedObject[].class);
			ChampionDTO[] cdto = new ChampionDTO[tos.length];
			for (int i = 0; i < tos.length; i++)
			{
				cdto[i] = new ChampionDTO(tos[i]);
			}
			return cdto;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Requests the URL for the store.
	 * 
	 * @return A String containing the store URL together with the session
	 *         token.
	 */
	public String getStoreUrl()
	{
		try
		{
			int id = invoke("loginService", "getStoreUrl", new Object[] {});
			TypedObject result = getResult(id);
			return result.getTO("data").getString("body");
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gathers a list of all Custom Games, currently in team selection.
	 * 
	 * @return An array, containing all relevant information for a custom game
	 *         list.
	 */
	public PracticeGameSearchResult[] listAllPracticeGames()
	{
		try
		{
			int id = invoke("gameService", "listAllPracticeGames", new Object[] {});
			TypedObject result = getResult(id);
			Object[] o = result.getTO("data").getArray("body");
			TypedObject[] tos = Arrays.copyOf(o, o.length, TypedObject[].class);
			PracticeGameSearchResult[] pgsr = new PracticeGameSearchResult[o.length];
			for (int i = 0; i < tos.length; i++)
			{
				pgsr[i] = new PracticeGameSearchResult(tos[i]);
			}
			return pgsr;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Joins a custom game. The custom game needs to be in the team selection,
	 * for someone to be able to join it.
	 * 
	 * @param gameId
	 *            The game's ID
	 * @param password
	 *            The game's password. Can be null if no password has been set.
	 * @return The game's information including summoners, chat room, etc.
	 */
	public void joinGame(double gameId, String password)
	{
		try
		{
			int id = invoke("gameService", "joinGame", new Object[] { gameId, password });
			TypedObject result = getResult(id);
			// return new GameDTO(result.getTO("data").getTO("body"));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		// return null;
	}

	/**
	 * Leaves the custom game, you are currently assigned to.
	 */
	public void quitGame()
	{
		try
		{
			int id = invoke("gameService", "quitGame", new Object[] {});
			join(id);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Starts Champion Selection for the custom game, you are currently assigned
	 * to. You need to be the owner of the game to start Champion Selection.
	 * 
	 * @param gameId
	 *            The game's ID
	 * @param optimisticLock
	 *            The optimistic lock of the game.
	 */
	public void startChampionSelection(double gameId, int optimisticLock)
	{
		try
		{
			int id = invoke("gameService", "startChampionSelection", new Object[] { gameId, optimisticLock });
			join(id);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Creates a custom game with the given configuration.
	 * 
	 * @param pgc
	 *            The game's configuration.
	 * @return An Object of the game.
	 */
	public GameDTO createPracticeGame(PracticeGameConfig pgc)
	{
		try
		{
			System.out.println(pgc.toTypedObject());
			int id = invoke("gameService", "createPracticeGame", new Object[] { pgc.toTypedObject() });
			TypedObject result = getResult(id);
			return new GameDTO(result.getTO("data").getTO("body"));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Requests the LoginDataPacket which contains information about server
	 * states, summoner data, etc.
	 * 
	 * @return The DataPacket
	 */
	public LoginDataPacket getLoginDataPacketForUser()
	{
		try
		{
			int id = invoke("clientFacadeService", "getLoginDataPacketForUser", new Object[] {});
			TypedObject result = getResult(id);
			return new LoginDataPacket(result.getTO("data").getTO("body"));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Calls all champions that are eligibla for ban. NOTE: This Method should
	 * only be called, when a Champion Selection is already in progress, which
	 * has the option to ban champions.
	 * 
	 * @return An Array, containing every champion, with the info if one of the
	 *         enemies owns it.
	 */
	public ChampionBanInfoDTO[] getChampionsForBan()
	{
		try
		{
			int id = invoke("gameService", "getChampionsForBan", new Object[] {});
			TypedObject result = getResult(id);
			Object[] o = result.getTO("data").getArray("body");
			TypedObject[] tos = Arrays.copyOf(o, o.length, TypedObject[].class);
			ChampionBanInfoDTO[] pgsr = new ChampionBanInfoDTO[o.length];
			for (int i = 0; i < tos.length; i++)
			{
				pgsr[i] = new ChampionBanInfoDTO(tos[i]);
			}
			return pgsr;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Informs the riot server, that the client is in sync with it
	 * 
	 * @param gameId
	 *            the game id, where champion selection is imminent
	 */
	public void setClientReceivedMassage_ChampSelect(double gameId)
	{
		try
		{
			int id = invoke("gameService", "setClientReceivedGameMessage", new Object[] { gameId, "CHAMP_SELECT_CLIENT" });
			join(id);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * Locks the currently selected champion in.
	 */
	public void championSelectCompleted()
	{
		try
		{
			Globals.client.invoke("gameService", "championSelectCompleted", new Object[] {});
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * @return All available matchmaking queues.
	 */
	public GameQueueConfig[] getAvailableQueues()
	{
		try
		{
			int id = invoke("matchmakerService", "getAvailableQueues", new Object[] {});
			TypedObject result = getResult(id);
			Object[] o = result.getTO("data").getArray("body");
			TypedObject[] tos = Arrays.copyOf(o, o.length, TypedObject[].class);
			GameQueueConfig[] pgsr = new GameQueueConfig[o.length];
			for (int i = 0; i < tos.length; i++)
			{
				pgsr[i] = new GameQueueConfig(tos[i]);
			}
			return pgsr;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Attaches the summoner to the given matchmaking queue
	 * @param mmp The matchmaking options
	 * @return A SearchingForMatchNotification object, which contains information about the estimated waiting time.
	 */
	public SearchingForMatchNotification attachToQueue(MatchMakerParams mmp)
	{
		try
		{
			int id = invoke("matchmakerService", "attachToQueue", new Object[] {mmp.toTypedObject()});
			TypedObject result = getResult(id);
			return new SearchingForMatchNotification(result.getTO("data").getTO("body"));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Attaches the summoner to the given matchmaking queue
	 * @param mmp The matchmaking options
	 * @return A SearchingForMatchNotification object, which contains information about the estimated waiting time.
	 */
	public SearchingForMatchNotification attachTeamToQueue(MatchMakerParams mmp)
	{
		try
		{
			int id = invoke("matchmakerService", "attachTeamToQueue", new Object[] {mmp.toTypedObject()});
			TypedObject result = getResult(id);
			return new SearchingForMatchNotification(result.getTO("data").getTO("body"));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Accepts or declines the matchmade game.
	 * @param b True for accpeting the game, False for declining.
	 */
	public void acceptPoppedGame(boolean b)
	{
		try
		{
			invoke("gameService", "acceptPoppedGame", new Object[] {b});
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void acceptInviteForMatchmakingGame(int inviteId)
	{
		try
		{
			invoke("matchmakerService", "acceptInviteForMatchmakingGame", new Object[] {inviteId});
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Selects the Rune Page, that will be used next game. [Methods, prefixed
	 * with an "async_" will be executed in a seperate Thread!]
	 * 
	 * @param callback
	 *            A Delegate to the method that will be called after the server
	 *            answered.
	 * @param sbp
	 *            The Rune Page that should be selected.
	 */
	public void async_selectDefaultSpellBookPage(Delegate callback, SpellBookPageDTO sbp)
	{
		async_invoke(callback, "spellBookService", "selectDefaultSpellBookPage", sbp.toTypedObject());
	}

	/**
	 * Saves the entire Mastery Book to the server. This Method can be called in
	 * Champion Selection, to select the Mastery Page which will be used in that
	 * game. [Methods, prefixed with an "async_" will be executed in a seperate
	 * Thread!]
	 * 
	 * @param callback
	 *            A Delegate to the method that will be called after the server
	 *            answered.
	 * @param sbp
	 *            The Mastery Book.
	 */
	public void async_saveMasteryBook(Delegate callback, MasteryBookDTO sbp)
	{
		async_invoke(callback, "masteryBookService", "saveMasteryBook", sbp.toTypedObject());
	}

	/**
	 * See: {@link #invoke(String, Object, Object)} [Methods, prefixed with an
	 * "async_" will be executed in a seperate Thread!]
	 * 
	 * @param callback
	 *            A Delegate to the method that will be called after the server
	 *            answered.
	 * @param destination
	 *            The destination.
	 * @param operation
	 *            The Operation
	 * @param The
	 *            Arguments
	 */
	public void async_invoke(Delegate callback, String destination, String operation, Object... args)
	{
		asyncCalls.add(new RemoteCall(callback, destination, operation, args));
	}

	@Override
	public void run()
	{

		while (true)
		{
			try
			{

				if (asyncCalls.size() > 0)
				{
					RemoteCall rc = asyncCalls.get(0);
					asyncCalls.remove(0);
					int id = invoke(rc.destination, rc.operation, rc.args);
					TypedObject result = getResult(id).getTO("data").getTO("body");
					if (rc.delegate != null)
						if (result != null)
						{
							Object obj = Class.forName(result.type).getConstructor(TypedObject.class).newInstance(result);
							rc.delegate.invoke(obj);
						} else
						{
							rc.delegate.invoke();
						}
				} else
				{
					sleep(100);
				}
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}

class RemoteCall
{
	public String destination, operation;
	public Object[] args;
	public Delegate delegate;

	public RemoteCall(Delegate callback, String dest, String op, Object... obj)
	{
		destination = dest;
		operation = op;
		args = obj;
		delegate = callback;
	}
}