package net.antropod.client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.riotgames.platform.game.PlayerCredentialsDto;

import net.antropod.client.resources.Sound;

/**
 * Provides methods, to start the League of Legends game client.
 * @author Antropod
 *
 */
public class GameClient
{
	/**
	 * @param f The client's exe.
	 * @throws FileNotFoundException If the game client doesn't exist at the given location.
	 * @throws IllegalArgumentException If the argument's file is not the LoL game client. 
	 */
	private File gameClient;
	public GameClient(File f) throws IllegalArgumentException, FileNotFoundException
	{
		if(!f.getAbsolutePath().endsWith("League of Legends.exe"))
			throw new IllegalArgumentException("The path \""+f.getAbsolutePath()+"\" to the game client is invalid!");
		else if(!f.exists())
			throw new FileNotFoundException("Can't find the game client at \""+f.getAbsolutePath()+"\".");
		else
			gameClient=f;
	}
	
	/**
	 * Starts the game client and connects it to a game.
	 * @param serverIp The server's IP
	 * @param serverPort The server's port
	 * @param encryptionKey The Encryption Key;
	 * @param summonerId The SummonerID
	 * @return The instance of the game client
	 */
	public Process startGame(String serverIp, int serverPort, String encryptionKey, int summonerId)
	{
		Process game=null;
		String[] cmd = new String[] { gameClient.getAbsolutePath(), "" +8000, "LoLLauncher.exe",
				"LoLClient.exe",
				serverIp + " " + serverPort + " " + encryptionKey + " " + summonerId };
		try
		{
			game = Runtime.getRuntime().exec(cmd, null, gameClient.getParentFile());
			new StreamGobbler(game.getInputStream());
			new StreamGobbler(game.getErrorStream());
		} catch (IOException e)
		{
			for (String s : cmd)
				System.out.println("cmd=" + s);
			e.printStackTrace();
		}
		return game;
	}
	
	/**
	 * Starts the game client and connects it to a game.
	 * @param dto The PlayerCredentialsDto
	 * @return The instance of the game client
	 */
	public Process startGame(PlayerCredentialsDto dto)
	{
		return startGame(dto.serverIp,dto.serverPort,dto.encryptionKey,dto.summonerId);
	}
	
	
}
