package net.antropod.client.managers;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JLabel;

import com.riotgames.platform.game.practice.PracticeGameSearchResult;


import net.antropod.client.Globals;
import net.antropod.client.components.JIcon;
import net.antropod.client.components.JQueueTimePanel;
import net.antropod.client.components.View;
import net.antropod.client.resources.ImageAssets;
import net.antropod.client.resources.LanguageDB;
import net.antropod.util.Delegate;

/**
 * Manages the home screen.
 * @author Antropod
 *
 */
public class HomeScreenManager implements IManager
{

	private JButton playButton;
	
	private JLabel ipLabel,rpLabel;
	private JIcon summonerIcon,ipIcon,rpIcon;
	private JQueueTimePanel queuePanel;
	
	@Override
	public void init(WindowManager wm)
	{
		View view=new View(WindowManager.HOME_VIEW);
		playButton=new JButton(LanguageDB.getLocalizedString("PLAY"));
		playButton.setFont(playButton.getFont().deriveFont(22f));
		playButton.setActionCommand("play");
		playButton.setBounds(0,0,150,100);
		playButton.setBackground(Color.red);
		
		
		ipIcon=new JIcon();
		ipIcon.setIcon(ImageAssets.IP_ICON);
		ipIcon.setBounds(320, 0, 30, 30);
		
		ipLabel=new JLabel("1337");
		ipLabel.setForeground(Color.white);
		ipLabel.setHorizontalAlignment(JLabel.RIGHT);
		ipLabel.setBounds(350, 0, 70, 30);
		ipLabel.setFont(ipLabel.getFont().deriveFont(18f));
		
		rpIcon=new JIcon();
		rpIcon.setBounds(320, 50, 30, 30);
		rpIcon.setIcon(ImageAssets.RP_ICON);
		
		rpLabel=new JLabel("1337");
		rpLabel.setForeground(Color.white);
		rpLabel.setHorizontalAlignment(JLabel.RIGHT);
		rpLabel.setBounds(350, 50, 70, 30);
		rpLabel.setFont(rpLabel.getFont().deriveFont(18f));
		
		summonerIcon=new JIcon();
		summonerIcon.setBounds(430,10,64,64);	
		
		queuePanel=new JQueueTimePanel();
		queuePanel.setLocation(500, 0);
		queuePanel.setVisible2(false);
		
		view.addComponent(playButton);
		view.addComponent(ipIcon);
		view.addComponent(ipLabel);
		view.addComponent(rpIcon);
		view.addComponent(rpLabel);
		view.addComponent(summonerIcon);
		view.addComponent(queuePanel);
		
		view.setBackgroundImage(ImageAssets.STANDARD_BG);
				
		wm.addView(view);
		
	}
	
	@Override
	public void registerEvents(WindowManager wm)
	{
		wm.registerEvent(playButton, new Delegate(this, "playButtonAction"));
	}
	
	public void setSummonerIcon(int id)
	{
		summonerIcon.setIcon(ImageAssets.getSummonerIcon(id));
	}
	
	public void setIPBalance(int i)
	{
		ipLabel.setText(""+i);
	}
	
	public void setRPBalance(int i)
	{
		rpLabel.setText(""+i);
	}
	
	public JButton getPlayButton()
	{
		return playButton;
	}
	
	public JIcon getIPIcon()
	{
		return ipIcon;
	}
	
	public JIcon getRPIcon()
	{
		return rpIcon;
	}
	
	public JIcon getSummonerIcon()
	{
		return summonerIcon;
	}
	
	public JLabel getIPLabel()
	{
		return ipLabel;
	}
	
	public JLabel getRPLabel()
	{
		return rpLabel;
	}
	public JQueueTimePanel getQueuePanel()
	{
		return queuePanel;
	}
	public void playButtonAction()
	{
		Globals.mainFrame.setCurrentView(WindowManager.PLAY_SCREEN_VIEW);
		Globals.winManager.getPlayScreenManager().normalGameButtonAction();
		//Globals.mainFrame.setCurrentView(WindowManager.CUSTOM_GAME_LIST_VIEW);
        //PracticeGameSearchResult[] to = Globals.client.listAllPracticeGames();
        //Globals.winManager.getCustomGameListManager().listGames(to);
	}
}
