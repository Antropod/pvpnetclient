package net.antropod.client.managers;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPopupMenu;

import com.riotgames.platform.game.GameTypeConfigDTO;
import com.riotgames.platform.game.map.GameMap;
import com.riotgames.platform.game.practice.PracticeGameSearchResult;
import com.riotgames.platform.matchmaking.GameQueueConfig;
import com.riotgames.platform.matchmaking.MatchMakerParams;
import com.riotgames.platform.matchmaking.SearchingForMatchNotification;

import net.antropod.client.Globals;
import net.antropod.client.components.JGamePoppedDialog;
import net.antropod.client.components.JIcon;
import net.antropod.client.components.JMapSelectionPanel;
import net.antropod.client.components.View;
import net.antropod.client.resources.GameMaps;
import net.antropod.client.resources.ImageAssets;
import net.antropod.client.resources.LanguageDB;
import net.antropod.client.serverdata.MMQProvider.QueueType;
import net.antropod.util.Delegate;

public class PlayScreenManager implements IManager,ItemListener
{
	private QueueType selectedQType=QueueType.NORMAL_GAME;
	
	private JMapSelectionPanel mapPanel;
	private JIcon minimapIcon;
	private JButton customGameButton,rankedGameButton,rankedTeamGameButton,normalGameButton,coopGameButton;
	private JComboBox<GameTypeConfigDTO> pickTypeBox;
	private DefaultComboBoxModel<GameTypeConfigDTO> pickTypeBoxModel;
	private JButton soloMatchButton,teamMatchButton;
	
	private JGamePoppedDialog poppedGameDialog;

	public PlayScreenManager()
	{
		
	}

	@Override
	public void init(WindowManager wm)
	{
		View view=new View(WindowManager.PLAY_SCREEN_VIEW);
		
		mapPanel=new JMapSelectionPanel();
		mapPanel.addMap(GameMaps.getMap(1));
		mapPanel.addMap(GameMaps.getMap(10));
		mapPanel.addMap(GameMaps.getMap(8));
		mapPanel.addMap(GameMaps.getMap(7), false);//disabled
		mapPanel.setBounds(100, 130, 550, 400);
		mapPanel.addItemListener(this);
		
		minimapIcon=new JIcon();
		minimapIcon.setIcon(ImageAssets.MINIMAP1);
		minimapIcon.setBounds(mapPanel.getWidth()+mapPanel.getX()+100, 130, 400, 400);
		
		customGameButton=new JButton(LanguageDB.getLocalizedString("PRACTICE_GAME"));//Will be externalized soon
		customGameButton.setBounds((mapPanel.getWidth()-200)/2+mapPanel.getX(),mapPanel.getY()+mapPanel.getHeight()+10,200,70);
		customGameButton.setFont(customGameButton.getFont().deriveFont(Font.BOLD,20f));
		
		normalGameButton=new JButton(LanguageDB.getLocalizedString("NORMAL_GAME"));
		normalGameButton.setBounds(mapPanel.getX(),customGameButton.getY()+customGameButton.getHeight(),mapPanel.getWidth()/4,50);
		normalGameButton.setFont(normalGameButton.getFont().deriveFont(Font.BOLD,12f));
		
		coopGameButton=new JButton(LanguageDB.getLocalizedString("COOP_GAME"));
		coopGameButton.setBounds(mapPanel.getX()+mapPanel.getWidth()/4,customGameButton.getY()+customGameButton.getHeight(),mapPanel.getWidth()/4,50);
		coopGameButton.setFont(coopGameButton.getFont().deriveFont(Font.BOLD,12f));
		
		rankedGameButton=new JButton(LanguageDB.getLocalizedString("RANKED_GAME"));
		rankedGameButton.setBounds(mapPanel.getX()+(mapPanel.getWidth()/4*2),customGameButton.getY()+customGameButton.getHeight(),mapPanel.getWidth()/4,50);
		rankedGameButton.setFont(rankedGameButton.getFont().deriveFont(Font.BOLD,12f));
		
		rankedTeamGameButton=new JButton("<html><center>"+LanguageDB.getLocalizedString("RANKED_TEAM_GAME")+"</center></html>");
		rankedTeamGameButton.setBounds(mapPanel.getX()+(mapPanel.getWidth()/4*3),customGameButton.getY()+customGameButton.getHeight(),mapPanel.getWidth()/4,50);
		rankedTeamGameButton.setFont(rankedTeamGameButton.getFont().deriveFont(Font.BOLD,12f));
		
		pickTypeBoxModel=new DefaultComboBoxModel<>();
		pickTypeBox=new JComboBox<>(pickTypeBoxModel);
		pickTypeBox.setBounds(rankedGameButton.getX()-100,rankedGameButton.getY()+rankedGameButton.getHeight()+10,200,30);
		pickTypeBox.setFont(pickTypeBox.getFont().deriveFont(Font.BOLD));
		
		soloMatchButton=new JButton(LanguageDB.getLocalizedString("MATCH_ME_WITH_TEAMMATES"));
		soloMatchButton.setBounds(minimapIcon.getX(), normalGameButton.getY(), 200, 50);
		soloMatchButton.setFont(soloMatchButton.getFont().deriveFont(Font.BOLD));
		
		teamMatchButton=new JButton(LanguageDB.getLocalizedString("I_WILL_INVITE_MY_OWN_TEAMMATES"));
		teamMatchButton.setBounds(soloMatchButton.getX()+soloMatchButton.getWidth(), normalGameButton.getY(), 200, 50);
		teamMatchButton.setFont(teamMatchButton.getFont().deriveFont(Font.BOLD));
		
		poppedGameDialog=new JGamePoppedDialog(wm.getManagedWindow());
		
		view.addComponent(wm.getHomeScreenManager().getPlayButton());
		view.addComponent(wm.getHomeScreenManager().getIPIcon());
		view.addComponent(wm.getHomeScreenManager().getRPIcon());
		view.addComponent(wm.getHomeScreenManager().getIPLabel());
		view.addComponent(wm.getHomeScreenManager().getRPLabel());
		view.addComponent(wm.getHomeScreenManager().getSummonerIcon());
		view.addComponent(minimapIcon);
		view.addComponent(mapPanel);
		view.addComponent(customGameButton);
		view.addComponent(rankedGameButton);
		view.addComponent(rankedTeamGameButton);
		view.addComponent(normalGameButton);
		view.addComponent(coopGameButton);
		view.addComponent(pickTypeBox);
		view.addComponent(soloMatchButton);
		view.addComponent(teamMatchButton);
		wm.addView(view);
	}

	@Override
	public void registerEvents(WindowManager wm)
	{
		wm.registerEvent(customGameButton, 		new Delegate(this, "customGameButtonAction"));
		wm.registerEvent(normalGameButton, 		new Delegate(this, "normalGameButtonAction"));
		wm.registerEvent(coopGameButton, 		new Delegate(this, "coopGameButtonAction"));
		wm.registerEvent(rankedGameButton, 		new Delegate(this, "rankedGameButtonAction"));
		wm.registerEvent(rankedTeamGameButton, 	new Delegate(this, "rankedTeamGameButtonAction"));
		wm.registerEvent(soloMatchButton, 		new Delegate(this, "soloMatchButtonAction"));
		wm.registerEvent(teamMatchButton, 		new Delegate(this, "teamMatchButtonAction"));
	}
	
	public void customGameButtonAction()
	{
		Globals.mainFrame.setCurrentView(WindowManager.CUSTOM_GAME_LIST_VIEW);
        PracticeGameSearchResult[] to = Globals.client.listAllPracticeGames();
        Globals.winManager.getCustomGameListManager().listGames(to);
	}
	
	public void normalGameButtonAction()
	{
		Integer[] pt=Globals.mmqProvider.getPickTypeIdsForMapAndType(mapPanel.getSelectedMap(), QueueType.NORMAL_GAME);
		pickTypeBoxModel.removeAllElements();
		for(int i:pt)
		{
			pickTypeBoxModel.addElement(Globals.gtcProvider.getConfigById(i));
		}
		setSelectedQueueType(QueueType.NORMAL_GAME);
	}
	
	public void coopGameButtonAction()
	{
		Integer[] pt=Globals.mmqProvider.getPickTypeIdsForMapAndType(mapPanel.getSelectedMap(), QueueType.COOP_GAME);
		pickTypeBoxModel.removeAllElements();
		for(int i:pt)
		{
			pickTypeBoxModel.addElement(Globals.gtcProvider.getConfigById(i));
		}
		setSelectedQueueType(QueueType.COOP_GAME);
	}
	
	public void rankedGameButtonAction()
	{
		Integer[] pt=Globals.mmqProvider.getPickTypeIdsForMapAndType(mapPanel.getSelectedMap(), QueueType.RANKED_GAME);
		pickTypeBoxModel.removeAllElements();
		for(int i:pt)
		{
			pickTypeBoxModel.addElement(Globals.gtcProvider.getConfigById(i));
		}
		setSelectedQueueType(QueueType.RANKED_GAME);
	}
	
	public void rankedTeamGameButtonAction()
	{
		Integer[] pt=Globals.mmqProvider.getPickTypeIdsForMapAndType(mapPanel.getSelectedMap(), QueueType.RANKED_TEAM_GAME);
		pickTypeBoxModel.removeAllElements();
		for(int i:pt)
		{
			pickTypeBoxModel.addElement(Globals.gtcProvider.getConfigById(i));
		}
		setSelectedQueueType(QueueType.RANKED_TEAM_GAME);
	}
	
	public void soloMatchButtonAction()
	{
		GameQueueConfig gqc=Globals.mmqProvider.getQueue(mapPanel.getSelectedMap(), selectedQType, ((GameTypeConfigDTO)pickTypeBox.getSelectedItem()).id);
		MatchMakerParams mmp=new MatchMakerParams();
		mmp.botDifficulty="MEDIUM";
		mmp.queueIds=new Integer[]{gqc.id};
		SearchingForMatchNotification sfmn=Globals.client.attachToQueue(mmp);
		Globals.mainFrame.setCurrentView(WindowManager.HOME_VIEW);
		Globals.winManager.getHomeScreenManager().getPlayButton().setEnabled(false);
		Globals.winManager.getHomeScreenManager().getQueuePanel().setApprox(sfmn.joinedQueues[0].waitTime);
		Globals.winManager.getHomeScreenManager().getQueuePanel().setVisible2(true);
	}
	
	public void teamMatchButtonAction()
	{
		Globals.winManager.getTeamLobbyManager().prepareTeamLobby();
	}
	
	@Override
	public void itemStateChanged(ItemEvent arg0)
	{
		GameMap map = mapPanel.getSelectedMap();
		switch (map.mapId)
		{
			case 7:
				minimapIcon.setIcon(ImageAssets.MINIMAP7);
				break;
			case 8:
				minimapIcon.setIcon(ImageAssets.MINIMAP8);
				break;
			case 10:
				minimapIcon.setIcon(ImageAssets.MINIMAP10);
				break;
			default:
				minimapIcon.setIcon(ImageAssets.MINIMAP1);
				break;
		}
		QueueType[] qarr=Globals.mmqProvider.getQueueTypesForMap(mapPanel.getSelectedMap());
		setQueueTypes(qarr);
		normalGameButtonAction();
	
		
	}
	
	private void setQueueTypes(QueueType[] qarr)
	{
		boolean normal=false,coop=false,ranked=false,rankedteam=false;
		for(QueueType qt:qarr)
		{
			if(QueueType.NORMAL_GAME==qt) normal=true;
			if(QueueType.COOP_GAME==qt) coop=true;
			if(QueueType.RANKED_GAME==qt) ranked=true;
			if(QueueType.RANKED_TEAM_GAME==qt) rankedteam=true;
			
		}
		normalGameButton.setEnabled(normal);
		coopGameButton.setEnabled(coop);
		rankedGameButton.setEnabled(ranked);
		rankedTeamGameButton.setEnabled(rankedteam);
	}
	
	public void setSelectedQueueType(QueueType qt)
	{
		selectedQType=qt;
		normalGameButton.setBackground(qt==QueueType.NORMAL_GAME?Color.ORANGE:Color.WHITE);
		coopGameButton.setBackground(qt==QueueType.COOP_GAME?Color.ORANGE:Color.WHITE);
		rankedGameButton.setBackground(qt==QueueType.RANKED_GAME?Color.ORANGE:Color.WHITE);
		rankedTeamGameButton.setBackground(qt==QueueType.RANKED_TEAM_GAME?Color.ORANGE:Color.WHITE);
	}
	
	public JGamePoppedDialog getPoppedGameDialog()
	{
		return poppedGameDialog;
	}

}
