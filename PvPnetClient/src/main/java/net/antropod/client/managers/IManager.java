package net.antropod.client.managers;

import java.util.ArrayList;

import javax.swing.JComponent;

import net.antropod.client.components.View;
/**
 * This interface has to be implemented by all submanagers from plugins.
 * @author Antropod
 *
 */
public interface IManager
{
	public void init(WindowManager wm);
	public void registerEvents(WindowManager wm);
}
