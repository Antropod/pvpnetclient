package net.antropod.client.managers;

import java.awt.Color;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import net.antropod.client.Globals;
import net.antropod.client.components.JSummoner;
import net.antropod.client.components.TeamTableRenderer;
import net.antropod.client.components.View;

public class TeamLobbyManager implements IManager
{
	private JTable participantTable;
	private DefaultTableModel participantTableModel;
	private JScrollPane tableScrollPane;
	public TeamLobbyManager()
	{
		
	}
	@Override
	public void init(WindowManager wm)
	{
		
		View view=new View(WindowManager.TEAM_LOBBY_VIEW);
		participantTableModel=new DefaultTableModel(new Object[]{"Team"},5);
		participantTable=new JTable(participantTableModel);
		participantTable.setBackground(Color.black);
		participantTable.setForeground(Color.white);
		participantTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		participantTable.setCellSelectionEnabled(false);
		participantTable.setRowSelectionAllowed(false);
		participantTable.setColumnSelectionAllowed(false);
		participantTable.setDefaultRenderer(JSummoner.class, new TeamTableRenderer());
		participantTable.setRowHeight(JSummoner.STD_HEIGHT);
		participantTable.getTableHeader().getColumnModel().getColumn(0).setWidth(JSummoner.STD_WIDTH);
		participantTable.getColumnModel().getColumn(0).setCellRenderer(new TeamTableRenderer());
		participantTable.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
		participantTableModel.setValueAt(new JSummoner(0,"SneakyGuenther",true),0,0);
		
		tableScrollPane=new JScrollPane(participantTable);
		tableScrollPane.setBounds(100, 180, JSummoner.STD_WIDTH, JSummoner.STD_HEIGHT * 5 +40);
		tableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		
		
		view.addComponent(tableScrollPane);
		view.addComponent(wm.getHomeScreenManager().getPlayButton());
		view.addComponent(wm.getHomeScreenManager().getIPIcon());
		view.addComponent(wm.getHomeScreenManager().getRPIcon());
		view.addComponent(wm.getHomeScreenManager().getIPLabel());
		view.addComponent(wm.getHomeScreenManager().getRPLabel());
		view.addComponent(wm.getHomeScreenManager().getSummonerIcon());
		wm.addView(view);
	}

	@Override
	public void registerEvents(WindowManager wm)
	{
	}
	
	public void prepareTeamLobby()
	{
		Globals.mainFrame.setCurrentView(WindowManager.TEAM_LOBBY_VIEW);
	}

}
