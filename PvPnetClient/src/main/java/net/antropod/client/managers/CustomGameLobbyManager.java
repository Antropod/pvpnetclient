package net.antropod.client.managers;

import java.awt.Color;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import com.riotgames.platform.game.BotParticipant;
import com.riotgames.platform.game.GameDTO;
import com.riotgames.platform.game.KnownParticipant;
import com.riotgames.platform.game.Participant;
import com.riotgames.platform.game.PlayerParticipant;
import com.riotgames.platform.game.practice.PracticeGameSearchResult;

import net.antropod.client.Globals;
import net.antropod.client.components.JSummoner;
import net.antropod.client.components.TeamTableRenderer;
import net.antropod.client.components.TeamTableModel;
import net.antropod.client.components.View;
import net.antropod.client.resources.ImageAssets;
import net.antropod.client.resources.LanguageDB;
import net.antropod.client.resources.SQLDatabase;
import net.antropod.client.resources.Sound;
import net.antropod.util.Delegate;
/**
 * Manages the custom game lobby.
 * @author Antropod
 *
 */
public class CustomGameLobbyManager implements IManager
{
	private JTable teamTable;
	private JScrollPane tableScrollPane;
	private JButton quitGameButton, startGameButton;

	private TeamTableModel teamTableModel;

	@Override
	public void init(WindowManager wm)
	{
		View view = new View(WindowManager.CUSTOM_GAME_LOBBY_VIEW);

		teamTableModel = new TeamTableModel(new Object[] { "Team 1", "Team 2" }, 5);
		;

		teamTable = new JTable();
		teamTable.setBackground(Color.black);
		teamTable.setForeground(Color.white);
		teamTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		teamTable.setCellSelectionEnabled(false);
		teamTable.setRowSelectionAllowed(false);
		teamTable.setColumnSelectionAllowed(false);
		teamTable.setDefaultRenderer(JSummoner.class, new TeamTableRenderer());
		teamTable.setModel(teamTableModel);
		teamTable.setRowHeight(JSummoner.STD_HEIGHT);
		teamTable.getTableHeader().getColumnModel().getColumn(0).setWidth(JSummoner.STD_WIDTH);
		teamTable.getTableHeader().getColumnModel().getColumn(1).setWidth(JSummoner.STD_WIDTH);
		teamTable.getColumnModel().getColumn(0).setCellRenderer(new TeamTableRenderer());
		teamTable.getColumnModel().getColumn(1).setCellRenderer(new TeamTableRenderer());
		teamTable.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
		teamTable.getTableHeader().getColumnModel().getColumn(1).setResizable(false);

		tableScrollPane = new JScrollPane(teamTable);
		tableScrollPane.setBounds(100, 180, JSummoner.STD_WIDTH * 2, JSummoner.STD_HEIGHT * 5 + 23);

		quitGameButton = new JButton(LanguageDB.getLocalizedString("QUIT"));
		quitGameButton.setBounds(100, 180 + JSummoner.STD_HEIGHT * 5 + 23, JSummoner.STD_WIDTH, 50);

		startGameButton = new JButton(LanguageDB.getLocalizedString("START_GAME"));
		startGameButton.setBounds(100 + quitGameButton.getWidth(), 180 + JSummoner.STD_HEIGHT * 5 + 23, JSummoner.STD_WIDTH, 50);

		view.addComponent(wm.getHomeScreenManager().getPlayButton());
		view.addComponent(wm.getHomeScreenManager().getIPIcon());
		view.addComponent(wm.getHomeScreenManager().getRPIcon());
		view.addComponent(wm.getHomeScreenManager().getIPLabel());
		view.addComponent(wm.getHomeScreenManager().getRPLabel());
		view.addComponent(wm.getHomeScreenManager().getSummonerIcon());
		view.addComponent(tableScrollPane);
		view.addComponent(quitGameButton);
		view.addComponent(startGameButton);

		wm.addView(view);
	}

	@Override
	public void registerEvents(WindowManager wm)
	{
		wm.registerEvent(quitGameButton, new Delegate(this, "quitGame"));
		wm.registerEvent(startGameButton, new Delegate(this, "startGame"));
	}

	public void quitGame()
	{
		Globals.client.quitGame();
		Sound.stopMusic();
		Globals.winManager.setCurrentGame(null);

	}
	
	public void startGame()
	{
        Globals.client.startChampionSelection(Globals.winManager.getCurrentGame().id, Globals.winManager.getCurrentGame().optimisticLock);

	}

	public void setGame(GameDTO pto)
	{
		if (pto == null)
			return;
		Participant[] o = pto.teamOne;
		if (o == null)
			return;
		for (int i = 0; i < 5; i++)
		{
			teamTableModel.setValueAt("", i, 0);
			teamTableModel.setValueAt("", i, 1);
		}
		for (int i = 0; i < o.length; i++)
		{

			if (o[i] instanceof PlayerParticipant)
			{
				PlayerParticipant to = (PlayerParticipant) o[i];
				JSummoner j = new JSummoner(to.profileIconId, to.summonerName, pto.ownerSummary.summonerName.equals(to.summonerName));
				teamTableModel.setValueAt(j, i, 0);
			} else
			{
				BotParticipant to = (BotParticipant) o[i];
				BufferedImage icon = ImageAssets.getChampionIcon(SQLDatabase.getChampionIDByName(to.summonerInternalName.split("_")[1]));
				JSummoner j = new JSummoner(icon, to.summonerName, pto.ownerSummary.summonerName.equals(to.summonerName));
				teamTableModel.setValueAt(j, i, 0);
			}
		}
		o = pto.teamTwo;
		for (int i = 0; i < o.length; i++)
		{
			if (o[i] instanceof PlayerParticipant)
			{
				PlayerParticipant to = (PlayerParticipant) o[i];
				JSummoner j = new JSummoner(to.profileIconId, to.summonerName, pto.ownerSummary.summonerName.equals(to.summonerName));
				teamTableModel.setValueAt(j, i, 1);
			} else
			{
				BotParticipant to = (BotParticipant) o[i];
				BufferedImage icon = ImageAssets.getChampionIcon(SQLDatabase.getChampionIDByName(to.summonerInternalName.split("_")[1]));
				JSummoner j = new JSummoner(icon, to.summonerName, pto.ownerSummary.summonerName.equals(to.summonerName));
				teamTableModel.setValueAt(j, i, 1);
			}
		}
		if (Globals.loginPacket.allSummonerData.summoner.internalName.equals(pto.ownerSummary.summonerInternalName))
		{
			startGameButton.setVisible(true);
		} else
		{
			startGameButton.setVisible(false);
		}

	}

}
