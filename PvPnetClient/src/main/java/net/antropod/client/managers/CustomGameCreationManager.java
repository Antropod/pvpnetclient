package net.antropod.client.managers;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import net.antropod.client.Globals;
import net.antropod.client.components.JClientWindow;
import net.antropod.client.components.JIcon;
import net.antropod.client.components.JMapSelectionPanel;
import net.antropod.client.components.View;
import net.antropod.client.resources.GameMaps;
import net.antropod.client.resources.ImageAssets;
import net.antropod.client.resources.LanguageDB;
import net.antropod.util.Delegate;

import com.riotgames.platform.game.GameDTO;
import com.riotgames.platform.game.GameTypeConfigDTO;
import com.riotgames.platform.game.PracticeGameConfig;
import com.riotgames.platform.game.map.GameMap;
/**
 * Manages custom game creation.
 * @author Antropod
 *
 */
public class CustomGameCreationManager implements IManager, ItemListener
{

	private JMapSelectionPanel mapPanel;
	private JComboBox<GameTypeConfigDTO> pickModeBox;
	private JTextField gameNameField;
	private JPasswordField gamePasswordField;
	private JIcon minimapIcon;
	private JButton quitGameButton, createGameButton;

	@Override
	public void init(WindowManager wm)
	{
		View view = new View(WindowManager.CUSTOM_GAME_CREATE_VIEW);
		
		mapPanel=new JMapSelectionPanel();
		mapPanel.setBounds(10, 20, JClientWindow.FRAME_WIDTH/2-20, JClientWindow.FRAME_HEIGHT/2);
		mapPanel.addMap(GameMaps.getMap(1));
		mapPanel.addMap(GameMaps.getMap(7));
		mapPanel.addMap(GameMaps.getMap(8));
		mapPanel.addMap(GameMaps.getMap(10));
		mapPanel.addItemListener(this);
		view.addComponent(mapPanel);

		pickModeBox = new JComboBox<>();
		pickModeBox.setBounds(80, JClientWindow.FRAME_HEIGHT/2+70, 300, 30);
		view.addComponent(pickModeBox);

		gameNameField = new JTextField();
		gameNameField.setBounds(80, JClientWindow.FRAME_HEIGHT/2+130, 300, 30);
		view.addComponent(gameNameField);

		gamePasswordField = new JPasswordField();
		gamePasswordField.setBounds(80, JClientWindow.FRAME_HEIGHT/2+190, 300, 30);
		view.addComponent(gamePasswordField);

		minimapIcon = new JIcon();
		minimapIcon.setIcon(ImageAssets.MINIMAP1);
		minimapIcon.setBounds(JClientWindow.FRAME_WIDTH / 2, 110, JClientWindow.FRAME_WIDTH / 2 - 10, JClientWindow.FRAME_WIDTH / 2);
		view.addComponent(minimapIcon);

		quitGameButton = new JButton(LanguageDB.getLocalizedString("QUIT"));
		quitGameButton.setBounds(20, JClientWindow.FRAME_HEIGHT - 80, 150, 30);
		view.addComponent(quitGameButton);

		createGameButton = new JButton(LanguageDB.getLocalizedString("CREATE_GAME"));
		createGameButton.setBounds(JClientWindow.FRAME_WIDTH / 2 - 170, JClientWindow.FRAME_HEIGHT - 80, 150, 30);
		view.addComponent(createGameButton);

		wm.addView(view);
	}

	@Override
	public void registerEvents(WindowManager wm)
	{
		wm.registerEvent(quitGameButton, new Delegate(this, "quit"));
		wm.registerEvent(createGameButton, new Delegate(this, "create"));
	}

	public void quit()
	{
		Globals.mainFrame.setCurrentView(WindowManager.CUSTOM_GAME_LIST_VIEW);
	}

	public void create()
	{
		PracticeGameConfig pgc = new PracticeGameConfig();
		pgc.allowSpectators = "ALL";
		pgc.gameMap = mapPanel.getSelectedMap();
		pgc.gameMode = mapPanel.getSelectedMap().mapId==7?"ARAM":mapPanel.getSelectedMap().mapId==8?"ODIN":"CLASSIC";//Throws a GameModeNotSupportedException, if the wrong game mode is selected.
		pgc.gameName = gameNameField.getText();
		pgc.gamePassword = new String(gamePasswordField.getPassword());
		pgc.gameTypeConfig = ((GameTypeConfigDTO) pickModeBox.getSelectedItem()).id;
		pgc.maxNumPlayers = mapPanel.getSelectedMap().totalPlayers;
		GameDTO game = Globals.client.createPracticeGame(pgc);
		Globals.winManager.setCurrentGame(game);
	}

	@Override
	public void itemStateChanged(ItemEvent arg0)
	{
		
		GameMap map = mapPanel.getSelectedMap();
		switch (map.mapId)
		{
			case 7:
				minimapIcon.setIcon(ImageAssets.MINIMAP7);
				break;
			case 8:
				minimapIcon.setIcon(ImageAssets.MINIMAP8);
				break;
			case 10:
				minimapIcon.setIcon(ImageAssets.MINIMAP10);
				break;
			default:
				minimapIcon.setIcon(ImageAssets.MINIMAP1);
				break;
		}
	}

	public void addPickMode(GameTypeConfigDTO cfg)
	{
		pickModeBox.addItem(cfg);
	}

	public void setCustomGameName(String s)
	{
		gameNameField.setText(s);
	}

}
