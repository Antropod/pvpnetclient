package net.antropod.client.managers;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableRowSorter;

import com.riotgames.platform.game.GameDTO;
import com.riotgames.platform.game.practice.PracticeGameSearchResult;

import net.antropod.client.Globals;
import net.antropod.client.components.GameTableModel;
import net.antropod.client.components.JClientWindow;
import net.antropod.client.components.IconTableCellRenderer;
import net.antropod.client.components.View;
import net.antropod.client.resources.GameMaps;
import net.antropod.client.resources.ImageAssets;
import net.antropod.client.resources.LanguageDB;
import net.antropod.util.Delegate;
/**
 * Manages the custom game list.
 * @author Antropod
 *
 */
public class CustomGameListManager implements IManager, DocumentListener
{
	private JTable gameTable;
	private JScrollPane tableScrollPane;
	private JButton createGameButton, joinGameButton;
	private JTextField searchField;
	private JButton refreshListButton,backButton;

	private PracticeGameSearchResult[] gameList;

	private GameTableModel gameTableModel;

	@Override
	public void init(WindowManager wm)
	{

		View view = new View(WindowManager.CUSTOM_GAME_LIST_VIEW);

		gameTableModel = new GameTableModel(new Object[] { "", "Game Name", "Owner", "Slots", "Spectators", "Map" }, 0);
		;

		gameTable = new JTable();
		gameTable.setBackground(Color.black);
		gameTable.setForeground(Color.white);
		gameTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		gameTable.setCellSelectionEnabled(false);
		gameTable.setRowSelectionAllowed(true);
		gameTable.setColumnSelectionAllowed(false);
		gameTable.setModel(gameTableModel);

		gameTable.getTableHeader().getColumnModel().getColumn(0).setHeaderRenderer(new IconTableCellRenderer());
		gameTable.getTableHeader().getColumnModel().getColumn(0).setHeaderValue(new ImageIcon(ImageAssets.PRIVATE_GAME_ICON));
		gameTable.getTableHeader().getColumnModel().getColumn(0).setWidth(20);
		gameTable.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(20);

		// Make table of games sortable
		gameTable.setRowSorter(new TableRowSorter<>(gameTableModel));

		tableScrollPane = new JScrollPane(gameTable);
		tableScrollPane.setBounds(0, 110, JClientWindow.FRAME_WIDTH / 5 * 4, JClientWindow.FRAME_HEIGHT - 110-40);
		tableScrollPane.setBackground(Color.black);
		tableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		createGameButton = new JButton(LanguageDB.getLocalizedString("CREATE_GAME"));
		createGameButton.setBounds(tableScrollPane.getWidth(), JClientWindow.FRAME_HEIGHT - 60, JClientWindow.FRAME_WIDTH / 5, 30);
		createGameButton.setActionCommand("showCreateGameScreen");

		joinGameButton = new JButton(LanguageDB.getLocalizedString("JOIN_GAME"));
		joinGameButton.setBounds(tableScrollPane.getWidth(), JClientWindow.FRAME_HEIGHT - 30, JClientWindow.FRAME_WIDTH / 5, 30);
		joinGameButton.setActionCommand("joinCustomGame");

		searchField = new JTextField();
		searchField.setBounds(tableScrollPane.getWidth(), tableScrollPane.getY(), joinGameButton.getWidth(), 30);
		searchField.getDocument().addDocumentListener(this);
		
		

		view.addComponent(wm.getHomeScreenManager().getPlayButton());
		view.addComponent(wm.getHomeScreenManager().getIPIcon());
		view.addComponent(wm.getHomeScreenManager().getRPIcon());
		view.addComponent(wm.getHomeScreenManager().getIPLabel());
		view.addComponent(wm.getHomeScreenManager().getRPLabel());
		view.addComponent(wm.getHomeScreenManager().getSummonerIcon());
		view.addComponent(tableScrollPane);
		view.addComponent(createGameButton);
		view.addComponent(joinGameButton);
		view.addComponent(searchField);

		wm.addView(view);
	}

	public void listGames(PracticeGameSearchResult[] pgsr)
	{
		listGames(searchField.getText(), pgsr);
	}
	
	public void refreshList()
	{
		PracticeGameSearchResult[] pgsr=Globals.client.listAllPracticeGames();
		listGames(pgsr);
	}

	private void listGames(String filter, PracticeGameSearchResult[] pgsr)
	{
		gameTableModel.clear();
		gameList = pgsr;
		for (int i = 0; i < gameList.length; i++)
		{
			if (gameList[i].name.toLowerCase().contains(filter.toLowerCase()) || gameList[i].owner.summonerName.toLowerCase().contains(filter.toLowerCase()))
				gameTableModel.addRow(
						new String[] { gameList[i].privateGame ? "X" : "", gameList[i].name, gameList[i].owner.summonerName,
								(gameList[i].team1Count + gameList[i].team2Count) + "/" + gameList[i].maxNumPlayers,
								gameList[i].allowSpectators.equalsIgnoreCase("ALL") ? gameList[i].spectatorCount + "/" + Globals.loginPacket.clientSystemStates.spectatorSlotLimit : "0/0",
								GameMaps.getMap(gameList[i].gameMapId).displayName }, gameList[i].id, i);
		}
	}

	@Override
	public void registerEvents(WindowManager wm)
	{
		wm.registerEvent(createGameButton, new Delegate(this, "createGame"));
		wm.registerEvent(joinGameButton, new Delegate(this, "joinGame"));
	}

	public void createGame()
	{
		Globals.mainFrame.setCurrentView(WindowManager.CUSTOM_GAME_CREATE_VIEW);
	}

	public void joinGame()
	{
		int selectedGame = gameTable.convertRowIndexToModel(gameTable.getSelectedRow());
		System.out.println(selectedGame);
		String s = gameTableModel.getValueAt(selectedGame, 0).equals("X") ? JOptionPane.showInputDialog(Globals.mainFrame, LanguageDB.getLocalizedString("CG_PASSWORD_INPUT"),
				LanguageDB.getLocalizedString("ENTER_PASSWORD"), 0) : null;
		double gameId = gameTableModel.getGameId((String) gameTableModel.getValueAt(selectedGame, 1));
		Globals.client.joinGame(gameId, s);
	}

	@Override
	public void changedUpdate(DocumentEvent arg0)
	{
		listGames(searchField.getText(), gameList);
	}

	@Override
	public void insertUpdate(DocumentEvent arg0)
	{
		changedUpdate(arg0);
	}

	@Override
	public void removeUpdate(DocumentEvent arg0)
	{
		changedUpdate(arg0);
	}

}
