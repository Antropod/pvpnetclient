package net.antropod.client.managers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import com.riotgames.platform.game.GameDTO;

import net.antropod.client.Globals;
import net.antropod.client.chat.components.JChatListManager;
import net.antropod.client.components.ClientLayout;
import net.antropod.client.components.JClientWindow;
import net.antropod.client.components.JPanel;
import net.antropod.client.components.View;
import net.antropod.client.resources.Sound;
import net.antropod.util.Delegate;

/**
 * Manages a JClientWindow.
 * @author Antropod
 *
 */
public class WindowManager implements ActionListener
{
	/** The Login-View ID. (Unused right now) */
	public static final int LOGIN_VIEW=0;
	/** The Home-View ID. */
	public static final int HOME_VIEW=1;
	/** The Custom-Game-List-View ID. */
	public static final int CUSTOM_GAME_LIST_VIEW=2;
	/** The Custom-Game-Lobby-View ID. */
	public static final int CUSTOM_GAME_LOBBY_VIEW = 3;
	/** The Game-Creation-View ID. */
	public static final int CUSTOM_GAME_CREATE_VIEW = 4;
	/** The Champion-Selection-View ID. */
	public static final int CHAMPION_SELECTION_VIEW = 5;
	/** The Play-Screen-View ID. */
	public static final int PLAY_SCREEN_VIEW = 6;
	/** The Team-Lobby-View ID. */
	public static final int TEAM_LOBBY_VIEW = 7;
	
	/** The window this WindowManager manages */
	private JClientWindow managedWindow;
	/** The chat, that belongs to this window. */
	private JChatListManager chat;
	private JScrollPane chatScrollPane;
	
	/** Events that are registered for this WindowManager. */
	private HashMap<JComponent, Delegate> registeredEvents;
	/** The Game currently in progress. */
	private GameDTO currentGame;
	
	//Sub-managers----------------------------------------------------
	
	/** The home screen submanager */
	private HomeScreenManager homeScreenManager;
	/** The custom game list submanager */
	private CustomGameListManager customGameListManager;
	/** The custom game lobby submanager */
	private CustomGameLobbyManager customGameLobbyManager;
	/** The custom game creation submanager */
	private CustomGameCreationManager customGameCreationManager;
	/** The champion selection submanager */
	private ChampionSelectionManager championSelectionManager;
	/** The play screen submanager */
	private PlayScreenManager playScreenManager;
	/** The team lobby submanager */
	private TeamLobbyManager teamLobbyManager;
	
	//----------------------------------------------------------------
	/**
	 * The default constructor
	 * @param w The window, this WindowManager will manage.
	 */
	public WindowManager(JClientWindow w)
	{
		managedWindow=w;
		registeredEvents=new HashMap<>();
		currentGame=null;
		initManagers();
	}
	
	/**
	 * Initializes all submanagers.
	 */
	private void initManagers()
	{
		
		homeScreenManager=new HomeScreenManager();
		homeScreenManager.init(this);
		homeScreenManager.registerEvents(this);
		
		customGameListManager=new CustomGameListManager();
		customGameListManager.init(this);
		customGameListManager.registerEvents(this);
		
		customGameLobbyManager=new CustomGameLobbyManager();
		customGameLobbyManager.init(this);
		customGameLobbyManager.registerEvents(this);
		
		customGameCreationManager= new CustomGameCreationManager();
		customGameCreationManager.init(this);
		customGameCreationManager.registerEvents(this);
		
		championSelectionManager=new ChampionSelectionManager();
		championSelectionManager.init(this);
		championSelectionManager.registerEvents(this);
		
		playScreenManager=new PlayScreenManager();
		playScreenManager.init(this);
		playScreenManager.registerEvents(this);
		
		teamLobbyManager=new TeamLobbyManager();
		teamLobbyManager.init(this);
		teamLobbyManager.registerEvents(this);
	}
	/**
	 * Initializes the chat component. 
	 * NOTE: This method should only be called after the pvpnet connection has been established.
	 */
	public void initChatComponent()
	{

		
		chat=new JChatListManager(Globals.pvpnet);
		int w=JClientWindow.FRAME_WIDTH-Globals.mainFrame.getInsets().left-Globals.mainFrame.getInsets().right;
		int h=JClientWindow.FRAME_HEIGHT-Globals.mainFrame.getInsets().top-Globals.mainFrame.getInsets().bottom;
		final JPanel j=new JPanel(new ClientLayout(w,h));
		j.setOpaque(false);
		chatScrollPane=new JScrollPane(chat);
		chatScrollPane.setBounds(w/4*3,0,w/4,h);
		j.add(chatScrollPane);
		chatScrollPane.setVisible(false);
		managedWindow.setGlassPane(j);
		managedWindow.getGlassPane().setVisible(true);
		j.addComponentListener(new ComponentListener(){

			@Override
			public void componentHidden(ComponentEvent e)
			{
			}

			@Override
			public void componentMoved(ComponentEvent e)
			{
			}

			@Override
			public void componentResized(ComponentEvent e)
			{
				int w=chatScrollPane.getWidth();
				chatScrollPane.setBounds(j.getWidth()-w,0,w,j.getHeight());
				chatScrollPane.repaint();
				
			}

			@Override
			public void componentShown(ComponentEvent e)
			{
				
			}
			
		});
		
		managedWindow.getGlassPane().repaint();
	}
	
	/**
	 * Adds a view to the managed window
	 * @param view The view 
	 */
	public void addView(View view)
	{
		managedWindow.addView(view);
	}
	/**
	 * Registers an event.
	 * @param j The component that is associated with the event
	 * @param d A delegate to a method that will be called when the event occurs.
	 */
	public void registerEvent(JComponent j, Delegate d)
	{
		if(j==null || d==null)
			throw new NullPointerException();
		if(j instanceof JButton)
			((JButton) j).addActionListener(this);
		else if(j instanceof JTextField)
			((JTextField)j).addActionListener(this);
		registeredEvents.put(j,d);
	}
	/**
	 * Removes an event from this Manager
	 * @param j THe Component, that is associated with the event
	 */
	public void removeEvent(JComponent j)
	{
		if(j instanceof JButton)
			((JButton) j).removeActionListener(this);
		else if(j instanceof JTextField)
			((JTextField)j).removeActionListener(this);
		registeredEvents.remove(j);
	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		registeredEvents.get(arg0.getSource()).invoke();
	}
	/**
	 * 
	 * @return The window this manager manages.
	 */
	public JClientWindow getManagedWindow()
	{
		return managedWindow;
	}
	
	/**
	 * 
	 * @return The home screen submanager.
	 */
	public HomeScreenManager getHomeScreenManager()
	{
		return homeScreenManager;
	}

	/**
	 * 
	 * @return The custom game list submanager.
	 */
	public CustomGameListManager getCustomGameListManager()
	{
		return customGameListManager;
	}

	/**
	 * 
	 * @return The custom game lobby submanager.
	 */
	public CustomGameLobbyManager getCustomGameLobbyManager()
	{
		return customGameLobbyManager;
	}

	/**
	 * 
	 * @return The custom game creation submanager.
	 */
	public CustomGameCreationManager getCustomGameCreationManager()
	{
		return customGameCreationManager;
	}

	/**
	 * 
	 * @return The champion selection submanager.
	 */
	public ChampionSelectionManager getChampionSelectionManager()
	{
		return championSelectionManager;
	}

	/**
	 * 
	 * @return The play screen submanager.
	 */
	public PlayScreenManager getPlayScreenManager()
	{
		return playScreenManager;
	}

	/**
	 * 
	 * @return The team lobby submanager.
	 */
	public TeamLobbyManager getTeamLobbyManager()
	{
		return teamLobbyManager;
	}
	
	/**
	 * 
	 * @return The game currently in progress or null if no game is in progress.
	 */
	public GameDTO getCurrentGame()
	{
		return currentGame;
	}
	/**
	 * Sets the current game and starts champion selection or shows the custom game lobby, depending on the state and type of the GameDTO.
	 * @param game The game or null if a game ended.
	 */
	public void setCurrentGame(GameDTO game)
	{
		if(game==null)
		{
			currentGame=null;
			Globals.mainFrame.setCurrentView(HOME_VIEW);
			if(currentGame!=null&&currentGame.gameState.contains(GameDTO.STATE_CHAMP_SELECT))
			{
				championSelectionManager.endChampionSelection();
			}
		}
		else if(currentGame==null&&game!=null)
		{
			if(game.gameType.equals(GameDTO.TYPE_CUSTOM_GAME))
			{
				Globals.mainFrame.setCurrentView(CUSTOM_GAME_LOBBY_VIEW);
				customGameLobbyManager.setGame(game);
			}
			else
			{
				if(game.gameState.equals(GameDTO.STATE_JOINING_CHAMP_SELECT))
				{
					playScreenManager.getPoppedGameDialog().showDialog();
					playScreenManager.getPoppedGameDialog().setStatusOfParticipants(game.statusOfParticipants);
				}
			}
		}
		else if(currentGame!=null)
		{
			if(game.gameType.equals(GameDTO.TYPE_CUSTOM_GAME))
			{
				if(game.gameState.equals(GameDTO.STATE_TEAM_SELECT))
				{
					if(!currentGame.gameState.equals(GameDTO.STATE_TEAM_SELECT))
					{
						Globals.mainFrame.setCurrentView(CUSTOM_GAME_LOBBY_VIEW);
						if(currentGame.gameState.contains(GameDTO.STATE_CHAMP_SELECT))
						{
							championSelectionManager.endChampionSelection();
							Sound.stopMusic();
						}
					}
					customGameLobbyManager.setGame(game);
				}
				else if(game.gameState.equals(GameDTO.STATE_CHAMP_SELECT))
				{
					if(currentGame.gameState.equals(GameDTO.STATE_TEAM_SELECT))
					{
						
						championSelectionManager.startChampionSelection(game);
					}
					else if(currentGame.gameState.equals(GameDTO.STATE_PRE_CHAMP_SELECT))
					{
						championSelectionManager.setChampionsForPick(Globals.availableChampions);
					}
					championSelectionManager.setGame(game);
				}
				else if(game.gameState.equals(GameDTO.STATE_POST_CHAMP_SELECT))
				{
					if(currentGame.gameState.equals(GameDTO.STATE_TEAM_SELECT))
					{
						championSelectionManager.startChampionSelection(game);
					}
					championSelectionManager.setGame(game);
				}
				else if(game.gameState.equals(GameDTO.STATE_PRE_CHAMP_SELECT))
				{
					if(currentGame.gameState.equals(GameDTO.STATE_TEAM_SELECT))
					{
						championSelectionManager.setChampionsForBan(Globals.client.getChampionsForBan());
						championSelectionManager.startChampionSelection(game);
						
					}
					championSelectionManager.setGame(game);
				}
			}
			else
			{
				if(game.gameState.equals(GameDTO.STATE_TERMINATED))
				{
					playScreenManager.getPoppedGameDialog().hideDialog();
					game=null;
				}
				else if(game.gameState.equals(GameDTO.STATE_JOINING_CHAMP_SELECT))
				{
					playScreenManager.getPoppedGameDialog().setStatusOfParticipants(game.statusOfParticipants);
				}
				else if(game.gameState.equals(GameDTO.STATE_CHAMP_SELECT))
				{
					if(currentGame.gameState.equals(GameDTO.STATE_JOINING_CHAMP_SELECT))
					{
						playScreenManager.getPoppedGameDialog().hideDialog();
						championSelectionManager.startChampionSelection(game);
					}
					else if(currentGame.gameState.equals(GameDTO.STATE_PRE_CHAMP_SELECT))
					{
						championSelectionManager.setChampionsForPick(Globals.availableChampions);
					}
					championSelectionManager.setGame(game);
				}
				else if(game.gameState.equals(GameDTO.STATE_POST_CHAMP_SELECT))
				{
					if(currentGame.gameState.equals(GameDTO.STATE_JOINING_CHAMP_SELECT))
					{
						playScreenManager.getPoppedGameDialog().hideDialog();
						championSelectionManager.startChampionSelection(game);
					}
					championSelectionManager.setGame(game);
				}
				else if(game.gameState.equals(GameDTO.STATE_PRE_CHAMP_SELECT))
				{
					if(currentGame.gameState.equals(GameDTO.STATE_JOINING_CHAMP_SELECT))
					{
						playScreenManager.getPoppedGameDialog().hideDialog();
						championSelectionManager.setChampionsForBan(Globals.client.getChampionsForBan());
						championSelectionManager.startChampionSelection(game);
						
					}
					championSelectionManager.setGame(game);
				}
			}
		}
		currentGame=game;
	}

	public void changeChatVisibility()
	{
		if(chatScrollPane!=null)
			chatScrollPane.setVisible(!chatScrollPane.isVisible());
	}
}
