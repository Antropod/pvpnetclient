package net.antropod.client.managers;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;

import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.muc.MultiUserChat;

import net.antropod.client.Globals;
import net.antropod.client.chat.ChatRoomType;
import net.antropod.client.chat.components.JChampSelectChat;
import net.antropod.client.components.JChampSpells;
import net.antropod.client.components.JChampionPanel;
import net.antropod.client.components.JClientWindow;
import net.antropod.client.components.JSpellPopup;
import net.antropod.client.components.View;
import net.antropod.client.resources.ImageAssets;
import net.antropod.client.resources.LanguageDB;
import net.antropod.client.resources.Sound;
import net.antropod.util.Delegate;
import net.antropod.util.Timer;
import net.antropod.util.TimerCallback;

import com.riotgames.platform.catalog.champion.ChampionDTO;
import com.riotgames.platform.game.ChampionBanInfoDTO;
import com.riotgames.platform.game.GameDTO;
import com.riotgames.platform.game.KnownParticipant;
import com.riotgames.platform.game.Participant;
import com.riotgames.platform.game.PlayerChampionSelectionDTO;
import com.riotgames.platform.summoner.masterybook.MasteryBookDTO;
import com.riotgames.platform.summoner.masterybook.MasteryBookPageDTO;
import com.riotgames.platform.summoner.spellbook.SpellBookPageDTO;

/**
 * Manages champion selection and everything that belongs to it.
 * 
 * @author Antropod
 * 
 */
public class ChampionSelectionManager implements IManager, TimerCallback, ItemListener
{
	private JTabbedPane tabbedPane;
	private JChampionPanel championPoolPanel;

	private JButton quitGameButton;
	private JProgressBar phaseProgressBar;
	private JButton spell1Button;
	private JButton spell2Button;
	private JButton lockInButton;
	private JSpellPopup spell1Popup;
	private JSpellPopup spell2Popup;
	private JChampSelectChat chatPanel;
	private ArrayList<JChampSpells> teamOnePanels;
	private ArrayList<JChampSpells> teamTwoPanels;
	private JComboBox<SpellBookPageDTO> runesBox;
	private JComboBox<MasteryBookPageDTO> masteriesBox;
	private Color clr_runes;
	private Color clr_masteries;

	private Timer counter;
	private int timeLeft = 0;
	private int playerTeam, playerPosition;
	private int spell1id, spell2id;

	private DefaultComboBoxModel<SpellBookPageDTO> runesModel;
	private DefaultComboBoxModel<MasteryBookPageDTO> masteriesModel;

	@Override
	public void init(WindowManager wm)
	{
		View view = new View(WindowManager.CHAMPION_SELECTION_VIEW);

		counter = new Timer(1000, this);

		teamOnePanels = new ArrayList<>();
		teamTwoPanels = new ArrayList<>();
		for (int i = 0; i < 5; i++)
		{
			JChampSpells jcs1 = new JChampSpells();
			jcs1.setLocation(10, 10 + i * (JClientWindow.FRAME_HEIGHT / 8));
			view.addComponent(jcs1);

			JChampSpells jcs2 = new JChampSpells();
			jcs2.setLocation(JClientWindow.FRAME_WIDTH - jcs2.getWidth() - 10, 10 + i * (JClientWindow.FRAME_HEIGHT / 8));
			;
			view.addComponent(jcs2);

			teamOnePanels.add(jcs1);
			teamTwoPanels.add(jcs2);
			jcs1.setVisible(false);
			jcs2.setVisible(false);

		}

		championPoolPanel = new JChampionPanel();
		championPoolPanel.setName("Champions");

		tabbedPane = new JTabbedPane();
		tabbedPane.setBounds(JClientWindow.FRAME_WIDTH / 2 - (64 * 10 + 32) / 2, 80, 64 * 10 + 32, 310);
		tabbedPane.add(championPoolPanel);

		phaseProgressBar = new JProgressBar();
		phaseProgressBar.setBounds(JClientWindow.FRAME_WIDTH / 2 - 50, 25, 100, 30);

		spell1Button = new JButton();
		spell1Button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		spell1Button.setBounds(tabbedPane.getX() + tabbedPane.getWidth() / 2 - 64, tabbedPane.getY() + tabbedPane.getHeight(), 64, 64);

		spell2Button = new JButton();
		spell2Button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		spell2Button.setBounds(tabbedPane.getX() + tabbedPane.getWidth() / 2, tabbedPane.getY() + tabbedPane.getHeight(), 64, 64);

		spell1Popup = new JSpellPopup(false);
		spell2Popup = new JSpellPopup(true);

		lockInButton = new JButton(LanguageDB.getLocalizedString("LOCK_IN"));
		lockInButton.setBounds(spell2Button.getX() + spell2Button.getWidth(), spell2Button.getY(), tabbedPane.getWidth() / 2 - spell2Button.getWidth(), spell2Button.getHeight());
		lockInButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		runesModel = new DefaultComboBoxModel<SpellBookPageDTO>();

		runesBox = new JComboBox<SpellBookPageDTO>();
		runesBox.setFont(runesBox.getFont().deriveFont(Font.BOLD));
		runesBox.setModel(runesModel);
		runesBox.addItemListener(this);
		runesBox.setBounds(tabbedPane.getX(), tabbedPane.getY() + tabbedPane.getHeight(), lockInButton.getWidth(), lockInButton.getHeight() / 2);
		clr_runes = runesBox.getForeground();

		masteriesModel = new DefaultComboBoxModel<MasteryBookPageDTO>();

		masteriesBox = new JComboBox<MasteryBookPageDTO>();
		masteriesBox.setFont(masteriesBox.getFont().deriveFont(Font.BOLD));
		masteriesBox.setModel(masteriesModel);
		masteriesBox.addItemListener(this);
		masteriesBox.setBounds(tabbedPane.getX(), tabbedPane.getY() + tabbedPane.getHeight() + lockInButton.getHeight() / 2, lockInButton.getWidth(), lockInButton.getHeight() / 2);
		clr_masteries = masteriesBox.getForeground();

		chatPanel = new JChampSelectChat();
		chatPanel.setLocation(tabbedPane.getX(), spell1Button.getY() + spell1Button.getHeight() + 50);

		view.addComponent(tabbedPane);
		view.addComponent(phaseProgressBar);
		view.addComponent(spell1Button);
		view.addComponent(spell2Button);
		view.addComponent(lockInButton);
		view.addComponent(runesBox);
		view.addComponent(masteriesBox);
		view.addComponent(chatPanel);

		wm.addView(view);

		// To be removed, only for testing purposes
		// c_quitGame = new JButton(LanguageDB.getLocalizedString("QUIT"));
		// c_quitGame.setBounds(100, 180 + JSummoner.STD_HEIGHT * 5 + 23,
		// JSummoner.STD_WIDTH, 50);
		// c_quitGame.setActionCommand("quitCustomGame");
	}

	@Override
	public void registerEvents(WindowManager wm)
	{
		wm.registerEvent(spell1Button, new Delegate(this, "spell1Menu"));
		wm.registerEvent(spell2Button, new Delegate(this, "spell2Menu"));
		wm.registerEvent(lockInButton, new Delegate(this, "lockIn"));
	}

	public void startChampionSelection(GameDTO game)
	{
		if (game.gameState.equals(GameDTO.STATE_CHAMP_SELECT))
		{
			setTimerTime(Globals.gtcProvider.getChampSelectionTime(game.gameTypeConfigId));
		} else if (game.gameState.equals(GameDTO.STATE_PRE_CHAMP_SELECT))
		{
			setTimerTime(Globals.gtcProvider.getPreChampSelectionTime(game.gameTypeConfigId));
		} else if (game.gameState.equals(GameDTO.STATE_POST_CHAMP_SELECT))
		{
			setTimerTime(Globals.gtcProvider.getPostChampSelectionTime(game.gameTypeConfigId));
		}
		Sound.CHAMP_SLCT_DRAFT.play();
		Globals.client.setClientReceivedMassage_ChampSelect(game.id);
		setGameMode(game.gameMode);

		Globals.pvpnet.joinMultiUserChat(game.roomName, game.roomPassword, new Delegate(this, "chatResult"));

		for (int i = 0; i < game.teamOne.length; i++)
		{
			if (game.teamOne[i] instanceof KnownParticipant)
			{
				teamOnePanels.get(i).setSummonerName(((KnownParticipant) game.teamOne[i]).summonerName);
				if (((KnownParticipant) game.teamOne[i]).summonerInternalName.equals(Globals.loginPacket.allSummonerData.summoner.internalName))
				{
					playerTeam = 1;
					playerPosition = i;
				}
			} else
			{
				teamOnePanels.get(i).setSummonerName("Summoner " + (i + 1));
			}
		}
		for (int i = 0; i < game.teamTwo.length; i++)
		{
			if (game.teamTwo[i] instanceof KnownParticipant)
			{
				teamTwoPanels.get(i).setSummonerName(((KnownParticipant) game.teamTwo[i]).summonerName);
				if (((KnownParticipant) game.teamTwo[i]).summonerInternalName.equals(Globals.loginPacket.allSummonerData.summoner.internalName))
				{
					playerTeam = 2;
					playerPosition = i;
				}
			} else
			{
				teamTwoPanels.get(i).setSummonerName("Summoner " + (i + 1));
			}
		}
		Globals.mainFrame.setCurrentView(WindowManager.CHAMPION_SELECTION_VIEW);
		Globals.winManager.getHomeScreenManager().getQueuePanel().setVisible2(false);
	}

	public void endChampionSelection()
	{
		chatPanel.leaveChatRoom();
	}

	public void chatResult(MultiUserChat muc)
	{
		chatPanel.setChat(muc);
	}

	public void spell1Menu()
	{
		spell1Popup.show(spell1Button, 0, 0);
	}

	public void spell2Menu()
	{
		spell2Popup.show(spell2Button, 0, 0);
	}

	public void lockIn()
	{
		if ((playerTeam == 1 ? Globals.winManager.getCurrentGame().teamOne : Globals.winManager.getCurrentGame().teamTwo)[playerPosition].pickMode == 1)
			Globals.client.championSelectCompleted();
		Sound.CHAMP_SLCT_LOCK_IN.play();
	}

	public void updateMasteriesAndRunePages()
	{
		masteriesModel.removeAllElements();
		runesModel.removeAllElements();
		for (MasteryBookPageDTO mbp : Globals.loginPacket.allSummonerData.masteryBook.bookPages)
		{
			masteriesModel.addElement(mbp);
			if (mbp.current)
				masteriesModel.setSelectedItem(mbp);
		}
		for (SpellBookPageDTO sbp : Globals.loginPacket.allSummonerData.spellBook.bookPages)
		{
			runesModel.addElement(sbp);
			if (sbp.current)
				runesModel.setSelectedItem(sbp);
		}
	}

	public void itemStateChanged(ItemEvent e)
	{
		if (e.getSource() == runesBox)
		{
			runesBox.setForeground(Color.red);
			Globals.client.async_selectDefaultSpellBookPage(new Delegate(this, "runesSaved"), (SpellBookPageDTO) e.getItem());

		} else if (e.getSource() == masteriesBox)
		{
			masteriesBox.setForeground(Color.red);
			for (int i = 0; i < Globals.loginPacket.allSummonerData.masteryBook.bookPages.length; i++)
			{
				Globals.loginPacket.allSummonerData.masteryBook.bookPages[i].current = i == masteriesBox.getSelectedIndex();
			}
			Globals.client.async_saveMasteryBook(new Delegate(this, "masteriesSaved"), Globals.loginPacket.allSummonerData.masteryBook);
		}
	}

	public void runesSaved(SpellBookPageDTO sbp)
	{

		for (int i = 0; i < Globals.loginPacket.allSummonerData.spellBook.bookPages.length; i++)
		{
			if (Globals.loginPacket.allSummonerData.spellBook.bookPages[i].pageId == sbp.pageId)
				Globals.loginPacket.allSummonerData.spellBook.bookPages[i] = sbp;
			else
				Globals.loginPacket.allSummonerData.spellBook.bookPages[i].current = false;
		}
		runesBox.setForeground(clr_runes);
		Arrays.sort(Globals.loginPacket.allSummonerData.spellBook.bookPages);
	}

	public void masteriesSaved(MasteryBookDTO mb)
	{
		Globals.loginPacket.allSummonerData.masteryBook = mb;
		masteriesBox.setForeground(clr_masteries);
		Arrays.sort(Globals.loginPacket.allSummonerData.masteryBook.bookPages);
	}

	public void timePassed()
	{
		phaseProgressBar.setString("" + timeLeft);
		phaseProgressBar.setValue(--timeLeft);
		if(timeLeft==10)
		{
			Sound.CHAMP_SLCT_CD10SEC.play();
		}
		if (timeLeft <= 0)
		{
			counter.pauseTimer();
			phaseProgressBar.setStringPainted(false);
		}
	}

	public void setChampionsForBan(ChampionBanInfoDTO[] cb)
	{
		championPoolPanel.setChampsForBan(cb);
	}

	public void setChampionsForPick(ChampionDTO[] champs)
	{
		championPoolPanel.setChampsForPick(champs);
	}

	public void setPhase(int phase)
	{
		championPoolPanel.setPhase(phase);
	}

	public void setGameMode(String string)
	{
		spell1Popup.setGameMode(string);
		spell2Popup.setGameMode(string);
	}

	public void setTimerTime(int sec)
	{
		timeLeft = sec;
		phaseProgressBar.setMaximum(sec);
		phaseProgressBar.setStringPainted(true);
		counter.startTimer();
	}

	public int getPlayerTeam()
	{
		return playerTeam;
	}

	public int getPlayerPosition()
	{
		return playerPosition;
	}

	public int getSpell1ID()
	{
		return spell1id;
	}

	public int getSpell2ID()
	{
		return spell2id;
	}

	public void setGame(GameDTO to)
	{
		Participant[] loc_teamOne = to.teamOne;
		Participant[] loc_teamTwo = to.teamTwo;

		if (to.gameState.equals(GameDTO.STATE_CHAMP_SELECT))
		{
			if (Globals.winManager.getCurrentGame().gameState.equals(GameDTO.STATE_CHAMP_SELECT) && to.pickTurn != Globals.winManager.getCurrentGame().pickTurn)
			{
				setTimerTime(Globals.gtcProvider.getChampSelectionTime(to.gameTypeConfigId));
			}
		} else if (to.gameState.equals(GameDTO.STATE_POST_CHAMP_SELECT))
		{
			if (!Globals.winManager.getCurrentGame().gameState.equals(GameDTO.STATE_POST_CHAMP_SELECT))
			{
				setTimerTime(Globals.gtcProvider.getPostChampSelectionTime(to.gameTypeConfigId));
			}
		} else if (to.gameState.equals(GameDTO.STATE_PRE_CHAMP_SELECT))
		{
			if (!Globals.winManager.getCurrentGame().gameState.equals(GameDTO.STATE_PRE_CHAMP_SELECT) || to.pickTurn != Globals.winManager.getCurrentGame().pickTurn)
			{
				setTimerTime(Globals.gtcProvider.getPreChampSelectionTime(to.gameTypeConfigId));
			}
		}

		for (int i = 0; i < 5; i++)
		{
			if (to.teamOne.length > i)
			{
				teamOnePanels.get(i).setVisible(true);
			} else
			{
				teamOnePanels.get(i).setVisible(false);
			}
			if (to.teamTwo.length > i)
			{
				teamTwoPanels.get(i).setVisible(true);
			} else
			{
				teamTwoPanels.get(i).setVisible(false);
			}
		}

		for (int i = 0; i < loc_teamOne.length; i++)
		{
			if (loc_teamOne[i] instanceof KnownParticipant && loc_teamOne[i].pickMode == 1
					&& ((KnownParticipant) loc_teamOne[i]).summonerInternalName.equals(Globals.loginPacket.allSummonerData.summoner.internalName))
			{
				teamOnePanels.get(i).playAnim(true);
				teamOnePanels.get(i).setPhase(to.gameStateString.equals("CHAMP_SELECT") ? "Picking!" : "Banning!");
			} else if (loc_teamOne[i].pickMode == 1)
			{
				teamOnePanels.get(i).playAnim(false);
				teamOnePanels.get(i).setPhase(to.gameStateString.equals("CHAMP_SELECT") ? "Picking!" : "Banning!");
			} else
			{
				teamOnePanels.get(i).stopAnim();
				teamOnePanels.get(i).setPhase("");
			}
		}

		for (int i = 0; i < loc_teamTwo.length; i++)
		{
			if (loc_teamTwo[i] instanceof KnownParticipant && loc_teamTwo[i].pickMode == 1
					&& ((KnownParticipant) loc_teamTwo[i]).summonerInternalName.equals(Globals.loginPacket.allSummonerData.summoner.internalName))
			{
				teamTwoPanels.get(i).playAnim(true);
				teamTwoPanels.get(i).setPhase(to.gameStateString.equals("CHAMP_SELECT") ? "Picking!" : "Banning!");
			} else if (loc_teamTwo[i].pickMode == 1)
			{
				teamTwoPanels.get(i).playAnim(false);
				teamTwoPanels.get(i).setPhase(to.gameStateString.equals("CHAMP_SELECT") ? "Picking!" : "Banning!");
			} else
			{
				teamTwoPanels.get(i).stopAnim();
				teamTwoPanels.get(i).setPhase("");
			}
		}

		PlayerChampionSelectionDTO[] loc_champs = to.playerChampionSelections;

		for (int i = 0; i < loc_champs.length; i++)
		{
			PlayerChampionSelectionDTO champ = loc_champs[i];

			if (champ.summonerInternalName.equals(Globals.loginPacket.allSummonerData.summoner.internalName))
			{
				spell1Button.setIcon(new ImageIcon(ImageAssets.getSummonerSpellIcon(champ.spell1Id, 60)));
				spell2Button.setIcon(new ImageIcon(ImageAssets.getSummonerSpellIcon(champ.spell2Id, 60)));
				spell1id = champ.spell1Id;
				spell2id = champ.spell2Id;
			}

			for (int j = 0; j < loc_teamOne.length; j++)
			{
				if (loc_teamOne[i] instanceof KnownParticipant &&((KnownParticipant)loc_teamOne[j]).summonerInternalName.equals(champ.summonerInternalName))
				{
					teamOnePanels.get(j).setChamp(champ.championId);
					teamOnePanels.get(j).setSpells(champ.spell1Id, champ.spell2Id);
				}
			}
			for (int j = 0; j < loc_teamTwo.length; j++)
			{
				if (loc_teamTwo[i] instanceof KnownParticipant &&((KnownParticipant)loc_teamTwo[j]).summonerInternalName.equals(champ.summonerInternalName))
				{
					teamTwoPanels.get(j).setChamp(champ.championId);
					teamTwoPanels.get(j).setSpells(champ.spell1Id, champ.spell2Id);
				}
			}
		}
		System.out.println("Team: " + playerTeam + "; " + "Position: " + playerPosition);

		if ((playerTeam == 1 ? loc_teamOne : loc_teamTwo)[playerPosition].pickMode == 1
				&& (!((playerTeam == 1 ? Globals.winManager.getCurrentGame().teamOne : Globals.winManager.getCurrentGame().teamTwo)[playerPosition].pickMode == 1) || !Globals.winManager
						.getCurrentGame().gameState.contains("CHAMP_SELECT")))
		{
			Sound.CHAMP_SLCT_YOUR_TURN.play();
			championPoolPanel.setEnabled(true);
		}
		if ((playerTeam == 1 ? loc_teamOne : loc_teamTwo)[playerPosition].pickMode == 0
				&& (!((playerTeam == 1 ? Globals.winManager.getCurrentGame().teamOne : Globals.winManager.getCurrentGame().teamTwo)[playerPosition].pickMode == 0) || !Globals.winManager
						.getCurrentGame().gameState.contains("CHAMP_SELECT")))
		{
			championPoolPanel.setEnabled(false);
		}

		if ((playerTeam == 1 ? loc_teamOne : loc_teamTwo)[playerPosition].pickMode == 1 && to.gameState.equals(GameDTO.STATE_CHAMP_SELECT))
		{
			lockInButton.setEnabled(true);
		} else
		{
			lockInButton.setEnabled(false);
		}

		if (to.bannedChampions.length > Globals.winManager.getCurrentGame().bannedChampions.length)
		{
			int[] champIds = new int[to.bannedChampions.length];
			for (int i = 0; i < to.bannedChampions.length; i++)
			{
				champIds[i] = to.bannedChampions[i].championId;
			}
			championPoolPanel.bannedChamps(champIds);
		}
		championPoolPanel.clearPickedChamps();
		for (PlayerChampionSelectionDTO pcs : to.playerChampionSelections)
		{
			championPoolPanel.addPickedChamp(pcs.championId);
		}
	}

}
