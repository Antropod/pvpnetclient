package net.antropod.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import net.antropod.client.resources.LanguageDB;

/**
 * A config file with convenient methods for the Client
 * 
 * @author Antropod
 *
 */
public class ClientProperties extends Properties
{
	/**
	 * Initializes and reads the properties from "cfg.properties"
	 */
	public ClientProperties()
	{
		try
		{
			File f = new File("cfg.properties");
			FileInputStream fis = new FileInputStream(f);
			load(fis);
			fis.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private String chooseLoLLocation()
	{
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File(System.getenv("ProgramFiles(x86)")));
		fc.setFileFilter(new FileFilter(){

			@Override
			public boolean accept(File arg0)
			{
				if(arg0.isDirectory())
					return true;
				else if(arg0.getName().endsWith("lol.launcher.exe")||arg0.getName().endsWith("lol.launcher.admin.exe"))
					return true;
				return false;
			}

			@Override
			public String getDescription()
			{
				return "lol.launcher.exe";
			}
			
		});
		//fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fc.setDialogTitle(LanguageDB.getLocalizedString("CHOOSE_DIR"));
		//fc.setAcceptAllFileFilterUsed(false);

		File f = null;
		while (f==null||!f.exists())
		{
			JOptionPane.showMessageDialog(null, LanguageDB.getLocalizedString("SPECIFY_LOL_LOCATION"),LanguageDB.getLocalizedString("CHOOSE_DIR"),JOptionPane.INFORMATION_MESSAGE);
			if (fc.showOpenDialog(Globals.mainFrame) == JFileChooser.APPROVE_OPTION)
			{
				f=new File(fc.getSelectedFile().getParentFile(),"lol.launcher.exe");
				System.out.println(f.getAbsolutePath().replace('\\', '/'));
			} else
			{
				System.exit(0);
			}
		}
		return fc.getSelectedFile().getParentFile().getAbsolutePath().replace('\\', '/')+"/";
	}
	
	/**
	 * Returns the game's location. If the location hasn't been specified yet, it will
	 * let the user specify the location and saves it.
	 * 
	 * @return The location, where lol.launcher.exe resides.
	 */
	public String getLoLLocation()
	{
		if (getProperty("gameLocation") == null)
		{
			setProperty("gameLocation",chooseLoLLocation());
		} else if (!(new File(getProperty("gameLocation").replace('\\', '/') + "lol.launcher.exe").exists()))
		{
			setProperty("gameLocation",chooseLoLLocation());
		}
		save();
		return getProperty("gameLocation").replace('\\', '/');
	}
	/**
	 * Tells you if the user has disabled the music.
	 * 
	 * @return If music is enabled or not.
	 */
	public boolean isMusicEnabled()
	{
		if (getProperty("musicEnabled") == null)
		{
			setProperty("musicEnabled", "true");
		}
		save();
		return Boolean.parseBoolean(getProperty("musicEnabled"));
	}
	/**
	 * Reads the last saved summoner name.
	 * @return The saved summoner name or an empty String if no summoner name has been set.
	 */
	public String getSummonerName()
	{
		return getProperty("summonerName","");
	}
	
	/**
	 * Saves the summoner name, that can be loaded for the login screen.
	 * @param name the summoner name
	 */
	public void setSummonerName(String name)
	{
		setProperty("summonerName", name);
		save();
	}
	
	/**
	 * Saves the properties to "cfg.properties" in the current directory.
	 */
	public void save()
	{
		try
		{

			File f = new File("cfg.properties");
			FileOutputStream fos = new FileOutputStream(f);
			super.store(fos, "Dem Config for da mighty NGLC");
			fos.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
