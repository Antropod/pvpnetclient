package net.antropod.logging;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import net.antropod.client.Globals;
import org.jivesoftware.smack.SmackConfiguration;

/**
 * Add SimpleLogger.getInstance().log(Exception ex) to a catch block to log
 * the exception to LOG_PATH
 * 
 * @author ArcadeStorm
 */
public class SimpleLogger {

    private static SimpleLogger instance;
    private static final String LOG_PATH = "test.log";
    
    private SimpleLogger() {
        
    }
    
    public static SimpleLogger getInstance() {
        if(instance == null) {
            instance = new SimpleLogger();
        }
        return instance;
    }
    
    /**
     * Appends the stacktrace and additional info of the given Exception to
     * the file at LOG_PATH
     * 
     * @param ex the exception to log
     */
    public static void log(Exception ex) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(LOG_PATH, true))) {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            bw.write("-------- Error Report --------\n\n");
            // Error date and time
            bw.write("Generated: " + df.format(new Date()) + "\n");
            // Client build info
            bw.write("Client: " + Globals.CLIENT_VERSION + " - " + Globals.build + "\n");
            // OS platform info
            bw.write("Operating System: " + System.getProperty("os.name") + " (" + System.getProperty("os.arch") + ") " + System.getProperty("os.version") + "\n");
            // Java platform info
            bw.write("Java: " + System.getProperty("java.version") + ", " + System.getProperty("java.vendor") + "\n");
            // Virtual machine info
            bw.write("VM: " + System.getProperty("java.vm.name") + " " + System.getProperty("java.vm.version") + ", " + System.getProperty("java.vm.vendor") + "\n");
            
            // API info
            bw.write("Smack: " + SmackConfiguration.getVersion() + "\n\n");
            
            bw.write("Message: " + ex.getMessage() + "\n");
            bw.write(ex.toString() + "\n");
            
            StackTraceElement[] st = ex.getStackTrace();
            for(StackTraceElement ste : st) {
                bw.write("\tat " + ste.getClassName() + ste.getMethodName() + "(" + ste.getFileName() + ":" + ste.getLineNumber() + ")\n");
            }
            bw.write("\n");
            bw.flush();
        } catch (IOException ioex) {
            System.err.println("Can't write to log file. Nothing logged...");
        }
    }
}