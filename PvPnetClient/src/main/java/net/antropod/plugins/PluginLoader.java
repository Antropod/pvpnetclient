package net.antropod.plugins;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;

import javax.swing.JOptionPane;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import net.antropod.client.Main;

public class PluginLoader {
	//setting the plugin path
	public static String pluginPath = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString().substring(1) + "plugins";
	public static ArrayList<URL> plugins;
	public static ArrayList<Manifest> manifests;
	private static ArrayList<String> classNames;
	public static PluginDB db;
	@SuppressWarnings("resource")
	public PluginLoader() {
		System.out.println("Setting Plugin Path: "+ pluginPath);
		db = new PluginDB();
		plugins = new ArrayList<URL>();
		manifests = new ArrayList<Manifest>();
		classNames = new ArrayList<String>();
		File a = new File(pluginPath);
		
		//creating the plugin folder if it doesn't exist
		if (!a.isDirectory())
			a.mkdirs();
		//getting all jar files from the plugin folder and grabbing their manifest
		File[] jars = a.listFiles();
		for (File plugin : jars) {
			try {
				manifests.add(new JarFile(plugin).getManifest());
				plugins.add(plugin.toURI().toURL());
			} catch (IOException e) {
				//Not A Plugin
			}
		}
		
		//debug output
		if (plugins.size() == 1)
			System.out.println("Found " + plugins.size() + " Plugin");
		else
			System.out.println("Found " + plugins.size() + " Plugins");
		//getting the main class from all plugin manifests
		for (Manifest mf : manifests)
			classNames.add(mf.getMainAttributes().getValue("Main-Class"));
		
		//setting up permissions (this needs to ask the user if he wants to allow it)
		for (URL plugin : plugins) {
			if (!db.isSaved(getName(plugin), getVersion(plugin)))
				db.savePlugin(getName(plugin), getVersion(plugin), getPermissions(plugin));
		}
		
		//running the class loader
		for (int i = 0;i < plugins.size(); i++) {
			try {
				URL[] urls = new URL[]{plugins.get(i)};
				Class<?> cls = new URLClassLoader(urls).loadClass(classNames.get(i));
				cls.newInstance();
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		
	}
	//gets the permissions from the url
	@SuppressWarnings("resource")
	private ArrayList<String> getPermissions(URL url){
		ArrayList<String> tmp = new ArrayList<String>();
		try {
			ZipEntry permf = new JarFile(url.getFile()).getEntry("perms.xml");
			Document permd = Jsoup.parse(new JarFile(url.getFile()).getInputStream(permf), null, " ", Parser.xmlParser());
			Elements perms = permd.getElementsByTag("perm");
			for (Element perm : perms)
				if (askUser(perm.text(), getName(url), getVersion(url)))
					tmp.add(perm.text());
		} catch (IOException e) {
			return new ArrayList<String>();
		}
		return tmp;
	}
	private boolean askUser(String perm, String name, int version) {
		String[] buttons = {"Yes","No"};
		int result = JOptionPane.showOptionDialog(null, "The Plugin " + name + " Version: " + version + " wants to use " + perm + " do you allow it ?", "Plugin", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, buttons, "Yes");
		if (result > 0 || result == -1)
			return false;
		else
			return true;
	}
	//gets the version from the url
	@SuppressWarnings("resource")
	private int getVersion(URL url) {
		try {
			ZipEntry permf = new JarFile(url.getFile()).getEntry("perms.xml");
			Document permd = Jsoup.parse(new JarFile(url.getFile()).getInputStream(permf), null, " ", Parser.xmlParser());
			Element version = permd.getElementsByTag("version").get(0);
			return Integer.parseInt(version.text());
		} catch (IOException e) {
			return 0;
		}
	}
	//gets the plugin name from the url
	private String getName(URL url) {
		String path = url.getFile();
		String[] parts = path.split("/");
		return parts[parts.length - 1].substring(0, parts[parts.length - 1].length() - 4);
	}
}
