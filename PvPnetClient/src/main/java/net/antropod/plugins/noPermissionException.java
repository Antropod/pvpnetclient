package net.antropod.plugins;

public class noPermissionException extends Exception {
	String perm;
	noPermissionException(String pe) {
		perm = pe;
	}
	public String getMessage() {
		return "the user did not allow the plugin to access this data";
	}
	public String getPermission(){
		return perm;
	}
}
