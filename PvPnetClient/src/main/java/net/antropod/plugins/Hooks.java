package net.antropod.plugins;

import net.antropod.client.Globals;

public class Hooks {
	public static String getLocation(String name) throws noPermissionException{
		PluginDB db = PluginLoader.db;
		if (db.getPermissions(name).contains("getLocation"))
			return Globals.REGION;
		else
			throw new noPermissionException("getLocation");
	}
	
}