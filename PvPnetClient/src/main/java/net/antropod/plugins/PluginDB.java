package net.antropod.plugins;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PluginDB {
	private static Connection conn;
	private static Statement stat;
	private static String path = PluginLoader.pluginPath+"\\db";
	PluginDB() {
		try {
			new File(path).mkdirs();
		    Class.forName("org.sqlite.JDBC");
		    conn = DriverManager.getConnection("jdbc:sqlite:" + path + "\\config.sqlite");
		    stat = conn.createStatement();
		    stat.executeUpdate("CREATE TABLE IF NOT EXISTS plugins(name string, version int, permissions string)");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public boolean isSaved(String name, int version) {
		try {
			ResultSet rs = stat.executeQuery("SELECT * FROM plugins WHERE name='" + name + "'");
			if (rs.next()) {
				if (rs.getInt(2) < version) {
					stat.executeUpdate("DELETE FROM plugins WHERE name='" + name + "'");
					return false;
				}
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public boolean savePlugin(String name, int version, ArrayList<String> perm) {
		try {
			String perms = "";
			for(String e : perm)
				perms += e + ";";
			if (perms.endsWith(";"))
				perms = perms.substring(0, perms.length() - 1);
			stat.executeUpdate("INSERT INTO plugins VALUES('" + name + "', '" + version + "', '" + perms + "')");
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public ArrayList<String> getPermissions(String name) {
		ArrayList<String> tmp = new ArrayList<String>();
		try {
			ResultSet res = stat.executeQuery("SELECT * FROM plugins WHERE name ='" + name + "'");
			String rawPerms = res.getString("permissions");
			String[] perms = rawPerms.split(";");
			for (String perm : perms)
				tmp.add(perm);
			return tmp;
		} catch (SQLException e) {
			return null;
		}
			
	}
}
