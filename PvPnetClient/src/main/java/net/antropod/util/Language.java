package net.antropod.util;

import java.util.ArrayList;

/**
 * Contains and manages information about languages
 * @author Antropod
 *
 */
public class Language
{
	private static ArrayList<Language> languages;
	static
	{
		languages=new ArrayList<>();
		languages.add(new Language(0,"en_US","English","English"));
	}
	
	private int ID;
	private String locale, name, localizedName;
	
	private Language(int id, String locale, String name, String localizedName)
	{
		ID=id;
		this.locale=locale;
		this.name=name;
		this.localizedName=localizedName;
	}
	
	public int getID()
	{
		return ID;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getLocalizedname()
	{
		return localizedName;
	}
	
	public String getLocale()
	{
		return locale;
	}
	
	public static Language[] getLanguages()
	{
		return (Language[]) languages.toArray();
	}
	
	public static Language getLanguageByLocale(String loc)
	{
		for(Language lang:languages)
		{
			if(lang.locale.equals(loc))
				return lang;
		}
		return null;
	}
	
}
