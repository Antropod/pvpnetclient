package net.antropod.util;

public class Timer extends Thread
{

	private int delay = 1000;
	private TimerCallback callback;
	private boolean stopped;
	private boolean paused;

	public Timer(int delay, TimerCallback callback) throws IllegalArgumentException
	{
		if (delay < 10)
			throw new IllegalArgumentException("delay must be greater than 10");
		this.delay = delay;
		this.callback = callback;
		this.paused = true;
		this.setDaemon(true);
		this.start();
	}

	public void startTimer()
	{
		paused = false;
	}

	public void stopTimer()
	{
		stopped = true;
	}

	public void pauseTimer()
	{
		paused = true;
	}

	public void run()
	{
		long lastTime = System.currentTimeMillis();
		try
		{
			while (!stopped)
			{
				if (!paused)
				{
					long l = System.currentTimeMillis() - lastTime;
					if (l > delay)
					{
						callback.timePassed();
						lastTime = System.currentTimeMillis();
					}
				}
				sleep(10);

			}
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}
