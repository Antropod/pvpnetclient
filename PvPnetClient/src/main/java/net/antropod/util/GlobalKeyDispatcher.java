package net.antropod.util;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import net.antropod.client.Globals;

public class GlobalKeyDispatcher implements KeyEventDispatcher {
	
	public GlobalKeyDispatcher()
	{
	}
	
    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {

		
        if (e.getID() == KeyEvent.KEY_PRESSED) 
        {
        	if(e.isControlDown()&&e.getKeyCode()==83)//CTRL S
        	{
        		if(Globals.winManager!=null)
        			Globals.winManager.changeChatVisibility();
        	}
        } else if (e.getID() == KeyEvent.KEY_RELEASED) 
        {
        } else if (e.getID() == KeyEvent.KEY_TYPED) 
        {
        }
        return false;
    }
}
