package net.antropod.util;

import java.lang.reflect.InvocationTargetException;

public class Delegate
{
	String name;
	Object obj;

	public Delegate(Object obj, String name)
	{
		this.name = name;
		this.obj = obj;
	}

	public void invoke()
	{
		try
		{
			obj.getClass().getMethod(name, null).invoke(obj, null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Object invoke(Object... args)
	{
		try
		{
			Class<?>[] classes = new Class<?>[args.length];

			for (int i = 0; i < args.length; i++)
			{
				classes[i] = args[i].getClass();
			}
			return obj.getClass().getMethod(name, classes).invoke(obj, args);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
