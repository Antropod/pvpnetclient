package net.client.antropod.resources;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import nl.arcadestorm.util.SimpleLogger;

public class ImageAssets {

    public static BufferedImage LOL_LOGO, LOL_NAME_LOGO;

    static {
        try {
            LOL_LOGO = ImageIO.read(new File("img/tray_32.png"));
            LOL_NAME_LOGO = ImageIO.read(new File("img/lol_name_logo.png"));
        } catch (IOException e) {
            SimpleLogger.log(e);
        }
    }
}
