package nl.arcadestorm.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import nl.arcadestorm.patcher.ScreenPatcher;

/**
 * Add SimpleLogger.log(Exception ex) to a catch block to log the
 * exception to LOG_PATH
 *
 * @author ArcadeStorm
 */
public class SimpleLogger {

    /**
     * Logs and additional info of the given Exception to a .log file
     *
     * @param ex the exception to log
     */
    public static void log(Exception ex) {
        DateFormat df = new SimpleDateFormat("yyyy_MM_dd_hh_mm");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("jpatcher_" + df.format(new Date()) + ".log", false))) {
            df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            bw.write("-------- Error Report --------\n\n");
            // Error date and time
            bw.write("Generated: " + df.format(new Date()) + "\n");
            // Client build info
            bw.write("Patcher: " + ScreenPatcher.PATCHER_VERSION + "\n");
            // OS platform info
            bw.write("Operating System: " + System.getProperty("os.name") + " (" + System.getProperty("os.arch") + ") " + System.getProperty("os.version") + "\n");
            // Java platform info
            bw.write("Java: " + System.getProperty("java.version") + ", " + System.getProperty("java.vendor") + "\n");
            // Virtual machine info
            bw.write("VM: " + System.getProperty("java.vm.name") + " " + System.getProperty("java.vm.version") + ", " + System.getProperty("java.vm.vendor") + "\n\n");

            bw.write("Message: " + ex.getMessage() + "\n");
            bw.write(ex.toString() + "\n");

            StackTraceElement[] st = ex.getStackTrace();
            for (StackTraceElement ste : st) {
                bw.write("\tat " + ste.getClassName() + ste.getMethodName() + "(" + ste.getFileName() + ":" + ste.getLineNumber() + ")\n");
            }
            bw.write("\n");
            bw.flush();
        } catch (IOException ioex) {
            System.err.println("Can't write to log file. Nothing logged...");
        }
    }
}