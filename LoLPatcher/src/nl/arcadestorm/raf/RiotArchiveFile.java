package nl.arcadestorm.raf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;

/**
 * Models the .raf file format that League of Legends uses to store game client
 * assets. Most of the documentation in this class is taken directly from
 * http://leagueoflegends.wikia.com/wiki/RAF:_Riot_Archive_File This page was
 * also used as reference when building this class.
 *
 * @author ArcadeStorm
 */
public class RiotArchiveFile {

    // A fixed value indicating this is a valid RAF file. The value
    // is (in Big Endian) 0x18be0ef0.
    private int magicNumber;
    // The version of the RAF format used by this file.
    private int version;
    // A value used internally by Riot. DO NOT MODIFY.
    private int managerIndex;
    // The offset from the start of the .raf file to the File List.
    private int fileListOffset;
    // The offset from the start of the .raf file to the Path List.
    private int pathListOffset;
    // A list of all of file entries.
    private RAFFileEntry[] fileEntries;
    // The number of bytes contained in the path list.
    private int pathListSize;
    // A list of all the path entries.
    private RAFPathListEntry[] pathListEntries;
    // The literal ASCII-encoded path string. Note that all strings are null
    // terminated (meaning the last byte of the string is 0x00).
    private String[] pathStrings;

    /**
     * fills the model with data from .raf file to which the given String
     * points. Check the link in the class description for an overview of the
     * model structure. *
     *
     * @param filePath the path to the .raf file
     * @return the model of the .raf file
     * @throws FileNotFoundException the .raf file was not found or could not be
     * read from
     */
    public RiotArchiveFile fill(String filePath) throws IOException {
        RandomAccessFile raf = new RandomAccessFile(filePath, "r");

        // Read general values
        magicNumber = readNextValue(raf);
        version = readNextValue(raf);
        managerIndex = readNextValue(raf);
        // Read list offsets
        fileListOffset = readNextValue(raf);
        pathListOffset = readNextValue(raf);

        // Set file pointer to file entry list
        raf.seek(getFileListOffset());
        fileEntries = new RAFFileEntry[readNextValue(raf)];
        // Fill file entry list with RiotFileEntry objects
        for (int i = 0; i < fileEntries.length; i++) {
            fileEntries[i] = new RAFFileEntry(readNextValue(raf), readNextValue(raf), readNextValue(raf), readNextValue(raf));
        }

        // Set file pointer to path entry list
        raf.seek(getPathListOffset());
        // pathListSize is not currently used
        pathListSize = readNextValue(raf);
        pathListEntries = new RAFPathListEntry[readNextValue(raf)];
        // Fill path entry list with RiotPathListEntry objects
        for (int i = 0; i < pathListEntries.length; i++) {
            pathListEntries[i] = new RAFPathListEntry(readNextValue(raf), readNextValue(raf));
        }
        pathStrings = new String[pathListEntries.length];
        Charset charset = Charset.forName("US-ASCII");

        // Read path strings using the information in the path entry list
        for (int i = 0; i < pathStrings.length; i++) {
            raf.seek(getPathListOffset() + pathListEntries[i].pathOffset);
            // Read string while ignoring the null-termination character
            byte[] temp = new byte[pathListEntries[i].pathLength - 1];
            for (int k = 0; k < pathListEntries[i].pathLength - 1; k++) {//
                temp[k] = (byte) raf.readUnsignedByte();
            }
            pathStrings[i] = new String(temp, charset);
        }
        return this;
    }

    /**
     * Returns the next 4 bytes in the given file as an integer. All values in
     * .raf files are 4-byte little-endian integers, while integers in Java are
     * big-endian. This method reads the next 4 bytes and converts them to
     * big-endian format.
     *
     * @param file the file to read from
     * @return the 4-byte value in big-endian format
     * @throws IOException can not read from file
     */
    private int readNextValue(RandomAccessFile file) throws IOException {
        int var = file.readUnsignedByte() & 0xff;
        var |= (file.readUnsignedByte() & 0xff) << 8;
        var |= (file.readUnsignedByte() & 0xff) << 16;
        return var |= (file.readUnsignedByte() & 0xff) << 24;
    }
    
    public int getMagicNumber() {
        return magicNumber;
    }

    public int getVersion() {
        return version;
    }

    public int getManagerIndex() {
        return managerIndex;
    }

    public int getFileListOffset() {
        return fileListOffset;
    }

    public int getPathListOffset() {
        return pathListOffset;
    }

    public int getPathListSize() {
        return pathListSize;
    }

    /**
     * Returns the number of found RAFFileEnty entities in the .raf file. The
     * number of RAFPathListEntry entities and path strings should be the same.
     *
     * @return the number of file entries in the .raf file
     */
    public int getNumEntries() {
        return fileEntries.length;
    }

    /**
     * Returns the RAFFileEntry at a given index. Note that the index of this
     * entry does not neccessarily correnponds to the index of the related
     * RAFPathListEntry and the index of the path string. Use the RAFFileEntry's
     * pathListIndex to find the correct index for these two.
     *
     * @param index the index of the RAFFileEntry to return
     * @return the requested RAFFileEntry, if the given index is within bounds
     */
    public RAFFileEntry getFileEntry(int index) {
        return (index >= 0 && index < fileEntries.length) ? fileEntries[index] : null;
    }

    /**
     * Returns the RAFPathListEntry at a given index. Note that the index of
     * this entry does not neccessarily correnponds to the index of the related
     * RAFFileEntry. Use the getFileEntry(RAFPathListEntry ple) method to find
     * the corresponding RAFFileEntry.
     *
     * @param index the index of the RAFPathListEntry to return
     * @return the requested RAFPathListEntry, if the given index is within
     * bounds
     */
    public RAFPathListEntry getPathListEntry(int index) {
        return (index >= 0 && index < pathListEntries.length) ? pathListEntries[index] : null;
    }

    /**
     * Returns the path string at a given index. Note that the index of this
     * entry does not neccessarily correnponds to the index of the related
     * RAFFileEntry. Use the getFileEntry(RAFPathListEntry ple) method to find
     * the corresponding RAFFileEntry, where ple is the RAFPathListEntry
     * corresponding to the path string.
     *
     * @param index the index of the RAFPathListEntry to return
     * @return the requested RAFPathListEntry, if the given index is within
     * bounds
     */
    public String getPathString(int index) {
        return (index >= 0 && index < pathStrings.length) ? pathStrings[index] : null;
    }

    /**
     * Returns a RAFFileEntry that corresponds to the given RAFPathListEntry.
     * RAFFileEntry keeps track of it's corresponding RAFPathListEntry with it's
     * pathListIndex field. The RAFPathListEntry doesn't, so this method
     * provides a way to find the related RAFFileEntry.
     *
     * @param ple the RAFPathListEntry to find the related RAFFileEntry of
     * @return the related RAFFileEntry, or null if there is no such
     * RAFFileEntry
     */
    public RAFFileEntry getFileEntry(RAFPathListEntry ple) {
        for (int i = 0; i < fileEntries.length; i++) {
            if (pathListEntries[fileEntries[i].pathListIndex].equals(ple)) {
                return fileEntries[i];
            }
        }
        return null;
    }

    /**
     * Models a raf file entry. League of Legends' .raf files usually contain
     * several of these, each one containing data about a file in the .raf.dat
     * file.
     */
    public class RAFFileEntry {

        // A hash of this file's path.
        public int hash;
        // The offset from the beginning of the associated .raf.dat file.
        public int dataOffset;
        // The size of the data stored in the associated .raf.dat file.
        public int dataSize;
        // The index into the path list. This value should be an integer
        // between 0 and the path list count - 1 (inclusive).
        public int pathListIndex;

        public RAFFileEntry(int hash, int dataOffset, int dataSize, int pathListIndex) {
            this.hash = hash;
            this.dataOffset = dataOffset;
            this.dataSize = dataSize;
            this.pathListIndex = pathListIndex;
        }
    }

    /**
     * Models a raf path list entry. League of Legends' .raf files usually
     * contain several of these, each one containing data about the path of a
     * file in the .raf.dat file.
     */
    public class RAFPathListEntry {

        // The number of bytes the path string is offset from
        // the path list (not the beginning of the file)
        private int pathOffset;
        // The length of the path string in bytes.
        private int pathLength;

        public RAFPathListEntry(int pathOffset, int pathLength) {
            this.pathOffset = pathOffset;
            this.pathLength = pathLength;
        }
    }
}
