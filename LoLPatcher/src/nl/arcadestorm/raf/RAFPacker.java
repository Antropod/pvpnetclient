package nl.arcadestorm.raf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Handles the packing of .dat.raf files. This is done by simply calling pack().
 * the pack() method takes care of data that needs to be compressed as well as
 * data that doesnt need to be compressed. Files that needed to be packed, can
 * be added by calling the addFilePath() method with the files' paths.
 *
 * @author ArcadeStorm
 */
public class RAFPacker {

    // Map of file paths to files that are to be included in the archive, 
    // as well wether or not the file should be compressed before being packed.  
    private ArrayList<PackFileSettings> filePathList;

    /**
     * Constructs an packer instance with an empty file path list.
     */
    public RAFPacker() {
        filePathList = new ArrayList<>();
    }

    /**
     * Adds a file to be packed in the archive. compress Indicates wether or not
     * the file should be compressed before being packed.
     *
     * @param filePath the path where the file resides
     * @param compress wether or not to compress before packing
     * @param unpackPath the path to which the file would be unpacked
     */
    public void addFilePath(String filePath, boolean compress, String unpackPath) {
        filePathList.add(new PackFileSettings(filePath, compress, unpackPath));
    }

    /**
     * Creates a .raf.dat archive and a corresponding .raf file. The archivePath
     * parameter should point to the location of the .raf file, so for example
     * "C:\test\lol\archive.raf". The .raf.dat file is created in the same
     * folder as the .raf file with the same name. Note that Riot names their
     * .raf and .raf.dat in the following format: "Archive_< decimal number>.raf(.dat)",
     * with both files having the same name.
     *
     * @param archivePath the location of the .raf file that will be created
     */
    public void pack(String archivePath) throws IOException{
        // Determine order of path list entries
        Collections.sort(filePathList, getUnpackPathComparator());
        for(int i = 0; i < filePathList.size(); i++) {
            filePathList.get(i).unpackPathIndex = i;
        }
        // Determine order of file entries
        Collections.sort(filePathList, getHashComparator());
        for(int i = 0; i < filePathList.size(); i++) {
            filePathList.get(i).hashIndex = i;
        }
        
        FileOutputStream fos = new FileOutputStream(new File(archivePath));
        
        // Write magic number
        writeNextValue(0x18be0ef0, fos);
        // Write raf format version
        writeNextValue(1, fos);
        // Write manager index
        writeNextValue(0, fos);
        // Write file entry list offset
        writeNextValue(20, fos);
        // Write path list offset
        writeNextValue(24 + filePathList.size() * 16, fos);
        // Write file list entry count
        writeNextValue(filePathList.size(), fos);
        // Write file list entries. File entries are written sorted by hash
        int fileOffset = 0;
        for(int i = 0; i < filePathList.size(); i++) {
            // Write file entry hash
            writeNextValue(hash(filePathList.get(i).unpackPath), fos);
            // Write file entry offset and update offset for next entry
            writeNextValue(fileOffset, fos);
            fileOffset += (int)filePathList.get(i).file.getTotalSpace();
            // Write file entry data size
            writeNextValue((int)filePathList.get(i).file.getTotalSpace(), fos);
            // Write path list index
            writeNextValue(filePathList.get(i).unpackPathIndex, fos);
        }
        
        // Path list entries and path strings are written sorted by unpack path
        Collections.sort(filePathList, getUnpackPathComparator());
        
        // Calculate path list size and write it
        int pathListSize = 8 + filePathList.size() * 8;
        for(int i = 0; i < filePathList.size(); i++) {
            pathListSize += filePathList.get(i).unpackPath.getBytes().length;
        }
        writeNextValue(pathListSize, fos);
        // Write number of path list entries
        writeNextValue(filePathList.size(), fos);
        // Write path list entries
        fileOffset = 8 + filePathList.size() * 8;
        for(int i = 0; i < filePathList.size(); i++) {
            // Write offset from beginning of path list and update for next entry
            writeNextValue(fileOffset, fos);
            fileOffset += filePathList.get(i).unpackPath.getBytes().length;
            // Write path string length
            writeNextValue(filePathList.get(i).unpackPath.getBytes().length, fos);
        }
        // Write path strings
        for(int i = 0; i < filePathList.size(); i++) {
            fos.write(filePathList.get(i).unpackPath.getBytes());
            // Add null-termination character to each path string
            fos.write(0x00);
        }
        
        fos.flush();
        fos.close();
    }
    
    public void writeNextValue(int value, FileOutputStream fos) throws IOException {
        fos.write(value & 0xff);
        fos.write((value & 0xff00) >>> 8);
        fos.write((value & 0xff0000) >>> 16);
        fos.write((value & 0xff000000) >>> 24);
    }

    /**
     * Calculates a hash for a given file path. This is the hash that is part of
     * each FileEntry in a .raf file. Note that this is the file path to which
     * RAFUnpacker will unpack the file.
     *
     * @param filePath the file path from which to construct the hash
     * @return the hash that needs to be set in a FileEntry
     */
    public static int hash(String filePath) {
        long hash = 0;
        long temp;
        for (int i = 0; i < filePath.length(); i++) {
            hash = ((hash << 4) + Character.toLowerCase(filePath.charAt(i))) & 0xffffffff;
            temp = hash & 0xf0000000;
            hash ^= (temp >>> 24);
            hash ^= temp;
        }
        return (int) hash;
    }

    public static Comparator<PackFileSettings> getHashComparator() {
        return new Comparator<PackFileSettings>() {
            @Override
            public int compare(PackFileSettings o1, PackFileSettings o2) {
                int o1Hash = hash(o1.unpackPath);
                int o2Hash = hash(o2.unpackPath);
                if (o1Hash < o2Hash) {
                    return -1;
                }
                return (o1Hash == o2Hash) ? 0 : -1;
            }
        };
    }

    public static Comparator<PackFileSettings> getUnpackPathComparator() {
        return new Comparator<PackFileSettings>() {
            @Override
            public int compare(PackFileSettings o1, PackFileSettings o2) {
                return o1.unpackPath.compareTo(o2.unpackPath);
            }
        };
    }

    private class PackFileSettings {

        public File file;
        public boolean compress;
        public String unpackPath;
        public int hashIndex;
        public int unpackPathIndex;

        public PackFileSettings(String currentPath, boolean compress, String unpackPath) {
            this.file = new File(currentPath);
            this.compress = compress;
            this.unpackPath = unpackPath;

            hashIndex = 0;
            unpackPathIndex = 0;
        }
    }
}
