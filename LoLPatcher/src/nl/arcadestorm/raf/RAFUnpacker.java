package nl.arcadestorm.raf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/**
 * Handles the unpacking of .dat.raf files. This is done by simply calling
 * unpack(build()). the unpack() method takes care of compressed as well as
 * uncompressed data.
 *
 * @author ArcadeStorm
 */
public class RAFUnpacker {

    // Buffer size for buffer that is used to unpack files
    private static final int BUFFER_SIZE = 0x10000;
    // path to the .raf file
    private String path;

    /**
     * Constructs an unpacker instance with the given path set, ready to receive
     * a build() call.
     *
     * @param path path to the .raf file
     */
    public RAFUnpacker(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Builds and returns a model of the .raf file given in the instance's path
     * variable.
     *
     * @return the built model
     */
    public RiotArchiveFile build() throws IOException {
            RiotArchiveFile raf = new RiotArchiveFile();
            return raf.fill(path);
    }

    /**
     * Unpacks a .raf.dat file, given a model by which to unpack. This method
     * creates the directory structure to hold the unpacked files, if
     * neccessary. Note that the path instance variable must be set for this
     * method to work, because it assumes that the .raf and .raf.dat files
     * reside in the same folder.
     *
     * @param raf the model by which the unpacking is done
     * @throws IOException the .raf.dat was not found or could not be read from
     * or the inflater tried to inflate a file that wasn't deflated
     */
    public void unpack(RiotArchiveFile raf) throws IOException {
        // unpack all files
        for (int index = 0; index < raf.getNumEntries(); index++) {
            unpackFile(raf, index);
        }
    }

    /**
     * Unpacks a single file from a .raf.dat file, given a model by which to
     * unpack. This method creates the directory structure to hold the unpacked
     * file, if neccessary. Note that the path instance variable must be set for
     * this method to work, because it assumes that the .raf and .raf.dat files
     * reside in the same folder.
     *
     * @param raf the model by which the unpacking is done
     * @param fileEntryIndex the index of the FileEntry in the model that
     * corresponds to the file that needs to be unpacked
     * @throws IOException the .raf.dat was not found or could not be read from
     * or the inflater tried to inflate a file that wasn't deflated
     */
    public void unpackFile(RiotArchiveFile raf, int fileEntryIndex) throws IOException {
        // create a new file and, if neccessary, the needed directories
        File file = new File(raf.getPathString(raf.getFileEntry(fileEntryIndex).pathListIndex).replace("/", "\\"));
        File dir = new File(file.getParent());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(file);

        RandomAccessFile datFile = new RandomAccessFile(path + ".dat", "r");
        // set file pointer to current file to unpack and read file
        datFile.seek(raf.getFileEntry(fileEntryIndex).dataOffset);
        // check file headers to see if the file is compressed
        boolean compressed = (datFile.readByte() == 0x78 && datFile.readByte() == 0xffffff9c) ? true : false;
        // reset file pointer to start of file
        datFile.seek(raf.getFileEntry(fileEntryIndex).dataOffset);

        int dataToRead = raf.getFileEntry(fileEntryIndex).dataSize;
        byte[] content;
        Inflater inflater = new Inflater();
        byte[] result;
        int resultLength;
        // Read data from .raf.dat while there's more data to be read
        while (dataToRead > 0) {
            if (dataToRead >= BUFFER_SIZE) {
                content = new byte[BUFFER_SIZE];
            } else {
                content = new byte[dataToRead];
            }
            datFile.read(content);
            dataToRead -= content.length;

            if (compressed) {
                // Use java's built-in zlib support to inflate deflated files
                inflater.setInput(content);
                result = new byte[content.length * 2];
                try {
                    while (!inflater.needsInput()) {
                        resultLength = inflater.inflate(result);
                        fos.write(result, 0, resultLength);
                    }
                } catch (DataFormatException ex) {
                    throw new IOException(ex);
                }
            } else {
                // Copy over data plainly if it's not deflated
                fos.write(content);
            }
        }
        fos.flush();
        fos.close();
    }
}
