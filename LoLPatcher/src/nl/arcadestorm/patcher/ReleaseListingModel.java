package nl.arcadestorm.patcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Simple container for the releaselisting files that can be obtained from Riot's
 * servers. Each file contains a list of all release numbers of a project/solutions, 
 * given by the url. 
 * 
 * @author ArcadeStorm
 */
public class ReleaseListingModel {

    // String to construct URL's from. The TYPE, NAME and REGION parts need to 
    // be replaced to create a valid url
    private static final String TEMPLATE_URL = "http://l3cdn.riotgames.com/releases/live/TYPE/NAME/releases/releaselisting_REGION";
    // Either "projects" or "solutions"
    private String group;
    // Name of the project/solution, for example "lol_launcher"
    private String name;
    // Region for which to obtain the releaselisting, for example "EUW"
    private String region;
    // Release numbers as integers, taken as X from 0.0.0.X
    private int[] releases;

    public ReleaseListingModel(String group, String name, String region) {
        this.group = group;
        this.name = name;
        this.region = region;
        releases = new int[0];
    }

    /**
     * 
     * 
     * @param type
     * @param name
     * @param region
     * @return
     * @throws MalformedURLException
     * @throws IOException 
     */
    public static ReleaseListingModel read(String type, String name, String region) throws MalformedURLException, IOException {
        ReleaseListingModel rlm = new ReleaseListingModel(type, name, region);
        ArrayList<Integer> releases = new ArrayList<>();
        URL url = new URL(TEMPLATE_URL.replace("TYPE", type).replace("NAME", name).replace("REGION", region));
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
        String inputLine;
        String[] components;
        while((inputLine = reader.readLine()) != null) {
            releases.add(Integer.parseInt(inputLine.substring(inputLine.lastIndexOf('.') + 1)));
        }
        
        int[] releasesArray = new int[releases.size()];
        for(int i = 0; i < releasesArray.length; i++) {
            releasesArray[i] = releases.get(i);
        }
        rlm.setReleases(releasesArray);
        return rlm;
    }

    /**
     * Returns the higest release number. This appears to always be the first 
     * entry, as release numbers seem to be sorted newest to oldest (highest 
     * to lowest). 
     * 
     * @return the highest release number contained in this ReleaseListingModel 
     */
    public int getLatestRelease() {
        int latestRelease = 0;
        for (int i = 0; i < getReleases().length; i++) {
            if(getReleases()[i] > latestRelease) {
                latestRelease = getReleases()[i];
            }
        }
        return latestRelease;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int[] getReleases() {
        return releases;
    }

    public void setReleases(int[] releases) {
        this.releases = releases;
    }
}
