package nl.arcadestorm.patcher;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import net.client.antropod.resources.ImageAssets;
import nl.arcadestorm.util.SimpleLogger;

/**
 *
 * @author ArcadeStorm
 */
// TODO: javadoc for class and methods
public class ScreenPatcher extends javax.swing.JFrame {

    public static final String PATCHER_VERSION = "0.1";
    // url for fetching news items to display in the patcher
    private static final String NEWS_BASE_URL = "http://ll.leagueoflegends.com/pages/launcher/";
    // (maximum) number of news items to display
    private static final int NUM_NEWS_LABELS = 10;
    // Default values in case properties aren't available
    private static final String DEFAULT_REGION = "EUW";
    private static final String DEFAULT_LOCALE = "en_gb";
    // Client properties from properties.cfg
    private Properties properties;
    // Data for news items in patcher
    private String[] newsTableTitles;
    private URL[] newsTableLinks;
    // TODO: add date labels to news items
    private String[] newsTableDates;
    // Reference to patcher backend
    private Patcher patcher;
    // Set if region is changed. Backend patcher checks this after it's done
    // to see if it has to patch again for a different region
    private boolean regionChanged;

    /**
     * Creates new form Patcher
     */
    public ScreenPatcher() {
        try {
            properties = new Properties();
            regionChanged = false;
            loadProperties();
            loadContent(properties.getProperty("region"), "en");
            initComponents();
            fillLabels();

            setIconImage(ImageIO.read(new File("img/tray_32.png")));
            setResizable(false);
            setTitle("League of Legends jPatcher " + PATCHER_VERSION);
            getContentPane().setBackground(new Color(11, 13, 26));
            //jButton2.setEnabled(false);// TODO: handle button logic

            // center the login screen
            Dimension screenResolution = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation(screenResolution.width / 2 - getWidth() / 2, screenResolution.height / 2 - getHeight() / 2);
            setVisible(true);

            patcher = new Patcher(this, properties.getProperty("region"), properties.getProperty("locale"));
            new Thread(patcher).start();


        } catch (IOException ex) {
            Logger.getLogger(ScreenPatcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setProgressValue(int progress) {
        // TODO: set progress bar max in constructor (indeterminate) and LnF
        jProgressBar1.setValue(progress);
    }

    // TODO: enable/disable
    public void setProgressString(String desc) {
        jProgressBar1.setString(desc);
    }

    public void setProgressMax(int max) {
        jProgressBar1.setMaximum(max);
    }

    public void setProgressIndeterminate(boolean indeterminate) {
        jProgressBar1.setIndeterminate(indeterminate);
    }

    public void setStatusText(String text) {
        jLabel12.setText(text);
    }

    public boolean getRegionChanged() {
        if (regionChanged) {
            regionChanged = false;
            return true;
        }
        return false;
    }

    // TODO: handle exceptions
    private void loadProperties() {
        // Read properties file if available
        File file = new File("properties.cfg");
        if (file.exists() && file.canRead()) {
            try (FileInputStream fis = new FileInputStream(file)) {
                properties.load(fis);
            } catch (IOException ex) {
                // No properties loaded
            }
        }

        // Check if the region field is set
        if (properties.getProperty("region") == null) {
            try {
                // Request user input for region
                Object[] possibilities = {"North America", "Europe West", "Europe Nordic & East", "Brazil", "Turkey"};
                properties.setProperty("region", (String) JOptionPane.showInputDialog(this,
                        "A server region hasn't been set yet. Please choose one below: ",
                        "Please choose a server region",
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        possibilities,
                        possibilities[0]));
            } catch (NullPointerException ex) {
                // Set region field to default in case the input dialog is closed
                properties.setProperty("region", DEFAULT_REGION);
            }
        }

        // TODO: add more locale choices
        // Check if the locale field is set
        if (properties.getProperty("locale") == null) {
            try {
                // Request user input for locale
                Object[] possibilities = {"English"};
                properties.setProperty("locale", (String) JOptionPane.showInputDialog(this,
                        "A language been set yet. Please choose one below: ",
                        "Please choose a language",
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        possibilities,
                        possibilities[0]));
            } catch (NullPointerException ex) {
                // Set locale field to default in case the input dialog is closed
                properties.setProperty("locale", DEFAULT_LOCALE);
            }
        }

        // TODO: move to play button action event
        // Write updated properties to properties.cfg
        try (FileOutputStream fos = new FileOutputStream(file)) {
            properties.store(fos, null);
        } catch (IOException ex) {
            SimpleLogger.log(ex);
        }
    }

    // TODO: write javadoc, handle exceptions
    private void loadContent(String region, String language) throws MalformedURLException, IOException {
        // TODO: build url on locale
        URL url = new URL(NEWS_BASE_URL + region.toLowerCase() + "?lang=" + language);
        URLConnection con = url.openConnection();
        con.setRequestProperty("User-Agent", "");
        BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String news = reader.readLine();

        news = news.substring(news.indexOf('[') + 1, news.indexOf(']'));
        String[] news_items = news.split("\\},\\{");
        news_items[0] = news_items[0].substring(1);
        news_items[news_items.length - 1] = news_items[news_items.length - 1].substring(0, news_items[news_items.length - 1].length() - 1);

        newsTableTitles = new String[NUM_NEWS_LABELS];
        newsTableLinks = new URL[NUM_NEWS_LABELS];
        newsTableDates = new String[NUM_NEWS_LABELS];

        for (int i = 0; i < Math.min(NUM_NEWS_LABELS, news_items.length); i++) {
            news_items[i] = news_items[i].substring(9);
            newsTableTitles[i] = news_items[i].substring(0, news_items[i].indexOf("\","));
            newsTableLinks[i] = new URL(news_items[i].substring(news_items[i].indexOf("\"url\":") + 7, news_items[i].indexOf("\",\"date\":")).replace("\\/", "/"));
            news_items[i] = news_items[i].substring(news_items[i].indexOf("\"url\"") + 5);
            newsTableDates[i] = news_items[i].substring(news_items[i].length() - 11, news_items[i].length() - 1);
        }
    }

    private void fillLabels() throws MalformedURLException {
        jLabel15.setText(newsTableTitles[0]);
        jLabel15.setURL(newsTableLinks[0]);
        jLabel16.setText(newsTableTitles[1]);
        jLabel16.setURL(newsTableLinks[1]);
        jLabel17.setText(newsTableTitles[2]);
        jLabel17.setURL(newsTableLinks[2]);
        jLabel18.setText(newsTableTitles[3]);
        jLabel18.setURL(newsTableLinks[3]);
        jLabel19.setText(newsTableTitles[4]);
        jLabel19.setURL(newsTableLinks[4]);
        jLabel20.setText(newsTableTitles[5]);
        jLabel20.setURL(newsTableLinks[5]);
        jLabel21.setText(newsTableTitles[6]);
        jLabel21.setURL(newsTableLinks[6]);
        jLabel22.setText(newsTableTitles[7]);
        jLabel22.setURL(newsTableLinks[7]);
        jLabel23.setText(newsTableTitles[8]);
        jLabel23.setURL(newsTableLinks[8]);
        jLabel24.setText(newsTableTitles[9]);
        jLabel24.setURL(newsTableLinks[9]);
        jLabel25.setText("View All");
        jLabel25.setURL(new URL("http://leagueoflegends.com/news"));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();
        jLabel12 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new nl.arcadestorm.util.LinkLabel();
        jLabel16 = new nl.arcadestorm.util.LinkLabel();
        jLabel17 = new nl.arcadestorm.util.LinkLabel();
        jLabel18 = new nl.arcadestorm.util.LinkLabel();
        jLabel19 = new nl.arcadestorm.util.LinkLabel();
        jLabel20 = new nl.arcadestorm.util.LinkLabel();
        jLabel21 = new nl.arcadestorm.util.LinkLabel();
        jLabel22 = new nl.arcadestorm.util.LinkLabel();
        jLabel23 = new nl.arcadestorm.util.LinkLabel();
        jLabel24 = new nl.arcadestorm.util.LinkLabel();
        jLabel25 = new nl.arcadestorm.util.LinkLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(11, 13, 26));

        jLabel1.setIcon(new ImageIcon(ImageAssets.LOL_NAME_LOGO));

        jProgressBar1.setStringPainted(true);

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Patching...");
        jLabel12.setMaximumSize(new java.awt.Dimension(200, 14));

        jButton2.setBackground(new java.awt.Color(200, 100, 20));
        jButton2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("Play!");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(11, 13, 26));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("News");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Region: ");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "North America", "Europe West", "Europe Nordic & East", "Brazil", "Turkey" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Language: ");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "English" }));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 292, Short.MAX_VALUE)
                        .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator2)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(161, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
        try {
            System.out.println("Starting jClient...");
            Runtime.getRuntime().exec("java -jar StarterTest.jar EUW en_gb ArcadeStorm TestPassword");
            setVisible(false);
            System.exit(0);
        } catch (IOException ex) {
            SimpleLogger.log(ex);
            System.exit(1);
        }
    }//GEN-LAST:event_jButton2MouseClicked

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        regionChanged = true;
    }//GEN-LAST:event_jComboBox1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ScreenPatcher.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ScreenPatcher.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ScreenPatcher.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ScreenPatcher.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ScreenPatcher().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private nl.arcadestorm.util.LinkLabel jLabel15;
    private nl.arcadestorm.util.LinkLabel jLabel16;
    private nl.arcadestorm.util.LinkLabel jLabel17;
    private nl.arcadestorm.util.LinkLabel jLabel18;
    private nl.arcadestorm.util.LinkLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private nl.arcadestorm.util.LinkLabel jLabel20;
    private nl.arcadestorm.util.LinkLabel jLabel21;
    private nl.arcadestorm.util.LinkLabel jLabel22;
    private nl.arcadestorm.util.LinkLabel jLabel23;
    private nl.arcadestorm.util.LinkLabel jLabel24;
    private nl.arcadestorm.util.LinkLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    // End of variables declaration//GEN-END:variables
}
