package nl.arcadestorm.patcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 *
 * @author Marijn
 */
public class PackageManifestModel {
    
    private static final String TEMPLATE_URL = "http://l3cdn.riotgames.com/releases/live/TYPE/NAME/releases/0.0.0.VERSION/packages/files/packagemanifest";
    private ArrayList<FileEntry> fileEntries;

    public PackageManifestModel() {
        fileEntries = new ArrayList<>();
    }
    
    public String[] getPaths() {
        String[] paths = new String[fileEntries.size()];
        for(int i = 0; i < paths.length; i++) {
            paths[i] = fileEntries.get(i).getPath();
        }
        return paths;
    }

    public void add(FileEntry entry) {
        fileEntries.add(entry);
    }
    
    public static PackageManifestModel read(String type, String name, String version) throws MalformedURLException, IOException {
        PackageManifestModel pmm = new PackageManifestModel();
        URL url = new URL(TEMPLATE_URL.replace("TYPE", type).replace("NAME", name).replace("VERSION", version));
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
        String inputLine = reader.readLine();
        String[] components;
        while((inputLine = reader.readLine()) != null) {
            components = inputLine.split(",");
            pmm.add(new FileEntry(components[0], components[1], Integer.parseInt(components[2]), Integer.parseInt(components[3])));
        }
        return pmm;
    }
    
    public static class FileEntry {

        private String path;
        private String sourceFile;
        private int offset;
        private int size;

        public FileEntry(String path, String sourceFile, int offset, int size) {
            this.path = path;
            this.sourceFile = sourceFile;
            this.offset = offset;
            this.size = size;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getSourceFile() {
            return sourceFile;
        }

        public void setSourceFile(String sourceFile) {
            this.sourceFile = sourceFile;
        }

        public int getOffset() {
            return offset;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }
    }
}
