package nl.arcadestorm.patcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Models the contents of a releasemanifest file. These files are used to keep
 * track of the files of a certain release of a project. Some of the contents of
 * this file are derived from this thread on the League of Legends forums:
 * http://na.leagueoflegends.com/board/showthread.php?p=8632975
 *
 * Data in the releasemanifest is divided in 4 parts: 
 * - Header and metadata 
 * - Directory entries 
 * - File Entries 
 * - Name strings
 *
 * @author ArcadeStorm
 */
public class ReleaseManifestModel {

    // File header. Spells out "RLMS"
    private int fileHeader;
    // Always seems to have the value 0x00010001
    private int unknownVar;
    // Number of directory entries + number of file entries listed in this manifest
    private int itemCount;
    // Release version this manifest corresponds to
    private int releaseVersion;
    // Number of directory entries contained in this manifest
    private int dirCount;
    private DirEntry[] dirEntries;
    // Number of file entries contained in this manifest
    private int fileCount;
    private FileEntry[] fileEntries;
    // Number of text strings contained in this manifest. 
    // The first string is the root ("") and the last string is the name of the 
    // project this manifest corresponds to
    private int stringCount;
    // Size in bytes of all strings combined
    private int dataSize;
    private String[] filePaths;

    private File[] managedFiles;
    private File[] releaseFiles;
    
    public ReleaseManifestModel() {
        managedFiles = new File[0];
        releaseFiles = new File[0];
    }

    public int getFileHeader() {
        return fileHeader;
    }

    public void setFileHeader(int fileHeader) {
        this.fileHeader = fileHeader;
    }

    public int getUnknownVar() {
        return unknownVar;
    }

    public void setUnknownVar(int unknownVar) {
        this.unknownVar = unknownVar;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public int getReleaseVersion() {
        return releaseVersion;
    }

    public void setReleaseVersion(int releaseVersion) {
        this.releaseVersion = releaseVersion;
    }

    public int getDirCount() {
        return dirCount;
    }

    public void setDirCount(int dirCount) {
        this.dirCount = dirCount;
    }

    public DirEntry[] getDirEntries() {
        return dirEntries;
    }

    public void setDirEntries(DirEntry[] dirEntries) {
        this.dirEntries = dirEntries;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public FileEntry[] getFileEntries() {
        return fileEntries;
    }

    public void setFileEntries(FileEntry[] fileEntries) {
        this.fileEntries = fileEntries;
    }

    public int getStringCount() {
        return stringCount;
    }

    public void setStringCount(int stringCount) {
        this.stringCount = stringCount;
    }

    public int getDataSize() {
        return dataSize;
    }

    public void setDataSize(int dataSize) {
        this.dataSize = dataSize;
    }

    public String[] getFilePaths() {
        return filePaths;
    }

    public void setFilePaths(String[] filePaths) {
        this.filePaths = filePaths;
    }

    /**
     * Fills a ReleaseManifestModel instance with data from the releasemanifest
     * that is located at the given path and returns it. Exceptions are thrown
     * when an invalid path is given, the path doesn't point to a file or the
     * releasemanifest file can't be read from. This method does not actively
     * check if a given file is indeed a releasemanifest file and instead relies
     * on the caller to provide a correct path.
     *
     * @param path location of the releasemanifest file
     * @return a ReleaseManifestModel instance containing filled data from the
     * given file
     * @throws FileNotFoundException couldn't find the given file
     * @throws IOException couldn't read from given file
     */
    public static ReleaseManifestModel read(String path) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(path);
        ReleaseManifestModel model = new ReleaseManifestModel();

        // Read releasemanifest metadata
        model.setFileHeader(readNextValue(fis));
        model.setUnknownVar(readNextValue(fis));
        model.setItemCount(readNextValue(fis));
        model.setReleaseVersion(readNextValue(fis));
        model.setDirCount(readNextValue(fis));

        // Read all directory entries
        DirEntry[] dirEntries = new DirEntry[model.getDirCount()];
        for (int i = 0; i < dirEntries.length; i++) {
            dirEntries[i] = new DirEntry(readNextValue(fis), readNextValue(fis), readNextValue(fis), readNextValue(fis), readNextValue(fis));
        }
        model.setDirEntries(dirEntries);

        model.setFileCount(readNextValue(fis));
        // Read all file entries
        FileEntry[] fileEntries = new FileEntry[model.getFileCount()];
        for (int i = 0; i < fileEntries.length; i++) {
            byte[] checksum = new byte[16];
            fileEntries[i] = new FileEntry(readNextValue(fis), readNextValue(fis), fis.read(checksum), readNextValue(fis), readNextValue(fis), readNextValue(fis), readNextValue(fis), readNextValue(fis));
            fileEntries[i].setChecksum(checksum);
        }
        model.setFileEntries(fileEntries);

        model.setStringCount(readNextValue(fis));
        model.setDataSize(readNextValue(fis));
        // Read all name strings
        String[] filePaths = new String[model.getStringCount()];
        String filePath = "";
        byte[] nextChar = new byte[1];
        for (int i = 0; i < filePaths.length; i++) {
            fis.read(nextChar);
            while (nextChar[0] != 0) {
                filePath += (char) nextChar[0];
                fis.read(nextChar);
            }
            filePaths[i] = filePath;
            filePath = "";
        }
        model.setFilePaths(filePaths);
        return model;
    }

    /**
     * Returns the next 4 bytes in the given file as an integer. All values in
     * .raf files are 4-byte little-endian integers, while integers in Java are
     * big-endian. This method reads the next 4 bytes and converts them to
     * big-endian format.
     *
     * @param fis the FileInputStream to read from
     * @return the next 4 bytes as integer
     * @throws IOException the file can not be read from
     */
    public static int readNextValue(FileInputStream fis) throws IOException {
        int var = fis.read() & 0xff;
        var |= (fis.read() & 0xff) << 8;
        var |= (fis.read() & 0xff) << 16;
        return var |= (fis.read() & 0xff) << 24;
    }
    
    public File[] getFiles() {
        ArrayList<File> files = new ArrayList<>();
        getFiles("/", 0, files);
        return files.toArray(new File[files.size()]);
    }
    
    private void getFiles(String path, int dirIndex, ArrayList<File> files) {
        for(int i = dirEntries[dirIndex].subdirFirstIndex; i < dirEntries[dirIndex].subdirFirstIndex + dirEntries[dirIndex].subdirCount; i++) {
            getFiles(path + ((path.length() != 1) ? "/" : "") + filePaths[dirEntries[i].nameIndex], i, files);
        }
        for(int i = dirEntries[dirIndex].fileFirstIndex; i < dirEntries[dirIndex].fileFirstIndex + dirEntries[dirIndex].fileCount; i++) {
            if(fileEntries[i].getUnknownVar1() == 4) {
                // TODO: projects -> projects/solutions
                files.add(new File("RADS/projects/" + filePaths[stringCount - 1] + "/releases/0.0.0." + releaseVersion + "/deploy/" + filePaths[fileEntries[i].nameIndex]));
            } else {
                files.add(new File("RADS/projects/" + filePaths[stringCount - 1] + "/managedfiles/0.0.0." + fileEntries[i].getReleaseVersion() + path + filePaths[fileEntries[i].nameIndex]));
            }
        }
    }

    /**
     * Wrapper class for directory entry data extracted from releasemanifest
     * files
     */
    public static class DirEntry {

        // Index of the string that contains this directory's name
        private int nameIndex;
        // Index of the directory that is the first subdirectory of this directory
        // Subdirectories of a certain directory are positioned adjacent to each 
        // other in the releasemanifest file
        private int subdirFirstIndex;
        // Number of subdirectories this directory has
        private int subdirCount;
        // Index of the first file that is located in this directory
        // Fiels contained in a certain directory are positioned adjacent to each 
        // other in the releasemanifest file
        private int fileFirstIndex;
        // Number of files contained in this directory
        private int fileCount;

        public DirEntry(int nameIndex, int subdirFirstIndex, int subdirCount, int fileFirstIndex, int fileCount) {
            this.nameIndex = nameIndex;
            this.subdirFirstIndex = subdirFirstIndex;
            this.subdirCount = subdirCount;
            this.fileFirstIndex = fileFirstIndex;
            this.fileCount = fileCount;
        }

        public int getNameIndex() {
            return nameIndex;
        }

        public void setNameIndex(int nameIndex) {
            this.nameIndex = nameIndex;
        }

        public int getSubdirFirstIndex() {
            return subdirFirstIndex;
        }

        public void setSubdirFirstIndex(int subdirFirstIndex) {
            this.subdirFirstIndex = subdirFirstIndex;
        }

        public int getSubdirCount() {
            return subdirCount;
        }

        public void setSubdirCount(int subdirCount) {
            this.subdirCount = subdirCount;
        }

        public int getFileFirstIndex() {
            return fileFirstIndex;
        }

        public void setFileFirstIndex(int fileFirstIndex) {
            this.fileFirstIndex = fileFirstIndex;
        }

        public int getFileCount() {
            return fileCount;
        }

        public void setFileCount(int fileCount) {
            this.fileCount = fileCount;
        }
    }

    /**
     * Wrapper class for file entry data extracted from releasemanifest files
     */
    public static class FileEntry {

        // Index of the string that contains this file's name
        private int nameIndex;
        // Release version of this file (may or may not correspond to the release 
        // version that the releasemanifest correnponds to
        private int releaseVersion;
        private byte[] checksum;
        // TODO: determine the role of these unknown variables
        private int unknownVar1;
        private int fileSize;
        private int unknownVar2;
        private int unknownVar3;
        private int unknownVar4;

        public FileEntry(int nameIndex, int releaseVersion, int checksumSize, int unknownVar1, int fileSize, int unknownVar2, int unknownVar3, int unknownVar4) {
            this.nameIndex = nameIndex;
            this.releaseVersion = releaseVersion;
            this.checksum = new byte[checksumSize];
            this.unknownVar1 = unknownVar1;
            this.fileSize = fileSize;
            this.unknownVar2 = unknownVar2;
            this.unknownVar3 = unknownVar3;
            this.unknownVar4 = unknownVar4;
        }

        public int getNameIndex() {
            return nameIndex;
        }

        public void setNameIndex(int nameIndex) {
            this.nameIndex = nameIndex;
        }

        public int getReleaseVersion() {
            return releaseVersion;
        }

        public void setReleaseVersion(int releaseVersion) {
            this.releaseVersion = releaseVersion;
        }

        public byte[] getChecksum() {
            return checksum;
        }

        public void setChecksum(byte[] checksum) {
            this.checksum = checksum;
        }

        public int getUnknownVar1() {
            return unknownVar1;
        }

        public void setUnknownVar1(int unknownVar1) {
            this.unknownVar1 = unknownVar1;
        }

        public int getFileSize() {
            return fileSize;
        }

        public void setFileSize(int fileSize) {
            this.fileSize = fileSize;
        }

        public int getUnknownVar2() {
            return unknownVar2;
        }

        public void setUnknownVar2(int unknownVar2) {
            this.unknownVar2 = unknownVar2;
        }

        public int getUnknownVar3() {
            return unknownVar3;
        }

        public void setUnknownVar3(int unknownVar3) {
            this.unknownVar3 = unknownVar3;
        }

        public int getUnknownVar4() {
            return unknownVar4;
        }

        public void setUnknownVar4(int unknownVar4) {
            this.unknownVar4 = unknownVar4;
        }
    }
}
