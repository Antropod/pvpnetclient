package nl.arcadestorm.patcher;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/**
 *
 * @author ArcadeStorm
 */
public class Patcher implements Runnable {

    private static final int BUFFER_SIZE = 4096;
    private ScreenPatcher viewReference;
    private String region;
    private String locale;

    public Patcher(ScreenPatcher viewReference, String region, String locale) {
        this.viewReference = viewReference;
        this.region = region;
        this.locale = locale;
    }

    @Override
    public void run() {
        try {
            System.out.println("started patcher");
            viewReference.setProgressMax(1);
            viewReference.setProgressValue(0);
            updateClient();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Patcher.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Patcher.class.getName()).log(Level.SEVERE, null, ex);
        }


        if (viewReference.getRegionChanged()) {
            // TODO: update again with new region
        }
    }

    public void updateClient() throws MalformedURLException, IOException {
        viewReference.setStatusText("Getting latest version info...");
        ReleaseListingModel rlm = ReleaseListingModel.read("projects", "lol_game_client", "EUW");
        int latestVersion = rlm.getLatestRelease();
        System.out.println("Latest version: " + latestVersion);
        File path = new File("RADS/projects/lol_game_client/releases/0.0.0." + String.valueOf(latestVersion) + "/S_OK");
        if (path.exists()) {
            return;
        }

        //File[] latestFiles = readReleaseManifest("projects", "lol_game_client", String.valueOf(latestVersion));
        viewReference.setStatusText("Getting latest version file list...");
        PackageManifestModel pmm = PackageManifestModel.read("projects", "lol_game_client", "148");
        String[] filePaths = pmm.getPaths();
        System.out.println("all files: " + filePaths.length);

        viewReference.setStatusText("Determining missing files...");
        ArrayList<String> missingFiles = new ArrayList<>(Arrays.asList(filePaths));
        Iterator<String> iterator = missingFiles.iterator();
        File temp;
        String nextPath;
        while (iterator.hasNext()) {
            nextPath = iterator.next();
            if(nextPath.toLowerCase().endsWith(".compressed")) {
                nextPath = nextPath.substring(0, nextPath.lastIndexOf('.'));
            }
            temp = new File("RADS" + nextPath);
            if (temp.exists()) {
                iterator.remove();
            }
        }
        filePaths = new String[missingFiles.size()];
        missingFiles.toArray(filePaths);

        viewReference.setProgressMax(filePaths.length);
        System.out.println("missing files: " + filePaths.length);
        for (int i = 0; i < filePaths.length; i++) {
            viewReference.setProgressValue(i);
            viewReference.setProgressString(i + "/" + filePaths.length);
            viewReference.setStatusText(filePaths[i].substring(filePaths[i].lastIndexOf('/') + 1));
            downloadFile("http://l3cdn.riotgames.com/releases/live" + filePaths[i].replace(" ", "%20"), "RADS" + filePaths[i]);
            if(filePaths[i].toLowerCase().endsWith(".compressed")) {
                inflateFile("RADS" + filePaths[i]);
                new File("RADS" + filePaths[i]).delete();
            }
        }
    }

    public File[] getFiles(String type, String name) {
        ArrayList<File> files = new ArrayList<>();
        File dir = new File("RADS/" + type + "/" + name);
        dir.mkdirs();
        getFiles(dir, files);
        return files.toArray(new File[files.size()]);
    }

    private void getFiles(File dir, ArrayList<File> files) {
        File[] dirContent = dir.listFiles();
        for (int i = 0; i < dirContent.length; i++) {
            if (dirContent[i].isDirectory()) {
                getFiles(dirContent[i], files);
            } else if (dirContent[i].isFile()) {
                files.add(dirContent[i]);
            }
        }
    }

    public File[] readReleaseManifest(String type, String name, String version) throws FileNotFoundException, IOException {
        String source = "http://l3cdn.riotgames.com/releases/live/TYPE/NAME/releases/0.0.0.VERSION/releasemanifest".replace("TYPE", type).replace("NAME", name).replace("VERSION", version);
        String destination = "RADS/" + type + "/" + name + "/releases/0.0.0." + version + "/releasemanifest";
        downloadFile(source, destination);
        ReleaseManifestModel rmm = ReleaseManifestModel.read(destination);
        return rmm.getFiles();
    }

    private static void downloadFile(String source, String destination) throws MalformedURLException, IOException {
        URL url = new URL(source);
        URLConnection con = url.openConnection();
        int contentSize = con.getContentLength();
        InputStream is = new BufferedInputStream(con.getInputStream());
        File dest = new File(destination);
        dest.getParentFile().mkdirs();
        if (!dest.exists()) {
            dest.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(destination);

        byte[] buffer;
        int numRead;
        while (contentSize > 0) {
            buffer = new byte[(contentSize > BUFFER_SIZE) ? BUFFER_SIZE : contentSize];
            numRead = is.read(buffer);
            fos.write(Arrays.copyOf(buffer, numRead));
            contentSize -= numRead;
        }

        is.close();
        fos.flush();
        fos.close();
    }

    private static void inflateFile(String path) throws FileNotFoundException, IOException {
        FileOutputStream fos = new FileOutputStream(path.substring(0, path.lastIndexOf('.')));
        FileInputStream fis = new FileInputStream(path);
        
        int dataToRead = (int) new File(path).length();
        byte[] content;
        Inflater inflater = new Inflater();
        byte[] result;
        int resultLength;
        // Read data while there's more data to be read
        while (dataToRead > 0) {
            if (dataToRead >= BUFFER_SIZE) {
                content = new byte[BUFFER_SIZE];
            } else {
                content = new byte[dataToRead];
            }
            fis.read(content);
            dataToRead -= content.length;

            // Use java's built-in zlib support to inflate deflated files
            inflater.setInput(content);
            result = new byte[content.length * 2];
            try {
                while (!inflater.needsInput()) {
                    resultLength = inflater.inflate(result);
                    fos.write(result, 0, resultLength);
                }
            } catch (DataFormatException ex) {
                throw new IOException(ex);
            }
        }
        fos.flush();
        fos.close();
        fis.close();
    }

    private int readNextValue(FileInputStream fis) throws IOException {
        int var = fis.read() & 0xff;
        var |= (fis.read() & 0xff) << 8;
        var |= (fis.read() & 0xff) << 16;
        return var |= (fis.read() & 0xff) << 24;
    }
}
